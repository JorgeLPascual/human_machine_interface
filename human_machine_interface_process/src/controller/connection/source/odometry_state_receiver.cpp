/*
  OdometryStateReceiver
  Launch a ROS node to subscribe topics.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/


#include "../include/odometry_state_receiver.h"


/*****************************************************************************
** Implementation
*****************************************************************************/

OdometryStateReceiver::OdometryStateReceiver(){
     subscriptions_complete = false;
}


void OdometryStateReceiver::openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace){

    readParams(nodeHandle);

    DroneEstimatedPoseSubs=nodeHandle.subscribe(rosnamespace + "/" + drone_pose_subscription, 1, &OdometryStateReceiver::droneEstimatedPoseCallback, this);
    DroneEstimatedSpeedSubs=nodeHandle.subscribe(rosnamespace + "/" +  drone_speeds_subscription, 1, &OdometryStateReceiver::droneEstimatedSpeedCallback, this);
    // Topic communications controller
    // Controller references (rebroadcasts): control mode and position, speed and trajectory references
    DroneTrajectoryPositionSubs=nodeHandle.subscribe(rosnamespace + "/" + drone_logger_position_ref_rebroadcast_subscription, 1, &OdometryStateReceiver::dronePoseCallback, this);
    DroneTrajectorySpeedsSubs=nodeHandle.subscribe(rosnamespace + "/" + drone_logger_speed_ref_rebroadcast_subscription, 1, &OdometryStateReceiver::droneSpeedsCallback, this);

    subscriptions_complete = true;
}

bool OdometryStateReceiver::ready() {
    if (!subscriptions_complete)
        return false;
    return true; //Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}

OdometryStateReceiver::~OdometryStateReceiver() {}


void OdometryStateReceiver::readParams(ros::NodeHandle nodeHandle){

    if (!nodeHandle.getParam("drone_pose_subscription", drone_pose_subscription))
      drone_pose_subscription = "EstimatedPose_droneGMR_wrt_GFF";  //It can be the aruco eye too, but the default value is odometry

    if (!nodeHandle.getParam("drone_speeds_subscription", drone_speeds_subscription))
      drone_speeds_subscription = "EstimatedSpeed_droneGMR_wrt_GFF"; //It can be the aruco eye too, but the default value is odometry

    if (!nodeHandle.getParam("drone_logger_position_ref_rebroadcast_subscription", drone_logger_position_ref_rebroadcast_subscription))
      drone_logger_position_ref_rebroadcast_subscription = "trajectoryControllerPositionReferencesRebroadcast";
    
    if (!nodeHandle.getParam("drone_logger_speed_ref_rebroadcast_subscription", drone_logger_speed_ref_rebroadcast_subscription))
      drone_logger_speed_ref_rebroadcast_subscription = "trajectoryControllerSpeedReferencesRebroadcast";


}


void OdometryStateReceiver::droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    drone_pose_msgs=*msg;
    Q_EMIT parameterReceived();
    Q_EMIT updateStatus();
    //printDroneGMREstimatedPoseCallback(msg);
    Q_EMIT updateDronePosition();
    return;
}



void OdometryStateReceiver::droneEstimatedSpeedCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg)
{
    drone_speeds_msgs=*msg;
    Q_EMIT parameterReceived();
    Q_EMIT updateStatus();
    //printDroneGMREstimatedSpeedCallback(msg);
    return;

}

void OdometryStateReceiver::dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    drone_controller_pose_msgs=*msg;
    Q_EMIT parameterReceived();
    Q_EMIT updateStatus();
   // printDronePoseCallback(msg);
    return;
}



void OdometryStateReceiver::droneSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg)
{
     drone_controller_speeds_msgs=*msg;
     Q_EMIT parameterReceived();
     Q_EMIT updateStatus();
     //printDroneSpeedsCallback(msg);
     return;
}