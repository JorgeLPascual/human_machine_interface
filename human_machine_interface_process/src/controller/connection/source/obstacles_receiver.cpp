/*!************************************************************************************
 *  \file      obstacles_receiver.cpp
 *  \brief     Gets the obstacules (walls and poles) size and position from its
 *             corresponding topic.
 *  \details   This file includes the implementation of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *************************************************************************************/

#include "../include/obstacles_receiver.h"


/*!***************************************************************************
 *  \class DronePositionReceiver
 *  \brief This class gets the map info.
 *****************************************************************************/
ObstaclesReceiver::ObstaclesReceiver()
{
    subscriptions_complete = false;
}

ObstaclesReceiver::~ObstaclesReceiver(){}

/*!********************************************************************
 *  \brief      This method allows the process to subscribe to a topic
 *  \param      ROS NodeHandler
 *  \param      ROS namespace
 **********************************************************************/
void ObstaclesReceiver::openSubscriptions(ros::NodeHandle n, std::string ros_namespace)
{
    if(n.getParam("obstacles_topic", obstacle_topic))
        obstacle_topic = "obstacles";

    obstacle_sub = n.subscribe(obstacle_topic, 1, &ObstaclesReceiver::dronePositionCallback,this);

    subscriptions_complete = true;
}

/*!********************************************************************
 *  \brief      This method allows the process to know if the process
 *              is already subscribe to a topic
 **********************************************************************/
bool ObstaclesReceiver::isReady()
{
    return subscriptions_complete;
}


/*!****************************************************************************
 *  \brief      This method emit a SIGNAL when a droneMsgsROS::obstaclesTwoDim
 *              is received
 *  \param      droneMsgsROS::obstaclesTwoDim::ConstPtr
 *****************************************************************************/
void ObstaclesReceiver::dronePositionCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr &msg)
{
    obstacle_msg = *msg;
    walls_vector = obstacle_msg.walls;
    poles_vector = obstacle_msg.poles;

    Q_EMIT updateMapInfo();
}

/*!********************************************************************
 *  \brief      This method gives you the walls vector
 *  \return     droneMsgsROS::obstaclesTwoDim::_walls_type
 **********************************************************************/
droneMsgsROS::obstaclesTwoDim::_walls_type ObstaclesReceiver::getWallsVector()
{
    return walls_vector;
}

/*!********************************************************************
 *  \brief      This method gives you the poles vector
 *  \return     droneMsgsROS::obstaclesTwoDim::_poles_type
 **********************************************************************/
droneMsgsROS::obstaclesTwoDim::_poles_type ObstaclesReceiver::getPolesVector()
{
    return poles_vector;
}
