 /*
  Connection - Intializes ROS threads to send and receive packages 
  @author  Yolanda de la Hoz Simón
  @date    05-2015
  @version 2.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/connection.h"

/*****************************************************************************
** Implementation
*****************************************************************************/

Connection::Connection(QWidget *parent,int argc, char** argv):
    init_argc(argc),
    init_argv(argv),
    QDialog(parent),
    ui(new Ui::connection)
{
	ui->setupUi(this);
    telemetry_receiver= new TelemetryStateReceiver();
    odometry_receiver= new OdometryStateReceiver();
    img_receiver = new ImagesReceiver();
    usercommander= new UserCommander();
    graph_receiver= new RosGraphReceiver();
    mission_planner_receiver= new MissionStateReceiver();
    obstacle_receiver = new ObstaclesReceiver();
    cooperative_receiver = new CooperativeReceiver();
    trajectory_abs_ref_commander_receiver = new TrajectoryAbsRefCommandReceiver();
    society_pose_receiver = new SocietyPoseReceiver();
    executing_tree = false;
    connect_status=false;

    if (init())
        connect_status=true;
    else
        connect_status=false;

    Q_EMIT connectionEstablish();
}

bool Connection::initInCommon(){

    if ( ! ros::master::check() ) // Check if roscore has been initialized.
      return false;

    ros::start(); // explicitly call to ros start
    ros::NodeHandle n;

    if(ros::this_node::getNamespace().compare("/")==0)
        rosnamespace.append("/drone1");//default namespace
    else
        rosnamespace.append(ros::this_node::getNamespace());

    std::cout << "Namespace con ros::this_node::getNamespace(): " << ros::this_node::getNamespace() << std::endl;


    // Start query
    telemetry_receiver->openSubscriptions(n, rosnamespace);
    odometry_receiver->openSubscriptions(n, rosnamespace);
    img_receiver->openSubscriptions(n, rosnamespace);
    graph_receiver->openSubscriptions(n, rosnamespace);
    mission_planner_receiver->openSubscriptions(n, rosnamespace);
    obstacle_receiver->openSubscriptions(n,rosnamespace);
    trajectory_abs_ref_commander_receiver->openSubscriptions(n, rosnamespace);
    society_pose_receiver->openSubscriptions(n,rosnamespace);

  cooperative_receiver->openCommunication(n,rosnamespace);

    // Start command
    usercommander->openPublications(n, rosnamespace);

    if(!Connection::readyForConnect())
     return false;

    std::thread thr(&Connection::spinnerThread, this);
    std::swap(thr, connection_admin_thread);

    return true;
}

bool Connection::init()
{
   ros::init(init_argc,init_argv,ros::this_node::getName());// ros node started

    return Connection::initInCommon();
}

bool Connection::readyForConnect()
{

    if(!telemetry_receiver->ready() || !odometry_receiver->ready() || !img_receiver->ready() || !graph_receiver->ready() ||
       !usercommander->ready() || !cooperative_receiver->ready() || !obstacle_receiver->isReady() ||
            !trajectory_abs_ref_commander_receiver->isReady())
        return false;
    return true;
}

void Connection::spinnerThread()
{
    ros::spin();

}

void Connection::shutdownThread()
{
    if(Connection::readyForConnect())
        connection_admin_thread.detach();
    if(ros::isStarted()) {
      ros::shutdown(); // Kill all open subscriptions, publications, service calls, and service servers.
    }

    std::cout << "Ros shutdown, proceeding to close the gui." << std::endl;
}

void Connection::close()
{
    this->~Connection();
}

Connection::~Connection()
{
    delete ui;
    delete telemetry_receiver;
    delete odometry_receiver;
    delete img_receiver;
    delete usercommander;
    delete graph_receiver;
    delete mission_planner_receiver;
    delete obstacle_receiver;
    delete cooperative_receiver;
    delete trajectory_abs_ref_commander_receiver;
}

void Connection::executeTree() {
    Q_EMIT(executeTreeSignal());
}

void Connection::cancelTree() {
    Q_EMIT(cancelTreeSignal());
}

void Connection::finishTree() {
    Q_EMIT(finishTreeSignal());
}

void Connection::useBTVisualizer(bool b) {
    Q_EMIT(visualizer(b));
}

void Connection::setMissionMode() {
    Q_EMIT(missionMode());
}

void Connection::setShowMode() {
    Q_EMIT(showMode());
}