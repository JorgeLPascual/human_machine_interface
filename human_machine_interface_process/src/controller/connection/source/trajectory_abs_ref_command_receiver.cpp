/*!************************************************************************************
 *  \file      trayectory_abs_ref_command_receiver.cpp
 *  \brief     Get the real trajectory of the drone.
 *  \details   This file includes the implementation of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *************************************************************************************/


#include "../include/trajectory_abs_ref_command_receiver.h"


/*!***************************************************************************
 *  \class TrajectoryAbsRefCommandReceiver
 *  \brief This class gets the real trajectory of the drone.
 *****************************************************************************/
TrajectoryAbsRefCommandReceiver::TrajectoryAbsRefCommandReceiver()
{
    subscriptions_complete = false;
}

TrajectoryAbsRefCommandReceiver::~TrajectoryAbsRefCommandReceiver(){}

/*!********************************************************************
 *  \brief      This method allows the process to subscribe to a topic
 *  \param      ROS NodeHandler
 *  \param      ROS namespace
 **********************************************************************/
void TrajectoryAbsRefCommandReceiver::openSubscriptions(ros::NodeHandle n, std::string ros_namespace)
{
    if(n.getParam("trajectory_topic", trajectory_topic))
        trajectory_topic = "droneTrajectoryAbsRefCommand";

    obstacle_sub = n.subscribe(trajectory_topic, 1, &TrajectoryAbsRefCommandReceiver::trajectoryCallback,this);

    subscriptions_complete = true;
}

/*!********************************************************************
 *  \brief      This method allows the process to know if the process
 *              is already subscribe to a topic
 **********************************************************************/
bool TrajectoryAbsRefCommandReceiver::isReady()
{
    return subscriptions_complete;
}


/*!***********************************************************************************************
 *  \brief      This method emit a SIGNAL when a droneMsgsROS::dronePositionTrajectoryRefCommand
 *              is received
 *  \param      droneMsgsROS::obstaclesTwoDim::ConstPtr
 *************************************************************************************************/
void TrajectoryAbsRefCommandReceiver::trajectoryCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr &msg)
{
    trajectory_msg = *msg;
    drone_trajectory_vector = trajectory_msg.droneTrajectory;
    Q_EMIT sendTrajectory();
}

/*!***********************************************************************************************
 *  \brief      This method returns the trajectory vector received by the message
 *  \param      droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type
 *************************************************************************************************/
droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type TrajectoryAbsRefCommandReceiver::getTrajectoryVector()
{
    return drone_trajectory_vector;
}
