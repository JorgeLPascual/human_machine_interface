#include "../include/society_pose_receiver.h"

SocietyPoseReceiver::SocietyPoseReceiver() : QObject() {
}


SocietyPoseReceiver::~SocietyPoseReceiver() {
}

droneMsgsROS::societyPose::_societyDrone_type SocietyPoseReceiver::getDronesPosition() {
    return positions;
}

void SocietyPoseReceiver::openSubscriptions(ros::NodeHandle n, std::string rosnamespace) {
    n.param<std::string>("society_pose", drones_position_topic, "societyPose");
    drones_position_sub = n.subscribe(drones_position_topic, 1, &SocietyPoseReceiver::dronesPositionCallback,this);
}


void SocietyPoseReceiver::dronesPositionCallback(const droneMsgsROS::societyPose::ConstPtr &msg) {
    message = *msg;
    positions = message.societyDrone;
    Q_EMIT poseReceived();
}