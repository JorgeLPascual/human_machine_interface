/*
  RosGraphReceiver
  Launch a ROS node to subscribe topics.
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/


#include "../include/ros_graph_receiver.h"


/*****************************************************************************
** Implementation
*****************************************************************************/

RosGraphReceiver::RosGraphReceiver(){
    subscriptions_complete = false;
}


void RosGraphReceiver::openSubscriptions(ros::NodeHandle n, std::string rosnamespace){
    // Topic communications

    if (!n.getParam("process_error_unified_notification", supervisor_process_error_unified_notification))
         supervisor_process_error_unified_notification = "process_error_unified_notification";

    if (!n.getParam("processes_performance", supervisor_processes_performance))
         supervisor_processes_performance = "processes_performance";

    if (!n.getParam("wifiIsOk", wifi_connection_topic))
         wifi_connection_topic = "wifiIsOk";

    if (!n.getParam("skill_list", skill_state_topic)) // FALTA METER EN ROSLAUNCH
         skill_state_topic = "skills_list";

    //supervisor
    error_informer_subs=n.subscribe(rosnamespace + "/" + supervisor_process_error_unified_notification, 1, &RosGraphReceiver::errorInformerCallback,this);
    watchdog_subs=n.subscribe(rosnamespace + "/"  + supervisor_processes_performance, 1, &RosGraphReceiver::processPerformanceListCallback,this);
    wificonnection_subs=n.subscribe(rosnamespace + "/"  + wifi_connection_topic, 1, &RosGraphReceiver::wifiConnectionCheckCallback,this);
    skill_state_subs=n.subscribe(rosnamespace + "/"  + skill_state_topic, 1, &RosGraphReceiver::skillStateCallback,this);


    /** request_skills_list_client=n.serviceClient<droneMsgsROS::requestskillsList>("/" + rosnamespace +  "/" + "request_skills_list");
   */
    subscriptions_complete=true;
//    real_time=ros;
}

RosGraphReceiver::~RosGraphReceiver() {}


bool RosGraphReceiver::ready() {
    if (!subscriptions_complete)
        return false;
    return true; //Used this way instead of "return subscriptions_complete" due to preserve add more conditions
}


void RosGraphReceiver::wifiConnectionCheckCallback(const std_msgs::Bool::ConstPtr& msg)
{
   is_wifi_connected=msg->data;
}

void RosGraphReceiver::skillStateCallback(const droneMsgsROS::SkillsList::ConstPtr& msg)
{
   skill_list = *msg;

   //std::cout << "skill list received" << std::endl;
   Q_EMIT stateSkillReceived(&skill_list);
   Q_EMIT stateSkill();

}


void RosGraphReceiver::errorInformerCallback(const droneMsgsROS::ProcessError::ConstPtr& msg)
{

    description=msg->description.c_str();
    node_name=msg->process_name.c_str();
    if(msg->error_type.value==msg->error_type.FrozenNode)
        error_type="FrozenNode";
    if(msg->error_type.value==msg->error_type.InvalidInputData)
        error_type="InvalidInputData";
    if(msg->error_type.value==msg->error_type.SafeguardRecoverableError)
        error_type="SafeguardRecoverableError";
    if(msg->error_type.value==msg->error_type.SafeguarFatalError)
        error_type="SafeguarFatalError";
    if(msg->error_type.value==msg->error_type.UnexpectedProcessStop)
        error_type="UnexpectedProcessStop";

    hostname= msg->hostname.c_str();
    location = msg->function.c_str();
    supervisor_state_time = ros::Time::now().toSec();
    Q_EMIT errorInformerReceived();

    //log(Info,std::string("Received description from /drone_0/error_informer: ")+ boost::lexical_cast<std::string>(msg->description.c_str()) );
    //log(Info,std::string("Received node_name from /drone_0/error_informer: ")+ boost::lexical_cast<std::string>(msg->process_name.c_str()) );
    //log(Info,std::string("Received hostname from /drone_0/error_informer: ")+ boost::lexical_cast<std::string>(msg->hostname.c_str()) );
    //log(Info,std::string("Received location from /drone_0/error_informer: ")+ boost::lexical_cast<std::string>(msg->function.c_str()) );

}


void RosGraphReceiver::processPerformanceListCallback(const droneMsgsROS::ProcessDescriptorList::ConstPtr& msg)
{
    list_process_state=*msg;
    for(unsigned int i = 0; i < msg->process_list.size(); i = i + 1)
        node_container= msg->process_list.at(i);

    Q_EMIT supervisorStateReceived();

}



