#include "../include/cooperative_receiver.h"

CooperativeReceiver::CooperativeReceiver()
{
  communication_established=false;
  is_cooperative_mode_active=false;
}

CooperativeReceiver::~CooperativeReceiver()
{
}

void CooperativeReceiver::activateCooperativeMode()
{
  is_cooperative_mode_active=true;
}

void CooperativeReceiver::deactivateCooperativeMode()
{
  is_cooperative_mode_active=false;
}


void CooperativeReceiver::openCommunication(ros::NodeHandle nodeHandle, std::string rosnamespace)
{
  //Get params
  nodeHandle.param<std::string>("requested_action", requested_action_topic, "requested_action");

  cooperative_pubs=nodeHandle.advertise<droneMsgsROS::actionData>(rosnamespace + "/" + requested_action_topic, 1);

  communication_established=true;
}



bool CooperativeReceiver::ready()
{
  return communication_established;
}


void CooperativeReceiver::sendCommand(QString command, droneMsgsROS::actionArguments arguments)
{

  droneMsgsROS::actionData action = processCommand(command,arguments);
  cooperative_pubs.publish(action);
}


droneMsgsROS::actionData CooperativeReceiver::processCommand(QString command, droneMsgsROS::actionArguments action_arguments)
{

  //Create the msg for the action required.
  droneMsgsROS::actionData action;
  action.time=ros::Time::now();

  //Setting the appropiate action and arguments
  if (command=="TAKE OFF")
  {
    action.mpAction=droneMsgsROS::actionData::TAKE_OFF;
  }
  else if (command=="LAND")
    action.mpAction=droneMsgsROS::actionData::LAND;
  else if (command=="WAIT")
  {
    action.mpAction=droneMsgsROS::actionData::HOVER;
    action.arguments.push_back(action_arguments);
  }
  else if (command=="GO HOME")
  {
    action.mpAction=droneMsgsROS::actionData::GO_TO_POINT;
    action.arguments.push_back(action_arguments);
  }
  else if (command=="GO TO POINT")
  {
    action.mpAction=droneMsgsROS::actionData::GO_TO_POINT;
    action.arguments.push_back(action_arguments);
  }
  return action;
}
