/*!************************************************************************************
 *  \file      trayectory_abs_ref_command_receiver.h
 *  \brief     Get the real trajectory of the drone.
 *  \details   This file includes the headers of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *************************************************************************************/

#ifndef TRAJECTORY_ABS_REF_COMMAND_RECEIVER_H
#define TRAJECTORY_ABS_REF_COMMAND_RECEIVER_H

#include <ros/ros.h>
#include <QObject>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>

class TrajectoryAbsRefCommandReceiver: public QObject
{
    Q_OBJECT
public:
    TrajectoryAbsRefCommandReceiver();
    ~TrajectoryAbsRefCommandReceiver();
    void openSubscriptions(ros::NodeHandle n, std::string ros_namespace);
    bool isReady();
    droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type getTrajectoryVector();

 Q_SIGNALS:
    void sendTrajectory();

private:
    bool subscriptions_complete;
    std::string trajectory_topic;
    ros::Subscriber obstacle_sub;

    droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_msg;
    droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type drone_trajectory_vector;

    void trajectoryCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr &msg);
};


#endif // TRAJECTORY_ABS_REF_COMMAND_RECEIVER_H
