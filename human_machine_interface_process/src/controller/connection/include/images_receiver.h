/*!*******************************************************************************************
 *  \file       images_receiver.h
 *  \brief      ImagesReceiver definition file.
 *  \details    This file includes the ImagesReceiver class declaration. To obtain more
 *              information about it's definition consult the images_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/

#ifndef IMAGESRECEIVER_H
#define IMAGESRECEIVER_H

/*****************************************************************************
** Includes
*****************************************************************************/
#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "communication_definition.h"

#include <QThread>
#include <QString>
#include <QStringListModel>
#include <QPixmap>

class ImagesReceiver : public QObject{
    Q_OBJECT

public:
    ImagesReceiver();
    virtual ~ImagesReceiver();
    void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);
    void onControlModeChange(QString key);
    bool ready();
    void setInspectionMode(bool b);


Q_SIGNALS:
    void Update_Image(const QPixmap* image, int id_camera);




private:
    bool subscriptions_complete;

    std::string drone_console_interface_sensor_bottom_camera;
    std::string drone_console_interface_sensor_front_camera;
    std::string surface_inspection_topic;

    bool surface_inspection;
    
    QPixmap px;
    ros::MultiThreadedSpinner threadSpin;
    image_transport::Subscriber image_bottom_sub_;
    image_transport::Subscriber image_front_sub_;
    image_transport::Subscriber image_surface_inspection_sub_;
    void imagesBottomReceptionCallback(const sensor_msgs::ImageConstPtr& msg);
    void imagesFrontReceptionCallback(const sensor_msgs::ImageConstPtr& msg);
    void imagesSurfaceInspectionCallback(const sensor_msgs::ImageConstPtr& msg);
    QImage cvtCvMat2QImage(const cv::Mat & image);

};


#endif // IMAGESRECEIVER_H
