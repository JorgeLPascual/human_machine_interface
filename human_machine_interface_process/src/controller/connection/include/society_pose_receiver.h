#ifndef SOCIETY_RECEIVER_H
#define SOCIETY_RECEIVER_H

#include <ros/ros.h>
#include <droneMsgsROS/societyPose.h>
#include <droneMsgsROS/droneInfo.h>
#include <QObject>

class SocietyPoseReceiver : public QObject {

    Q_OBJECT

public:
     
     SocietyPoseReceiver();
     ~SocietyPoseReceiver();
     droneMsgsROS::societyPose::_societyDrone_type getDronesPosition();
     void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

private:

    ros::Subscriber drones_position_sub;
    std::string drones_position_topic;
    droneMsgsROS::societyPose message;
    droneMsgsROS::societyPose::_societyDrone_type positions;

    void dronesPositionCallback(const droneMsgsROS::societyPose::ConstPtr &msg);

Q_SIGNALS:
    
    void poseReceived();


};

#endif //SOCIETY_RECEIVER