/*!*******************************************************************************************
 *  \file       odometry_state_receiver.h
 *  \brief      OdometryStateReceiver definition file.
 *  \details    This file includes the OdometryStateReceiver class declaration. To obtain more
 *              information about it's definition consult the odometry_state_receiver.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#ifndef HumanMachineInterface_ODOMETRYSTATERECEIVER_H_
#define HumanMachineInterface_ODOMETRYSTATERECEIVER_H_


#include <ros/ros.h>
#include <string>
#include "std_msgs/String.h"
#include "std_msgs/Float32MultiArray.h"
#include "geometry_msgs/Point.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/AliveSignal.h"
#include "droneMsgsROS/ProcessError.h"
#include "droneMsgsROS/ErrorType.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/obsVector.h"
#include "droneMsgsROS/ProcessDescriptorList.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"

#include <QString>
#include <QThread>
#include <QtDebug>
#include <QStringListModel>

#include <ros/ros.h>
#include <ros/network.h>
#include <string>
#include <std_msgs/String.h>
#include <sstream>


/*****************************************************************************
** Class
*****************************************************************************/

class OdometryStateReceiver: public QObject{
    Q_OBJECT
public:
        OdometryStateReceiver();
	virtual ~ OdometryStateReceiver();

        void run();
        bool ready();

        droneMsgsROS::droneSpeeds drone_speeds_msgs;
        droneMsgsROS::dronePose drone_pose_msgs;

        droneMsgsROS::droneSpeeds drone_controller_speeds_msgs;
        droneMsgsROS::dronePose drone_controller_pose_msgs;

        //std::vector<droneMsgsROS::ProcessDescriptor> list_process_state;



        void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);
        void readParams(ros::NodeHandle nodeHandle);


Q_SIGNALS:

        void parameterReceived();
        void updateStatus();
        void supervisorStateReceived();
        void updateDronePosition();


private:
        bool subscriptions_complete;

        std::string drone_pose_subscription;
        std::string drone_speeds_subscription;
        
        std::string drone_logger_position_ref_rebroadcast_subscription;
        std::string drone_logger_speed_ref_rebroadcast_subscription;

        int init_argc;
        int real_time;
        char** init_argv;

        ros::Subscriber DroneEstimatedPoseSubs;
        void droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg);

        ros::Subscriber DroneEstimatedSpeedSubs;
        void droneEstimatedSpeedCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);

        ros::Subscriber DroneTrajectoryPositionSubs;
        void dronePoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg);

        ros::Subscriber DroneTrajectorySpeedsSubs;
        void droneSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);
};


#endif /* HumanMachineInterface_ODOMETRYSTATERECEIVER_H_ */
