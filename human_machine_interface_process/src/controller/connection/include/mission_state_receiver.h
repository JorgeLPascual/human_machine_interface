/*!*******************************************************************************************
 *  \file       mission_state_receiver.h
 *  \brief      MissionStateReceiver definition file.
 *  \details    This class is in charge of receiving the actions performed in the
 *              Cooperative Interaction and Guided by Mission Planner modes
 *              (approved and completed actions). It also asks for the mission to start/stop
 *              or its name, and receives mission-related information such as completion status
 *              and task performed.
 *  \author     Yolanda de la Hoz Simon, German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/

#ifndef HumanMachineInterface_MISSIONSTATERECEIVER_H_
#define HumanMachineInterface_MISSIONSTATERECEIVER_H_

#include <ros/ros.h>
#include <string>
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "droneMsgsROS/droneMissionInfo.h"
#include <QString>
#include <QtDebug>
#include <QStringListModel>
#include "droneMsgsROS/actionData.h"
#include "droneMsgsROS/actionArguments.h"
#include "droneMsgsROS/openMissionFile.h"
#include "droneMsgsROS/missionName.h"
#include "droneMsgsROS/CompletedAction.h"
#include "droneMsgsROS/PublicEvent.h"
#include "droneMsgsROS/Event.h"


class MissionStateReceiver: public QObject {
    Q_OBJECT
public:
  //Constructor & Destructor
  MissionStateReceiver();
  ~MissionStateReceiver();

public:
  /*!************************************************************************
      *  \brief  This method returns whether the receiver is ready or not.
      *  \return True if the communication with the ROS node has been established; false otherwise.
      *************************************************************************/
  bool ready();

  /*!************************************************************************
       *  \brief  This method establishes the appropiate connections to the ROS topics and services needed.
       *  \param nodeHandle The HMI ROS node.
       *  \param rosnamespace The HMI ROS namespace.
       *************************************************************************/
    void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

    /*!************************************************************************
         *  \brief  This method sets the autonomous mode indicator to active.
         *************************************************************************/
    void activateAutonomousMode();

    /*!************************************************************************
         *  \brief  This method sets the cooperative mode indicator to inactive.
         *  \details Requests mission name.
         *************************************************************************/
    void deactivateAutonomousMode();

    /*!************************************************************************
         *  \brief  This method gets the arguments of the current action
         *************************************************************************/
    QString getActionArguments();
  
    /*!************************************************************************
         *  \brief  This method calls the start mission service.
         *  \return True if the service was called; false otherwise
         *************************************************************************/
  bool startMission();

  /*!************************************************************************
       *  \brief  This method calls the start mission service.
       *  \return True if the service was called; false otherwise
       *************************************************************************/
  bool abortMission();

  /*!************************************************************************
       *  \brief  This method calls the mission name service.
       *  \return True if the service was called; false otherwise
       *************************************************************************/
  bool requestMissionName();

  /*!************************************************************************
       *  \brief  This method emits the emergency land signal.
       *************************************************************************/
  void emitEmergencyLand();

  /** UNUSED */
  bool loadMission(const std::string file_path);

  Q_SIGNALS:

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the current action was received.
        *********************************************************************************************************************/
  void actionReceived(const QString action);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the curent task was received.
        *********************************************************************************************************************/
	void taskReceived(const QString task);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the mission name was received.
        *********************************************************************************************************************/
	void missionLoaded(const QString mission);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when errors in the mission specification file were detected.
        *********************************************************************************************************************/
	void missionErrors(const std::vector<std::string> error_messages);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the mission finished (successfully or unsuccessfully)
        *********************************************************************************************************************/
	void missionCompleted(const bool ack);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the mission starts.
        *********************************************************************************************************************/
	void missionStarted();

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the current action was completed (successfully or unsuccessfully).
        *********************************************************************************************************************/
  void actionCompleted(const int state,const int timeout);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent to indicate emergency land was issued.
        *********************************************************************************************************************/
	void emergencyLand();

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when the current action's arguments were received.
        *********************************************************************************************************************/
	void actionArguments(const QString action_arguments);

  /*!********************************************************************************************************************
        *  \brief      This signal is sent when some drone starts its mission.
        *********************************************************************************************************************/
  void commonMissionStarted();

    /*!********************************************************************************************************************
        *  \brief      This signal is sent when all drones complete their missions.
        *********************************************************************************************************************/
  void commonMissionCompleted();

    /*!********************************************************************************************************************
        *  \brief      This signal is sent when the mission is started due to an external method.
        *********************************************************************************************************************/
  void externalMissionStarted();

public:
  //Indicates if the guided by mission planner mode is active or not.
  bool is_autonomous_mode_active;

private:

  std::vector<std::string> active_drones;

  //ROS Topics
  std::string current_task_topic;
  std::string current_action_topic;
  std::string completed_mission_topic;
  std::string completed_action_topic;
  std::string public_event_topic;
  std::string event_topic;
  
  //Current action
  droneMsgsROS::actionData current_action;
  //Curent task name
  std::string task_name;

  //Indicates if the communication with the ROS node has been established or not.
    bool subscriptions_complete;

    //Current action arguments
    QString arguments;
    
  //ROS Service clients
  ros::ServiceClient load_mission_client; /** UNUSED */
  ros::ServiceClient mission_name_client;
  ros::ServiceClient start_mission_client;
  ros::ServiceClient stop_mission_client;
  
  //ROS subscribers
  ros::Subscriber current_task_subs;
  ros::Subscriber current_action_subs;
  ros::Subscriber completed_mission_subs;
  ros::Subscriber completed_action_subs;
  ros::Subscriber public_event_sub;
  ros::Subscriber event_sub;
  
  //Callbacks. Emit signals accordingly.
  void currentActionCallback(const droneMsgsROS::actionData::ConstPtr &msg);
  void currentTaskCallback(const std_msgs::String::ConstPtr &msg);
  void completedMissionCallback(const std_msgs::Bool::ConstPtr &msg);
  void completedActionCallback(const droneMsgsROS::CompletedAction::ConstPtr &msg);
  void publicEventCallback(const droneMsgsROS::PublicEvent::ConstPtr &msg);
  void externalMissionStartedEvent(const droneMsgsROS::Event::ConstPtr &msg);
};
#endif
