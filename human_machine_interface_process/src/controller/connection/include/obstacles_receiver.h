/*!************************************************************************************
 *  \file      obstacles_recevier.h
 *  \brief     Gets the obstacules (walls and poles) size and position from its
 *             corresponding topic (obstacules).
 *  \details   This file includes the headers of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *************************************************************************************/

#ifndef OBSTACLES_RECEIVER_H
#define OBSTACLES_RECEIVER_H

#include <ros/ros.h>
#include <QObject>
#include "droneMsgsROS/obstaclesTwoDim.h"
#include "droneMsgsROS/obstacleTwoDimWall.h"

class ObstaclesReceiver: public QObject
{
    Q_OBJECT
public:
    ObstaclesReceiver();
    ~ObstaclesReceiver();
    void openSubscriptions(ros::NodeHandle n, std::string ros_namespace);
    bool isReady();
    droneMsgsROS::obstaclesTwoDim::_walls_type getWallsVector();
    droneMsgsROS::obstaclesTwoDim::_poles_type getPolesVector();

Q_SIGNALS:
    void updateMapInfo();

private:
    bool subscriptions_complete;
    std::string obstacle_topic;
    ros::Subscriber obstacle_sub;
    droneMsgsROS::obstaclesTwoDim obstacle_msg;
    droneMsgsROS::obstaclesTwoDim::_walls_type walls_vector;
    droneMsgsROS::obstaclesTwoDim::_poles_type poles_vector;

    void dronePositionCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr &msg);
};

#endif // OBSTACLES_RECEIVER_H
