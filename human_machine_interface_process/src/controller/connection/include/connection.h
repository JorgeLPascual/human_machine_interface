/*!*******************************************************************************************
 *  \file       connection.h
 *  \brief      Connection definition file.
 *  \details    This file includes the Connection class declaration. To obtain more
 *              information about it's definition consult the connection.cpp file.
 *  \author     Yolanda de la Hoz Simon
 *  \copyright  Copyright 2015 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#ifndef CONNECTION_H
#define CONNECTION_H

#include <thread>
#include "../../connection/include/telemetry_state_receiver.h"
#include "../../connection/include/odometry_state_receiver.h"
#include "../../connection/include/images_receiver.h"
#include "../../user_commander/include/user_commander.h"
#include "../../connection/include/mission_state_receiver.h"
#include "../../connection/include/obstacles_receiver.h"
#include "../../connection/include/cooperative_receiver.h"
#include "../../connection/include/trajectory_abs_ref_command_receiver.h"
#include "../../connection/include/society_pose_receiver.h"

#include "ros_graph_receiver.h"

#include <QDialog>
#include <QSettings>
#include "ui_connection.h"

namespace Ui {
class connection;
}

class Connection : public QDialog
{
    Q_OBJECT

public:
    explicit Connection(QWidget *parent = 0, int argc=0, char **argv=0);
    ~Connection();

        void close();
        bool connect_status;
        bool readyForConnect();
        void shutdownThread();
        std::string rosnamespace;
        std::string node_name;
        bool executing_tree;


        TelemetryStateReceiver* telemetry_receiver;
        OdometryStateReceiver* odometry_receiver;
        ImagesReceiver* img_receiver;
        RosGraphReceiver* graph_receiver;
        UserCommander* usercommander;
        MissionStateReceiver* mission_planner_receiver;
        ObstaclesReceiver *obstacle_receiver;
        CooperativeReceiver* cooperative_receiver;
        TrajectoryAbsRefCommandReceiver *trajectory_abs_ref_commander_receiver;
        SocietyPoseReceiver* society_pose_receiver;

        QString settings_file;

        void spinnerThread();

public Q_SLOTS:
        bool init();
        void executeTree();
        void cancelTree();
        void finishTree();

        void useBTVisualizer(bool b);

        void setMissionMode();
        void setShowMode();

Q_SIGNALS:
        void rosShutdown();
        void connectionEstablish();
        
        void executeTreeSignal();
        void cancelTreeSignal();
        void finishTreeSignal();
        void visualizer(bool);
        void missionMode();
        void showMode();
private:

    bool initInCommon();
    std::thread connection_admin_thread;
    Ui::connection *ui;
    int init_argc;
    char** init_argv;


};

#endif // CONNECTION_H
