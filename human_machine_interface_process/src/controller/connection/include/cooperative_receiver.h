/*!*****************************************************************************
 *  \file      cooperative_receiver.h
 *  \brief     Definition of all the classes used in the file
 *             cooperative_receiver.cpp .
 * \details    In charge of requesting commands for the
 *             cooperative interaction mode.
 *
 *  \author    German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef COOPERATIVE_RECEIVER_H
#define COOPERATIVE_RECEIVER_H



#include <QTextEdit>
#include <QString>
#include <ros/ros.h>
#include <string>
#include <droneMsgsROS/actionData.h>
#include <droneMsgsROS/actionArguments.h>
#include <droneMsgsROS/CompletedAction.h>

class CooperativeReceiver : public QObject
{

	Q_OBJECT

public:
  //Constructor & Destructor
  CooperativeReceiver();
  ~CooperativeReceiver();

public:
  /*!************************************************************************
     *  \brief  This method publishes the command specified by its name.
     *  \param command Where the command name is specified.
     *  \param arguments Where the command arguments are stored.
     *************************************************************************/
  void sendCommand(QString command, droneMsgsROS::actionArguments arguments);

  /*!************************************************************************
     *  \brief  This method sets the cooperative mode indicator to active.
     *************************************************************************/
  void activateCooperativeMode();

  /*!************************************************************************
     *  \brief  This method sets the cooperative mode indicator to inactive.
     *************************************************************************/
  void deactivateCooperativeMode();

  /*!************************************************************************
     *  \brief  This method establishes the appropiate connections to the ROS topics needed.
     *  \param nodeHandle The HMI ROS node.
     *  \param rosnamespace The HMI ROS namespace.
     *************************************************************************/
  void openCommunication(ros::NodeHandle nodeHandle, std::string rosnamespace);

  /*!************************************************************************
     *  \brief  This method returns whether the receiver is ready or not.
     *  \return True if the communication with the ROS node has been established; false otherwise.
     *************************************************************************/
  bool ready();
  

public:
  //Indicates if the cooperative mode is active or not.
  bool is_cooperative_mode_active;


private:
 //Publisher to request actions
  ros::Publisher cooperative_pubs;

  //Topic to request actions
  std::string requested_action_topic;

  //Indicates if the communication with the ROS node has been established or not.
  bool communication_established;

  /*!************************************************************************
     *  \brief  This method processes the requested command and its arguments.
     *  \param command The requested command
     *  \param arguments The requested arguments for the command
     *  \return The ROS msg for the action that will be requested according to the command.
     *************************************************************************/
  droneMsgsROS::actionData processCommand(QString command, droneMsgsROS::actionArguments arguments);

};

#endif // COOPERATIVE_RECEIVER_H
