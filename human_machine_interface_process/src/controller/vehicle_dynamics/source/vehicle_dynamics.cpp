#include "../include/vehicle_dynamics.h"

VehicleDynamics::VehicleDynamics(QWidget *parent, OdometryStateReceiver * odometry_receiver) :
    QWidget(parent),
    ui(new Ui::VehicleDynamics)
{
    ui->setupUi(this);
    this->odometryReceiver=odometry_receiver;

}

VehicleDynamics::~VehicleDynamics()
{
    delete ui;
}


void VehicleDynamics::updateDynamicsPanel()
{
        // vehicle
        QString value_x = QString::number(odometryReceiver->drone_pose_msgs.x,'f',2);
        QString value_y = QString::number(odometryReceiver->drone_pose_msgs.y,'f',2);
        QString value_z = QString::number(odometryReceiver->drone_pose_msgs.z,'f',2);
        QString value_yaw = QString::number(angles::to_degrees(odometryReceiver->drone_pose_msgs.yaw),'f',2);
        QString value_pitch = QString::number(angles::to_degrees(odometryReceiver->drone_pose_msgs.pitch),'f',2);
        QString value_roll = QString::number(angles::to_degrees(odometryReceiver->drone_pose_msgs.roll),'f',2);

        if (value_x == "-0.00")
            ui->value_vehicle_x->setText("0.00"); //This prevents displaying -0.00
        else
            ui->value_vehicle_x->setText(value_x);
        if (value_y == "-0.00")
            ui->value_vehicle_y->setText("0.00"); //This prevents displaying -0.00
        else
            ui->value_vehicle_y->setText(value_y);
        if (value_z == "-0.00")
            ui->value_vehicle_z->setText("0.00"); //This prevents displaying -0.00
        else
            ui->value_vehicle_z->setText(value_z);
        if (value_yaw == "-0.00")
            ui->value_vehicle_yaw->setText("0.00"); //This prevents displaying -0.00
        else
            ui->value_vehicle_yaw->setText(value_yaw);
        if (value_pitch == "-0.00")
            ui->value_vehicle_pitch->setText("0.00"); //This prevents displaying -0.00
        else
            ui->value_vehicle_pitch->setText(value_pitch);
        if (value_roll == "-0.00")
            ui->value_vehicle_roll->setText("0.00"); //This prevents displaying -0.00
        else
            ui->value_vehicle_roll->setText(value_roll);
}
