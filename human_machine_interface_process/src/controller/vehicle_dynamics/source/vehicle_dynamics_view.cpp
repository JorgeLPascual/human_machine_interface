#include "../include/vehicle_dynamics_view.h"


VehicleDynamicsView::VehicleDynamicsView(QWidget *parent, Connection * connection) :
    QWidget(parent),
    ui(new Ui::VehicleDynamicsView)
{
    ui->setupUi(this);
    //Add the vehicle dynamics widget.
    vehicleDynamics = new VehicleDynamics(this, connection->odometry_receiver);
    ui->grid_vehicle->addWidget(vehicleDynamics);
}

VehicleDynamicsView::~VehicleDynamicsView()
{
    delete ui;
    delete vehicleDynamics;
}

VehicleDynamics* VehicleDynamicsView::getVehicleDynamics()
{
    return vehicleDynamics;
}
