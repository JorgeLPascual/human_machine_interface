/*!*******************************************************************************************
 *  \file       vehicle_dynamics.h
 *  \brief      Vehicle Dynamics definition file.
 *  \details    The vehicle dynamics wiget shows the drone's current position.
 *  \author     German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/

#ifndef VEHICLE_DYNAMICS_H
#define VEHICLE_DYNAMICS_H

#include <QWidget>
#include "../../connection/include/odometry_state_receiver.h"
#include <angles/angles.h>
#include "ui_vehicle_dynamics.h"

//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
class VehicleDynamics;
}

class VehicleDynamics : public QWidget
{
    Q_OBJECT

public:
  //Constructor & Destructor
    explicit VehicleDynamics(QWidget *parent = 0, OdometryStateReceiver* odometry_receiver = 0);
    ~VehicleDynamics();

public Q_SLOTS:
  /*!********************************************************************************************************************
       *  \brief      This method updates the drone position displayed.
       *
       *********************************************************************************************************************/
    void updateDynamicsPanel();

private:
    //Layout
    Ui::VehicleDynamics *ui;

    OdometryStateReceiver * odometryReceiver;

};

#endif // VEHICLE_DYNAMICS_H
