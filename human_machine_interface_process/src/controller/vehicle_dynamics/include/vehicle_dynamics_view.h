/*!*******************************************************************************************
 *  \file       vehicle_dynamics_view.h
 *  \brief      Vehicle Dynamics View definition file.
 *  \details    General layout for the vehicle dynamics component. Widgets can be dynamically added to it.
 *  \author     German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/

#ifndef VEHICLE_DYNAMICS_VIEW_H
#define VEHICLE_DYNAMICS_VIEW_H

#include <QWidget>
#include "vehicle_dynamics.h"
#include "../../connection/include/connection.h"
#include "ui_vehicle_dynamics_view.h"

//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
class VehicleDynamicsView;
}

class VehicleDynamicsView : public QWidget
{
    Q_OBJECT

public:
  //Constructor & Destructor
    explicit VehicleDynamicsView(QWidget *parent = 0, Connection * connection = 0);
    ~VehicleDynamicsView();

public:
   VehicleDynamics* getVehicleDynamics();

private:
   //Layout
    Ui::VehicleDynamicsView *ui;

    //Widget
    VehicleDynamics * vehicleDynamics;
};

#endif // VEHICLE_DYNAMICS_VIEW_H
