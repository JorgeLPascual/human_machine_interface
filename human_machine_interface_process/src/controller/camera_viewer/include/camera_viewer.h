/*!*******************************************************************************************
 *  \file       camera_viewer.h
 *  \brief      CameraViewer definition file.
 *  \details    This file includes the CameraViewer class declaration. To obtain more
 *              information about it's definition consult the camera_viewer.cpp file.
 *  \author     Carlos Valencia Laray
 *  \copyright  Copyright 2016 UPM. All right reserved. Released under license BSD-3.
 ********************************************************************************************/
#ifndef CAMERAVIEWER_H
#define CAMERAVIEWER_H

#include <QWidget>
#include <QCheckBox>
#include "../../connection/include/images_receiver.h"
#include "../../connection/include/connection.h"
#include "../../camera_viewer/include/camera_display_option.h"
#include "../../camera_viewer/include/camera_main_option.h"
#include "../../camera_viewer/include/camera_grid_option.h"
#include "ui_camera_viewer.h"

namespace Ui {
class CameraViewer;
}

class CameraViewer : public QWidget
{
    Q_OBJECT

public:
    explicit CameraViewer(QWidget *parent = 0, ImagesReceiver *imgReceiver=0);
    ~CameraViewer();

private:
    Ui::CameraViewer *ui;
    //Camera option selected
    bool is_open_main_camera_view;
    bool is_open_one_camera_view;
    bool is_open_four_camera_view;
    int camera_view_manager;

    ImagesReceiver* receiver;

    Connection* connection;

    //Widgets
    CameraDisplayOption *mainoption;
    CameraMainOption *one_option;
    CameraGridOption *fourCamera;
    QCheckBox * surface_inspection;

private:
  /*!********************************************************************************************************************
   *  \brief      This method initializes the camera view layout.
   *********************************************************************************************************************/
  void initializeCameraView();


public Q_SLOTS:
     /*!********************************************************************************************************************
     *  \brief      This method sets the one camera layout.
     *********************************************************************************************************************/
    void displayOneCamera();

    /*!********************************************************************************************************************
     *  \brief      This method sets the main camera grid layout.
     *********************************************************************************************************************/
    void displayMainGridCamera();

    /*!********************************************************************************************************************
     *  \brief      This method sets the four camera grid layout.
     *********************************************************************************************************************/
    void displayFourGridCamera();

    /*!********************************************************************************************************************
     *  \brief      This method emits a signal to indicate that the user wants to save the image.
     *********************************************************************************************************************/
    void saveCurrentCameraView();

    /*!********************************************************************************************************************
     *  \brief      This method changes the image received to the surface inspection topic
     *********************************************************************************************************************/
    void surfaceInspectionChanged(int i);


Q_SIGNALS:
    void saveImage(const int);

};

#endif 