/*
  Camera viewer - Widget with all camera options
  @author  Carlos Valencia Laray
  @date    11-2016
  @version 1.0
*/
#include "../include/camera_viewer.h"

CameraViewer::CameraViewer(QWidget *parent,ImagesReceiver* img_receiver) :
    QWidget(parent),
    ui(new Ui::CameraViewer)
{
    ui->setupUi(this);
    receiver = img_receiver;
    surface_inspection = ui->surface_inspection;

    //Camera UI layout change according to button pressed.
    connect(ui->one_camera_button, SIGNAL(clicked()), this, SLOT(displayOneCamera()));
    connect(ui->main_camera_button, SIGNAL(clicked()), this, SLOT(displayMainGridCamera()));
    connect(ui->four_camera_button, SIGNAL(clicked()), this, SLOT(displayFourGridCamera()));
    connect(surface_inspection, SIGNAL(stateChanged(int)), this, SLOT(surfaceInspectionChanged(int)));

    //Save image
    connect(ui->save_image_button, SIGNAL(clicked()), this, SLOT(saveCurrentCameraView()));


    initializeCameraView(); //Camera widget

}

CameraViewer::~CameraViewer()
{
    delete ui;
}
void CameraViewer::saveCurrentCameraView()
{
    Q_EMIT saveImage(camera_view_manager);
}

void CameraViewer::initializeCameraView()
{
    camera_view_manager=0;
    one_option= new CameraMainOption(this,receiver);
    mainoption= new CameraDisplayOption(this,receiver);
    fourCamera= new CameraGridOption(this,receiver);

    ui->grid_camera->addWidget(one_option,0,0);
    ui->grid_camera->addWidget(mainoption,1,0);
    ui->grid_camera->addWidget(fourCamera,2,0);
    mainoption->hide();
    fourCamera->hide();

    connect(this, SIGNAL(saveImage(const int)), one_option, SLOT(saveCameraImages(const int)));

    displayOneCamera();
}

void CameraViewer::displayOneCamera()
{
    if(!is_open_one_camera_view)
    {
      if (is_open_main_camera_view)
          mainoption->hide();
      else if (is_open_four_camera_view)
          fourCamera->hide();
      camera_view_manager=0;
      one_option->show();
      is_open_one_camera_view=true;
      is_open_four_camera_view=false;
      is_open_main_camera_view=false;
    }
}

void CameraViewer::displayMainGridCamera()
{
    if(!is_open_main_camera_view)
    {
        if (is_open_one_camera_view)
            one_option->hide();
        else if (is_open_four_camera_view)
             fourCamera->hide();
      camera_view_manager=1;
     mainoption->show();
      is_open_main_camera_view=true;
      is_open_four_camera_view=false;
      is_open_one_camera_view=false;
    }
}

void CameraViewer::displayFourGridCamera()
{
    if(!is_open_four_camera_view)
    {
        if (is_open_main_camera_view)
            mainoption->hide();
        else if (is_open_one_camera_view)
            one_option->hide();
        camera_view_manager=3;
        fourCamera->show();
        is_open_four_camera_view=true;
        is_open_main_camera_view=false;
        is_open_one_camera_view=false;
    }
}


void CameraViewer::surfaceInspectionChanged(int i) {
  if (i == 2) {
    receiver->setInspectionMode(true);
  }
  else if (i == 0) {
    receiver->setInspectionMode(false);
  }
}
