#include "../include/main_window.h"

//Define step commands
#define CTE_COMMAND_YAW    0.40
#define CTE_COMMAND_PITCH  0.33
#define CTE_COMMAND_ROLL   0.33
#define CTE_COMMAND_HEIGHT 0.50


// Define controller commands define constants
#define CONTROLLER_CTE_COMMAND_SPEED        ( 1.00 )
#define CONTROLLER_STEP_COMMAND_POSITTION   ( 1.00 )
#define CONTROLLER_STEP_COMMAND_ALTITUDE    ( 1.00 )
#define CONTROLLER_STEP_COMMAND_YAW         ( 10.0 * (M_PI/180.0) )

MainWindow::MainWindow(int argc, char** argv,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/images/images/drone-icon.png"));
    setWindowTitle(QString::fromUtf8("Graphical User Interface"));
    connection = new Connection(this,argc,argv);

    //Views
    controlPanelView = new ControlPanelView(0,connection);
    vehicleDynamicsView = new VehicleDynamicsView(0,connection);
    controlPanel=controlPanelView->getControlPanel();
    execution_viewer = new ExecutionViewer(0);
    tabManager = new TabManager(0);


    //Get widgets
    getWidgets();

    //Adding views to layout
    ui->gridLayout->addWidget(vehicleDynamicsView,1,0,1,1);
    ui->gridLayout->addWidget(controlPanelView,0,0,1,1);
    ui->gridLayout->addWidget(tabManager,0,1,4,1);

    //Connections between widgets
    setSignalHandlers();
    controlPanel->testConnection();


    //TabManager widgets, to add.

    visualizer = new MissionVisualizer(0,connection,connection->odometry_receiver,connection->obstacle_receiver,
        connection->mission_planner_receiver,connection->trajectory_abs_ref_commander_receiver,connection->society_pose_receiver);
    tabManager->customAddTab(visualizer,"Environment");

    QObject::connect(visualizer,SIGNAL(editing(QWidget *)),tabManager,SLOT(maximizeWidget(QWidget*)));
    QObject::connect(visualizer,SIGNAL(showing(QWidget *)),this,SLOT(minimizeWidget(QWidget*)));
    
    param_plot = new ParameterTemporalSeries(0,connection->telemetry_receiver,
        connection->odometry_receiver);
    tabManager->customAddTab(param_plot, "Parameters");

    camera_viewer = new CameraViewer(0,connection->img_receiver);
    tabManager->customAddTab(camera_viewer,"Camera Views");
    tabManager->customAddTab(execution_viewer,"Execution Viewer");

    //process_view = new PerformanceMonitorViewer(0,connection->graph_receiver,
    //    connection->usercommander);
    //tabManager->customAddTab(process_view,"Process Monitor");

    //skill_viewer = new SkillViewer(0, connection->graph_receiver,connection->usercommander);
    //tabManager->customAddTab(skill_viewer,"Requested skills");

    //operator_dialogue = new OperatorDialogue(0,connection->mission_planner_receiver,
    //    connection->cooperative_receiver,controlPanel);
    //tabManager->customAddTab(operator_dialogue,"Operator dialog");

    QObject::connect(visualizer,SIGNAL(editingTree(QWidget *)),tabManager,SLOT(maximizeWidget(QWidget*)));
    QObject::connect(visualizer,SIGNAL(showingTree(QWidget *)),this,SLOT(minimizeWidget(QWidget*)));


    QObject::connect(tabManager,SIGNAL(maximizeTab(QWidget *)),this,SLOT(maximizeWidget(QWidget*)));
    QObject::connect(this,SIGNAL(minimizeTab(QWidget *)),tabManager,SLOT(minimizeWidget(QWidget*)));
    

    n.param<std::string>("behavior_event", behavior_event,
     "behavior_event");
    n.param<std::string>("activate_behavior", activate_behavior,
     "activate_behavior");
    n.param<std::string>("inhibit_behavior", inhibit_behavior,
     "inhibit_behavior");
    n.param<std::string>("initiate_behaviors", initiate_behaviors,
     "initiate_behaviors");
    //Topics para funciones del teclado
    behavior_sub=n.subscribe(behavior_event, 1000, &MainWindow::behaviorCompletedCallback,this);
    activate_behavior_srv=n.serviceClient<droneMsgsROS::BehaviorSrv>(activate_behavior);
    inhibit_behavior_srv=n.serviceClient<droneMsgsROS::BehaviorSrv>(inhibit_behavior);
    initiate_behaviors_srv=n.serviceClient<droneMsgsROS::InitiateBehaviors>(initiate_behaviors);


    droneMsgsROS::InitiateBehaviors msg;
    initiate_behaviors_srv.call(msg);
    active_self_localize_by_odometry = false;
    isAKeyPressed = false;
    setFocusPolicy(Qt::StrongFocus);
    acceptedKeys.insert(0x01000012, false); //Tecla UP
    acceptedKeys.insert(0x01000013, false); //Tecla DOWN
    acceptedKeys.insert(0x01000014, false); //Tecla LEFT
    acceptedKeys.insert(0x01000015, false); //Tecla RIGHT
    acceptedKeys.insert(0x51, false); //Tecla Q
    acceptedKeys.insert(0x41, false); //Tecla A
    acceptedKeys.insert(0x54, false); //Tecla T
    acceptedKeys.insert(0x59, false); //Tecla Y
    acceptedKeys.insert(0x48, false); //Tecla H
    acceptedKeys.insert(0x5a, false); //Tecla Z
    acceptedKeys.insert(0x58, false); //Tecla X
}

MainWindow::~MainWindow()
{
    connection->shutdownThread();
    delete connection;
    delete ui;
    delete controlPanelView;
    delete vehicleDynamicsView;
    //delete process_view;
    delete param_plot;
    //delete skill_viewer;
    //delete operator_dialogue;
    delete camera_viewer;
    delete visualizer;

    /*Delete the tabManager after deleting all the widgets. If Tab manager is showing a widget,
    deleting the tabManager will delete it too. In order to avoid null_ptr, it should be the last
    widget to delete*/

    delete tabManager;
}

void MainWindow::maximizeWidget(QWidget * widget) {
    tabManager->hide();
    controlPanelView->hide();
    vehicleDynamicsView->hide();
    ui->gridLayout->addWidget(widget,0,0,4,1);
    widget->show();
    setFocusPolicy(Qt::NoFocus);
    widget->setFocus();
}

void MainWindow::minimizeWidget(QWidget * widget) {
    tabManager->show();
    Q_EMIT(minimizeTab(widget));
    controlPanelView->show();
    vehicleDynamicsView->show();
    setFocus();
}

void MainWindow::getWidgets()
{
    controlPanel=controlPanelView->getControlPanel();
    vehicleDynamics=vehicleDynamicsView->getVehicleDynamics();
}

// Pop-up windows///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void MainWindow::on_actionAbout_Human_Machine_Interface_triggered()
{
    /**
     * IMPORTANT NOTE: This method is not connected through explicit code in this class.
     * Qt, thanks to the declarations in the .ui file, automatically connects the menu action to this method through their names:
     * actionX is connected to on_actionX_triggered method
     */
    QMessageBox::about(this, tr("About Graphical User Interface"),tr("<h2>Graphical User Interface</h2><p>Copyright (C) 2016 Universidad Politecnica de Madrid.</p><p>This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.</p><p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the <a href='http://www.gnu.org/licenses/'>GNU General Public License</a> for more details.</p><p><b>Safety Warning and Disclaimer:</b></p><p>You are using this software at your own risk. The authors decline any responsibility for personal injuries and/or property damage. Some drones supported by this framework ARE NOT TOYS. Even operation by expert users might cause SERIOUS INJURIES to people around. So, please fly in a properly screened or isolated flight area.</p>"));
}

void MainWindow::on_actionKeyboard_Teleoperation_Help_triggered()
{
    /**
     * IMPORTANT NOTE: This method is not connected through explicit code in this class.
     * Qt, thanks to the declarations in the .ui file, automatically connects the menu action to this method through their names:
     * actionX is connected to on_actionX_triggered method
     */
    QMessageBox::about(this, tr("Keyboard Teleoperation Guide"),tr("<h2>Graphical User Interface</h2><h4>Keys for Keyboard Teleoperation Mode</h4><p>SELF_LOCALIZE_BY_ODOMETRY = l</p><p>TAKE_OFF = t</p><p>LAND = y</p><p>STABILIZE = h</p><p>MOVE_UPWARDS = q</p><p>MOVE_DOWNWARDS = a</p><p>TURN_COUNTER_CLOCKWISE = z</p><p>TURN_CLOCKWISE = x</p><p>MOVE_FORWARD = up </p><p>MOVE_BACK = down</p><p>MOVE_RIGHT = right</p><p>MOVE_LEFT = left</p>"));
    //<p>RESET_COMMANDS= s</p>
    //<p>EMERGENCY_STOP = space</p>
    //<p>SET_YAW_REFERENCE_TO_0 = backslash</p>
}

void MainWindow::setSignalHandlers()
{
    connect(connection->telemetry_receiver, SIGNAL(parameterReceived()),vehicleDynamics, SLOT(updateDynamicsPanel( ))); //Update drone position display
}


// User commands in Keyboard

void MainWindow::keyPressEvent(QKeyEvent *e){
  if(!isAKeyPressed && acceptedKeys.contains(e->key())){
    isAKeyPressed = true;

    if (connection -> connect_status)
      {
        if (!connection->mission_planner_receiver->is_autonomous_mode_active && !connection->cooperative_receiver->is_cooperative_mode_active)
        {
          switch(e->key())
          {
            case Qt::Key_T:
            {
              if(!e->isAutoRepeat())
              {
                  acceptedKeys.insert(0x54, true);
                  controlPanel->onTakeOffButton();
                  /*connection->setMissionMode();
                  droneMsgsROS::BehaviorSrv::Request msg;
                  droneMsgsROS::BehaviorSrv::Response res;
                  droneMsgsROS::BehaviorCommand behavior;
                  behavior.name = "TAKE_OFF";
                  msg.behavior = behavior;
                  activate_behavior_srv.call(msg,res);
                  if(!res.ack)
                      std::cout << res.error_message << std::endl;
                  */
              }
              break;
            }
            case Qt::Key_Y:
            {
              if(!e->isAutoRepeat())
              {
                acceptedKeys.insert(0x59, true);
                controlPanel->onLandButton();
               /* connection->setShowMode();
                droneMsgsROS::BehaviorSrv::Request msg;
                droneMsgsROS::BehaviorSrv::Response res;
                droneMsgsROS::BehaviorCommand behavior;
                behavior.name = "LAND";
                msg.behavior = behavior;
                activate_behavior_srv.call(msg,res);
                if(!res.ack)
                    std::cout << res.error_message << std::endl;
              */}
              break;
            }
            case Qt::Key_H:
            {
              acceptedKeys.insert(0x48, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_HOVERING";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                std::cout << res.error_message << std::endl;
              break;
            }
            case Qt::Key_Right:
            {
              acceptedKeys.insert(0x01000014, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_MOVING";
              behavior.arguments = "direction: \"RIGHT\"\nspeed: 0.4";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            }
            case Qt::Key_Left:
            {
              acceptedKeys.insert(0x01000012, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_MOVING";
              behavior.arguments = "direction: \"LEFT\"\nspeed: 0.4";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            }
            case Qt::Key_Down:
            {
              acceptedKeys.insert(0x01000015, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_MOVING";
              behavior.arguments = "direction: \"BACKWARD\"\nspeed: 0.4";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            } 
            case Qt::Key_Up:
            {
              acceptedKeys.insert(0x01000013, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_MOVING";
              behavior.arguments = "direction: \"FORWARD\"\nspeed: 0.4";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            } 
            case Qt::Key_Q:
            { 
              acceptedKeys.insert(0x51, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_MOVING";
              behavior.arguments = "direction: \"UP\"\nspeed: 0.4";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;  
            }
            case Qt::Key_A:
            {
              acceptedKeys.insert(0x41, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "KEEP_MOVING";
              behavior.arguments = "direction: \"DOWN\"\nspeed: 0.4";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            }
            case Qt::Key_Z:
            {
              acceptedKeys.insert(0x5a, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "ROTATE";
              behavior.arguments = "relative_angle: +179";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            }    
            case Qt::Key_X: 
            {
              acceptedKeys.insert(0x58, true);
              droneMsgsROS::BehaviorSrv::Request msg;
              droneMsgsROS::BehaviorSrv::Response res;
              droneMsgsROS::BehaviorCommand behavior;
              behavior.name = "ROTATE";
              behavior.arguments = "relative_angle: -179";
              msg.behavior = behavior;
              activate_behavior_srv.call(msg,res);
              if(!res.ack)
                  std::cout << res.error_message << std::endl;
              break;
            }
          }
        }
        else
        {
          switch(e->key())
          {
            case Qt::Key_T:
              std::cout<<"Swap to keyboard teleoperation mode button pressed"<<std::endl;
              controlPanel->swapControlPanel("Keyboard teleoperation");
              break;
          }
        }
        QWidget::keyPressEvent(e);
      }
    }
}


void MainWindow::keyReleaseEvent(QKeyEvent *e)
{ 
    if(e->isAutoRepeat() || !acceptedKeys.contains(e->key()))
    {
      isAKeyPressed = false;
      e->ignore();
    }
    else if(acceptedKeys.contains(e->key()) && acceptedKeys.value(e->key()))
    {
      acceptedKeys.insert(e->key(), false);
      droneMsgsROS::BehaviorSrv::Request msg;
      droneMsgsROS::BehaviorSrv::Response res;
      droneMsgsROS::BehaviorCommand behavior;
      behavior.name = "KEEP_HOVERING";
      msg.behavior = behavior;
      if(e->key() != Qt::Key_Y)
        activate_behavior_srv.call(msg,res);
      if(!res.ack)
          std::cout << res.error_message << std::endl;
      isAKeyPressed = false;
      QWidget::keyReleaseEvent(e);
    }
    else 
    {
      isAKeyPressed = false;
      e->ignore();
      QWidget::keyReleaseEvent(e);
    }
}

void MainWindow::behaviorCompletedCallback(const droneMsgsROS::BehaviorEvent &msg)
{
  if (msg.name == "ROTATE" && msg.behavior_event_code == "GOAL_ACHIEVED" && isAKeyPressed && acceptedKeys.contains(0x5a) && acceptedKeys.values(0x5a).first() == true)
  {            
    droneMsgsROS::BehaviorSrv::Request msg;
    droneMsgsROS::BehaviorSrv::Response res;
    droneMsgsROS::BehaviorCommand behavior;
    behavior.name = "ROTATE";
    behavior.arguments = "relative_angle: 179";
    msg.behavior = behavior;
    activate_behavior_srv.call(msg,res);
    if(!res.ack)
      std::cout << res.error_message << std::endl;
  }
  if (msg.name == "ROTATE" && msg.behavior_event_code == "GOAL_ACHIEVED" && isAKeyPressed && acceptedKeys.contains(0x58) && acceptedKeys.values(0x58).first() == true)
  {            
    droneMsgsROS::BehaviorSrv::Request msg;
    droneMsgsROS::BehaviorSrv::Response res;
    droneMsgsROS::BehaviorCommand behavior;
    behavior.name = "ROTATE";
    behavior.arguments = "relative_angle: -179";
    msg.behavior = behavior;
    activate_behavior_srv.call(msg,res);
    if(!res.ack)
      std::cout << res.error_message << std::endl;
  }
}