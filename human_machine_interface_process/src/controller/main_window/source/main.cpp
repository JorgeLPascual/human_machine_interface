/*
  Main Qt GUI
  Initialize the GUI application
  @author  Yolanda de la Hoz Simón
  @date    03-2015
  @version 1.0
*/

/*****************************************************************************
** Includes
*****************************************************************************/

#include <stdio.h>
#include <ros/ros.h>
#include "../include/main_window.h"
#include <QApplication>
#include <signal.h>

/*****************************************************************************
** Implementation
*****************************************************************************/

void signalhandler(int sig)
{
    if (sig == SIGINT)
    {
        qApp->quit();
    }
    else if (sig == SIGTERM)
    {
        qApp->quit();
    }
}

int main(int argc, char *argv[])
{
    ros::init(argc,argv,"human_machine_interface");// ros node started.
    QApplication app(argc, argv);
    MainWindow w(argc,argv);

    w.show();

    signal(SIGINT,signalhandler);
    signal(SIGTERM,signalhandler);

    app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
    int result = app.exec();

    return result;
}
