/*!*******************************************************************************************
 *  \file      main_window.h
 *  \brief      Main Window definition file.
 *  \details    General layout for the main window of the application. Widgets can be dynamically added to it.
 *  \author     German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <ros/ros.h>

#include <QMainWindow>
#include "../../connection/include/connection.h"
#include "../../control_panel/include/control_panel_view.h"
#include "../../vehicle_dynamics/include/vehicle_dynamics_view.h"
#include "../../tab_manager/include/tab_manager.h"
//#include "../../skill_viewer/include/skill_viewer.h"
#include "../../connection/include/connection.h"
#include "../../connection/include/odometry_state_receiver.h"
#include "../../user_commander/include/user_commander.h"
#include "../../parameters_temporal_series/include/parameter_temporal_series.h"
//#include "../../process_monitor/include/performance_monitor_viewer.h"
#include "../../connection/include/society_pose_receiver.h"
#include "../../control_panel/include/control_panel.h"
#include "../../environment_viewer/include/environment_widget.h"
#include "../../environment_viewer/include/mission_visualizer.h"
#include "../../execution_viewer/include/execution_viewer.h"
//#include "../../operator_dialogue/include/operator_dialogue.h"
#include "../../camera_viewer/include/camera_viewer.h"
#include <droneMsgsROS/InitiateBehaviors.h>
#include <droneMsgsROS/BehaviorSrv.h>
#include <droneMsgsROS/ListOfBehaviors.h>
#include <droneMsgsROS/AllBeliefs.h>
#include <QMap>
#include "ui_main_window.h"


//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //Constructor & Destructor
    explicit MainWindow(int argc, char** argv,QWidget *parent = 0);
    ~MainWindow();

public:
   void getWidgets();

public Q_SLOTS:
  /*!********************************************************************************************************************
   *  \brief      This method opens the About window when the About menu option is clicked.
   *********************************************************************************************************************/
    void on_actionAbout_Human_Machine_Interface_triggered();

    /*!********************************************************************************************************************
     *  \brief      This method opens the Keyboard Help window when the Keyboard Help menu option is clicked.
     *********************************************************************************************************************/
    void on_actionKeyboard_Teleoperation_Help_triggered();

    /*!********************************************************************************************************************
     *  \brief      This method is the responsible for handling interruption signals.
     *  \details    The signals handled are the user keyboard commands.
     *********************************************************************************************************************/
    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    /*!********************************************************************************************************************
     *  \brief      This method is the responsible for handling signals which envolve maximize the widget.
     *  \details    The signal will maximize the widget given as a parameter.
     *********************************************************************************************************************/
    void maximizeWidget(QWidget * widget);

    /*!********************************************************************************************************************
     *  \brief      This method is the responsible for handling signals which envolve minimize the widget.
     *  \details    The signal will minimize (in the tab_manager) the widget given as a parameter.
     *********************************************************************************************************************/
    void minimizeWidget(QWidget * widget);

private:
    /*!********************************************************************************************************************
     *  \brief      This method is where all the signals interruptions are handled.
     *********************************************************************************************************************/
    void setSignalHandlers();
    void behaviorCompletedCallback(const droneMsgsROS::BehaviorEvent &msg);

private:
    Ui::MainWindow *ui;

    Connection * connection;

    //Views
    ControlPanelView * controlPanelView;
    VehicleDynamicsView * vehicleDynamicsView;

    //Widgets
    ControlPanel* controlPanel;
    VehicleDynamics* vehicleDynamics;
    TabManager* tabManager;

    std::string behavior_event;
    std::string activate_behavior;
    std::string inhibit_behavior;
    std::string initiate_behaviors;
    QMap<int, bool> acceptedKeys;
    bool active_self_localize_by_odometry;

private: //TabManagerWidgets
    //PerformanceMonitorViewer * process_view;
    ParameterTemporalSeries * param_plot;
    //SkillViewer * skill_viewer;
    //OperatorDialogue * operator_dialogue;
    CameraViewer * camera_viewer;
    MissionVisualizer * visualizer;
    ExecutionViewer * execution_viewer;

    ros::NodeHandle n;
    ros::Subscriber behavior_sub;
    ros::ServiceClient activate_behavior_srv;
    ros::ServiceClient inhibit_behavior_srv;
    ros::ServiceClient initiate_behaviors_srv;

 
    bool isAKeyPressed;

Q_SIGNALS:
    void minimizeTab(QWidget*);

};

#endif // MAIN_WINDOW_H
