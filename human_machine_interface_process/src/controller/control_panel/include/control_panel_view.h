/*!*******************************************************************************************
 *  \file       control_panel_view.h
 *  \brief      Control Panel View definition file.
 *  \details    General layout for the Control Panel component. Widgets can be dynamically added to it.
 *  \author     German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/

#ifndef CONTROL_PANEL_VIEW_H
#define CONTROL_PANEL_VIEW_H

#include <QWidget>
#include "control_panel.h"
#include "ui_control_panel_view.h"

//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
class ControlPanelView;
}

class ControlPanelView : public QWidget
{
    Q_OBJECT

public:
  //Constructor & Destructor
    explicit ControlPanelView(QWidget *parent = 0, Connection * connection = 0);
    ~ControlPanelView();

public:
  ControlPanel* getControlPanel();

private:
  //Layout
    Ui::ControlPanelView *ui;
    ControlPanel *controlPanel;
};

#endif // CONTROL_PANEL_VIEW_H
