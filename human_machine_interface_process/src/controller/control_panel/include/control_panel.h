/*!*******************************************************************************************
 *  \file       control_panel.h
 *  \brief      Control_panel definition file.
 *  \details    The control panel shows drone-related information and manages the buttons for interaction with the drone.
 *  \author     Yolanda de la Hoz Simon, Laura García García, German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/

#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

//Operation modes
#define AUTONOMOUS 0
#define MANUAL 1
#define COOPERATIVE 2

#include <QMessageBox>
#include <QMainWindow>
#include <QTime>
#include <QTimer>
#include <QToolButton>
#include <QWidget>
#include <QAction>
#include "../../connection/include/connection.h"
#include "ui_controlpanel.h"
#include "point_manager.h"
#include <boost/thread.hpp>

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <cstring>
#include <string>
#include <QStackedWidget>
#include <QFileDialog>
#include <QErrorMessage>
#include <ros/ros.h>
#include <droneMsgsROS/openMissionFile.h>
#include <droneMsgsROS/BehaviorSrv.h>
#include <droneMsgsROS/BehaviorCommand.h>
#include <droneMsgsROS/ListOfBehaviors.h>


//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
class ControlPanel;
}


class ControlPanel : public QWidget
{
    Q_OBJECT

public:
  //Constructor & Destructor
  explicit ControlPanel(QWidget *parent,Connection* connection);
  ~ControlPanel();

public:
  /*!********************************************************************************************************************
   *  \brief This method initializes the timer that informs about the time the drone has been flying.
   *  \param ms The interval at which the timer works.
   *********************************************************************************************************************/
  void setTimerInterval(double ms);

  /*!********************************************************************************************************************
   *  \brief      This method initializes the control mode to keyboard teleoperation.
   **********************************************************************************************************************/
  void setInitialControlMode();


public Q_SLOTS:

  /*!********************************************************************************************************************
   *  \brief      This method updates the status panel (drone status, battery, connection status).
   *  \details    It does not update the current_action.
   *
   *********************************************************************************************************************/
    //void updateStatusPanel();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to make the drone to take off.
     *
     *********************************************************************************************************************/
    void onTakeOffButton();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to make the drone to land.
     *
     *********************************************************************************************************************/
    void onLandButton();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to reset the drone.
     *  \details    Resets angles (yaw).
     *********************************************************************************************************************/
    void onResetCommandButton();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to make the drone to hover.
     *
     *********************************************************************************************************************/
    void onHoverButton();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to make the drone to emergency stop.
     *
     *********************************************************************************************************************/
    void onEmergencyStopButton(); /** UNUSED */

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to make the drone to loop.
     *
     *********************************************************************************************************************/
    void onLoopButton();  /** UNUSED */


    /*!********************************************************************************************************************
     *  \brief      This method tells the mission state receiver to signal the start of the mission.
     *
     *********************************************************************************************************************/
    void onStartMissionButton();

    /*!********************************************************************************************************************
     *  \brief      This method tells the mission state receiver to signal the interruption of the mission.
     *
     *********************************************************************************************************************/
    void onAbortMissionButton();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when the user wants to make the drone land while on a mission.
     *
     *********************************************************************************************************************/
    void onEmergencyLandButton();

    /*!********************************************************************************************************************
     *  \brief      This method takes action when an error on roscore ocurs.
     *  \details    Shows a message in a pop-up window.
     *
     *********************************************************************************************************************/
    void testConnection();

    /*!********************************************************************************************************************
     *  \brief      This method informs about the time the drone has been flying.
     *
     *********************************************************************************************************************/
    void flightTime();

    /*!********************************************************************************************************************
     *  \brief      This method informs about the number of errors that have ocurred since the drone began to fly.
     *
     *********************************************************************************************************************/
    //void incrementErrorsCounter();

    /*!********************************************************************************************************************
     *  \brief      This method changes the control panel buttons when changing the control mode.
     *  \details    Changes the bottom part of the control panel.
     *
     *********************************************************************************************************************/
    void swapControlPanel(const QString & mode);

    /*!********************************************************************************************************************
     *  \brief      This method updates the mission name on the panel.
     *********************************************************************************************************************/
  void updateMissionName(const QString mission_name);

  /*!********************************************************************************************************************
   *  \brief      This method shows the errors found in the mission specification file
   *********************************************************************************************************************/
  void displayMissionErrors(const std::vector<std::string> error_messages);

  /*!********************************************************************************************************************
   *  \brief      This method updates the control panel's current action.
   *
   * It is done separately to prevent possible problems with the Q_SIGNALS and Q_SLOTS. Also may need to be changed according to the control mode selected.
   *
   *********************************************************************************************************************/
  //void updateAction(const QString action);

  /*!********************************************************************************************************************
   *  \brief      This method updates the autonomous control panel's current task.
   *
   * It is done separately to prevent possible problems with the Q_SIGNALS and Q_SLOTS. Also may need to be changed according to the control mode selected.
   *
   *********************************************************************************************************************/
  void updateTask(const QString task);

  /*!********************************************************************************************************************
   *  \brief      This method prepares the autonomous mode layout to be able to start a mission.
   *********************************************************************************************************************/
  void displayMissionCompleted(const bool ack);

  /*!********************************************************************************************************************
   *  \brief      This method changes the cooperative mode layout according to the command selected.
   *********************************************************************************************************************/
   void setParameterLayout(const QString & command);

   /*!********************************************************************************************************************
    *  \brief      This method shows the point manager.
    *********************************************************************************************************************/
   void managePoints();

   /*!********************************************************************************************************************
    *  \brief      This method sends the command selected in cooperative mode to the cooperative receiver.
    *  \details    Emits message to show in the operator dialogue.
    **********************************************************************************************************************/
     void sendCommand();

     /*!********************************************************************************************************************
      *  \brief      This method updates the point list.
      *********************************************************************************************************************/
     void updatePoints(const std::vector<struct point> points);

     /*!********************************************************************************************************************
     *  \brief      This method takes action when the mission starts due to an external method.
     *
     *********************************************************************************************************************/
    void externalMissionStarted();

    void abortPythonMission();
    void startPythonMission();
    void selectPythonMission();
    void executeTreeMission();
    void cancelTreeMission();


Q_SIGNALS:
     /*!********************************************************************************************************************
      *  \brief      This signal is sent when the start mission button was pressed.
      *********************************************************************************************************************/
    void startMission();

    /*!********************************************************************************************************************
     *  \brief      This signal is sent when the operation mode was selected.
     *********************************************************************************************************************/
    void operationMode(const int mode);

    /*!********************************************************************************************************************
     *  \brief      This signal is sent when a message in the operator dialogue should be shown (by operator).
     *********************************************************************************************************************/
    void operatorText(const QString text);

    void executeTree();
    void cancelTree();


private:

    /*!********************************************************************************************************************
     *  \brief      This method takes action when an error on roscore ocurs.
     *  \details    Shows a message in a pop-up window.
     *
     *********************************************************************************************************************/
    void showNoMasterMessage();

    /*!********************************************************************************************************************
     *  \brief      This method informs when connection has been established correctly.
     *  \details    Shows a message in a pop-up window.
     *
     *********************************************************************************************************************/
    void showConnectionEstablished();

    /*!********************************************************************************************************************
     *  \brief      This method is the responsible for handling all the interruption signals.
     *  \details    The signals handled are the user signals present in the control panel.
     *
     *********************************************************************************************************************/
    void setSignalHandlers();

    /*!********************************************************************************************************************
     *  \brief      This method shows the drone id.
     *
     *********************************************************************************************************************/
    void setCurrentUAV();

    void newBehaviorCallback(const droneMsgsROS::ListOfBehaviors & msg);
private:
    int current_control_mode;

    bool is_takenOff;

    std::vector<point> point_list;

    QTimer *flight_timer; //Timer that sends the timeout signal every second.
    QTime *current_time;
    int error_counter;

    //Layout
    Ui::ControlPanel *ui;

    int d_interval; // timer in ms
    int d_timerId;

    Connection* connect;

    PointManager* point_manager;

    std::string python_srv;
    std::string python_select_srv;
    std::string python_mission;
    bool isExecuting;
    bool active_self_localization_by_odometry;

    std::string list_of_active_behaviors;
    ros::Subscriber list_of_behaviors_sub;
    std::string drone_id_namespace;
    std::string activate_behavior;
    ros::ServiceClient initiate_behaviors_srv;
    ros::ServiceClient activate_behavior_srv;
    ros::ServiceClient cancel_behavior_srv;
    ros::NodeHandle n;
};

#endif // CONTROLPANEL_H
