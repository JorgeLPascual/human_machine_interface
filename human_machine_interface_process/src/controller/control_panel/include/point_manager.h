/*!*****************************************************************************
 *  \file      point_manager.h
 *  \brief     Definition of all the classes used in the file
 *             operator_dialogue.cpp .
 *  \details   Window to manage points for the cooperative interaction mode.
 *  \author    German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef POINT_MANAGER_H
#define POINT_MANAGER_H

#include <QDialog>
#include <QMessageBox>

#include "ui_point_manager.h"

//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
  class PointManager;
}

/*!************************************************************************
 *  \struct point
 *  \brief  An abstraction of a tridimensional point.
 *************************************************************************/
struct point
{
  QString name;
  float x;
  float y;
  float z;
};

class PointManager : public QDialog
{
    Q_OBJECT

public:
  //Constructor & Destructor
  explicit PointManager(QWidget *parent = 0);
  ~PointManager();


  Q_SIGNALS:
  /*!************************************************************************
    *  \brief  This signal is emitted when the point list was updated.
    *  \param points The point list.
    *************************************************************************/
    void updatePoints(const std::vector<point> points);

public Q_SLOTS:
  /*!************************************************************************
    *  \brief  This method adds a point to the list.
    *  \details Points with an empty name, or with the same name cannot be added.
    *  \return True if the point was added. False, otherwise.
    *************************************************************************/
  bool addPoint();

  /*!************************************************************************
    *  \brief  This method deletes a point from the list.
    *  \return True if the point was deleted. False, otherwise.
    *************************************************************************/
  bool deletePoint();

  /*!************************************************************************
    *  \brief  This method determines whether the 'Delete Point' button is enabled or not.
    *  \details Button enabled as long as a point is selected.
    *************************************************************************/
  void deleteStatus();

public:
  /*!************************************************************************
    *  \brief  Establishes the Qt signal/slot connections
    *************************************************************************/
  void setSignalHandlers();

  /*!************************************************************************
    *  \brief  This method sets the HOME point. Adds it if it didn't exist, or changes the current HOME.
    *  \param home The HOME point.
    *************************************************************************/
  void setHome(const point home);

  /*!************************************************************************
    *  \brief  This method searches the point list for a point with the given name.
    *  \param point_name The name of the point to look for.
    *  \return The index at which the point was found. Returns -1 if the point was not found.
    *************************************************************************/
  int searchPointName(QString point_name);

private:
  //Layout
  Ui::PointManager *ui;
  std::vector<point> point_list;

  //Pattern for white space.
  QRegExp m_whiteSpace;
};

#endif // POINT_MANAGER_H
