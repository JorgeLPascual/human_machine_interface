/*
  ControlPanel
  @author  Germán Quintero
  @date    03-2015
  @version 1.0
*/
#include "../include/control_panel.h"

//Define step commands
#define CTE_COMMAND_YAW    0.40
#define CTE_COMMAND_PITCH  0.33
#define CTE_COMMAND_ROLL   0.33
#define CTE_COMMAND_HEIGHT 0.50


// Define controller commands define constants
#define CONTROLLER_CTE_COMMAND_SPEED        ( 1.00 )
#define CONTROLLER_STEP_COMMAND_POSITTION   ( 0.25 )
#define CONTROLLER_STEP_COMMAND_ALTITUDE    ( 0.25 )
#define CONTROLLER_STEP_COMMAND_YAW         ( 10.0 * (M_PI/180.0) )

using namespace std;

ControlPanel::ControlPanel(QWidget *parent,Connection* connection) :
    QWidget(parent),
    ui(new Ui::ControlPanel) //initialize ui member
{
  ui->setupUi(this);// connects all ui's triggers

  ros::NodeHandle n;
  n.param<std::string>("pml_select_mission", python_select_srv,
    "python_based_mission_interpreter_process/select_mission_file");
  n.param<std::string>("pml_mission_interpreter", python_srv,
    "python_based_mission_interpreter_process");

  isExecuting = false;
  is_takenOff = false;
  error_counter=0;
  connect=connection;

  point_manager = new PointManager(this);

  setSignalHandlers(); // triggers

  active_self_localization_by_odometry = false;

  this->current_time = new QTime(0,0,0);
  setTimerInterval(1000);// 1 second = 1000

  flight_timer = new QTimer(this);
  flight_timer->start(1000);

  setInitialControlMode();

  //Hide buttons that are only shown when a mission has started.
  ui->abort_button->setVisible(false);
  ui->land_button_3->setVisible(false);
  ui->land_button_2->setVisible(false);
  ui->reset_button_2->setVisible(false);
}

ControlPanel::~ControlPanel()
{
    delete ui;
    delete current_time;
    delete flight_timer;
    delete point_manager;
}

void ControlPanel::setTimerInterval(double ms)
{
  d_interval = qRound(ms);
  if (d_interval >= 0 )
    d_timerId = startTimer(d_interval);
}

void ControlPanel::setSignalHandlers()
{
  //Drone status display
  //QObject::connect(connect->usercommander, SIGNAL(managerStatusReceived( )), this, SLOT( updateStatusPanel( )));
  //QObject::connect(connect->mission_planner_receiver, SIGNAL(actionReceived(const QString &)), this, SLOT( updateStatusPanel( )));

  QObject::connect(connect,SIGNAL(connectionEstablish( )),this, SLOT(testConnection()));

  //Manual mode buttons
  QObject::connect(ui->land_button_2,SIGNAL(clicked()),this, SLOT(onLandButton()));
  QObject::connect(ui->take_off_button_2,SIGNAL(clicked()),this, SLOT(onTakeOffButton()));
  //QObject::connect(ui->hover_button_2,SIGNAL(clicked()),this, SLOT(onHoverButton())); este era el boton STOP
  QObject::connect(ui->reset_button_2,SIGNAL(clicked()),this, SLOT(onResetCommandButton()));

  //Error number display
  //QObject::connect(connect->graph_receiver, SIGNAL( errorInformerReceived( )), this, SLOT( incrementErrorsCounter( )));

  //Control panel layout managing
  QObject::connect(ui->selection_mode, SIGNAL(activated(const QString &)), this, SLOT(swapControlPanel(const QString &)));

  //Drone action, task and mission display.
  //QObject::connect(connect->mission_planner_receiver,SIGNAL(actionReceived(const QString)),this,SLOT(updateAction(const QString)));
  QObject::connect(connect->mission_planner_receiver,SIGNAL(taskReceived(const QString)),this,SLOT(updateTask(const QString)));
  QObject::connect(connect->mission_planner_receiver,SIGNAL(missionLoaded(const QString)),this,SLOT(updateMissionName(const QString)));
  QObject::connect(connect->mission_planner_receiver,SIGNAL(missionErrors(const std::vector<std::string>)),this,SLOT(displayMissionErrors(const std::vector<std::string>)));
  QObject::connect(connect->mission_planner_receiver,SIGNAL(missionCompleted(const bool)),this,SLOT(displayMissionCompleted(const bool)));
  //QObject::connect(connect->mission_planner_receiver,SIGNAL(externalMissionStarted()),this,SLOT(externalMissionStarted()));

  //Autonomous mode buttons
  QObject::connect(ui->land_button_3,SIGNAL(clicked()),this, SLOT(onEmergencyLandButton()));
  QObject::connect(ui->start_button,SIGNAL(clicked()),this,SLOT(onStartMissionButton()));
  QObject::connect(ui->abort_button,SIGNAL(clicked()),this, SLOT(onAbortMissionButton()));

  //Cooperative mode buttons and layout managing.
  QObject::connect(ui->commands,SIGNAL(activated(const QString &)),this,SLOT(setParameterLayout(const QString &)));
  QObject::connect(ui->send_command,SIGNAL(clicked()),this,SLOT(sendCommand()));
  QObject::connect(ui->manage_points,SIGNAL(clicked()),this,SLOT(managePoints()));
  QObject::connect(point_manager,SIGNAL(updatePoints( std::vector<struct point>)),this,SLOT(updatePoints(const std::vector<struct point>)));

  //Behavior Tree mission connect

  QObject::connect(this,SIGNAL(executeTree()),connect,SLOT(executeTree()));
  QObject::connect(this,SIGNAL(cancelTree()),connect,SLOT(cancelTree()));

  QObject::connect(ui->execute_tree_button,SIGNAL(clicked()),this,SLOT(executeTreeMission()), Qt::UniqueConnection);
  QObject::connect(ui->emergency_land_tree_button,SIGNAL(clicked()),this,SLOT(onLandButton()));
  QObject::connect(ui->abort_mission_tree_button,SIGNAL(clicked()),this,SLOT(cancelTreeMission()), Qt::UniqueConnection);
  QObject::connect(connect,SIGNAL(finishTreeSignal()),this,SLOT(cancelTreeMission()));
 // ui->cancel_tree_button->hide();
  ui->emergency_land_tree_button->setVisible(false);
  ui->abort_mission_tree_button->setVisible(false);


  //Python mission connect

  QObject::connect(ui->abort_python_button,SIGNAL(clicked()),this,SLOT(abortPythonMission()));
  QObject::connect(ui->start_python_button,SIGNAL(clicked()),this,SLOT(startPythonMission()));
  QObject::connect(ui->select_python_button,SIGNAL(clicked()),this,SLOT(selectPythonMission()));
  QObject::connect(ui->emergency_land_python_button,SIGNAL(clicked()),this,SLOT(onLandButton()));

  ui->abort_python_button->setVisible(false);
  ui->emergency_land_python_button->setVisible(false);
  python_mission = "";

  n.param<std::string>("activate_behavior", activate_behavior,
     "activate_behavior");
  activate_behavior_srv=n.serviceClient<droneMsgsROS::BehaviorSrv>(activate_behavior);

  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace +  "/" + list_of_active_behaviors, 1000, &ControlPanel::newBehaviorCallback, this);

}

void ControlPanel::setParameterLayout(const QString & command)
{
  //Shows the appropiate cooperative mode arguments widget accordingly
  if (command=="TAKE OFF")
    ui->arguments->setCurrentWidget(ui->empty); //No parameters

  else if (command=="LAND")
    ui->arguments->setCurrentWidget(ui->empty); //No parameters

  else if (command=="GO HOME")
    ui->arguments->setCurrentWidget(ui->empty); //No parameters

  else if (command=="SET HOME")
    ui->arguments->setCurrentWidget(ui->empty); //No parameters

  else if (command=="GO TO POINT")
    ui->arguments->setCurrentWidget(ui->point); //Point parameter

  else if (command=="WAIT")
    ui->arguments->setCurrentWidget(ui->timer); //Timer parameter
}

void ControlPanel::swapControlPanel(const QString & mode)
{
  if(mode == "Guided by TML mission")
  {
    ui->stackedWidget->setCurrentWidget(ui->autonomous);
    connect->useBTVisualizer(false);
    current_control_mode = AUTONOMOUS;
    connect->cooperative_receiver->deactivateCooperativeMode();
    connect->mission_planner_receiver->activateAutonomousMode();
    /*Q_EMIT operationMode(AUTONOMOUS);*/
  }
  if (mode == "Keyboard teleoperation")
  {
    droneMsgsROS::BehaviorSrv::Request msg;
    droneMsgsROS::BehaviorSrv::Response res;
    droneMsgsROS::BehaviorCommand behavior;
    behavior.name = "SELF_LOCALIZE_BY_ODOMETRY";
    msg.behavior = behavior;
    activate_behavior_srv.call(msg,res);
    if(!res.ack)
          std::cout << res.error_message << std::endl;
    ui->stackedWidget->setCurrentWidget(ui->manual);
    current_control_mode = MANUAL;
    connect->mission_planner_receiver->deactivateAutonomousMode();
    connect->cooperative_receiver->deactivateCooperativeMode();
    connect->useBTVisualizer(false);

    //Q_EMIT operationMode(MANUAL);
  }
  /*else if (mode == "Cooperative dialogue")
  {
    ui->arguments->setCurrentWidget(ui->empty);
    ui->commands->setCurrentIndex(0);
    ui->stackedWidget->setCurrentWidget(ui->cooperative);
    current_control_mode = COOPERATIVE;
    connect->mission_planner_receiver->deactivateAutonomousMode();
    connect->cooperative_receiver->activateCooperativeMode();
    Q_EMIT operationMode(COOPERATIVE);
  }*/
  else if (mode == "Guided by behavior trees")
  {
    ui->stackedWidget->setCurrentWidget(ui->behaviorTree);
    //current_control_mode = MANUAL;
    connect->mission_planner_receiver->deactivateAutonomousMode();
    connect->cooperative_receiver->deactivateCooperativeMode();
    connect->useBTVisualizer(true);
  }
    else if (mode == "Guided by Python mission")
  {
    ui->stackedWidget->setCurrentWidget(ui->pythonMission);
    //current_control_mode = MANUAL;
    connect->mission_planner_receiver->deactivateAutonomousMode();
    connect->cooperative_receiver->deactivateCooperativeMode();
    connect->useBTVisualizer(false);
  }
}

void ControlPanel::showNoMasterMessage()
{
    QMessageBox msgBox;
    msgBox.setText("roscore node could have not been initialized");
    msgBox.exec();
}


void ControlPanel::showConnectionEstablished()
{
    QMessageBox msgBox;
    msgBox.setText("The connection has been established succesfully.");
    msgBox.exec();
}



void ControlPanel::testConnection()
{
    if (!connect->connect_status){
        cout << "roscore node could have not been initialized" << '\n';
        showNoMasterMessage();
    }else{
        //showConnectionEstablished();
        if(connect->graph_receiver->is_wifi_connected)
            ui->value_wifi->setText("Connected");
        setCurrentUAV();
        QObject::connect(flight_timer, SIGNAL(timeout()), this, SLOT(flightTime()));
        //QObject::connect(flight_timer, SIGNAL(timeout()), this, SLOT(updateStatusPanel()));
    }
}



void ControlPanel::flightTime()
{
    if (is_takenOff)
    {
        this->current_time->setHMS(this->current_time->addSecs(+1).hour(),this->current_time->addSecs(+1).minute(),this->current_time->addSecs(+1).second());
        QString text = this->current_time->toString();
        ui->value_fligth_time->setText(text);
    }
}



/*void ControlPanel::incrementErrorsCounter()
{
    if (connect->connect_status)
    {
        error_counter++;
        ui->value_errors->setText(QString::number(error_counter));
    }
}*/



void ControlPanel::setInitialControlMode()
{
    ui->selection_mode->setCurrentIndex(3);
    swapControlPanel("Keyboard teleoperation");
}



/*void ControlPanel::updateStatusPanel()
{

    if (connect->connect_status){

        //Drone status
        switch(connect->usercommander->getDroneManagerStatus().status)
        {
        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD:
            ui->value_status->setText("Moving Manual Altitude");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_EMERGENCY:
            ui->value_status->setText("Moving Emergency");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY:
            ui->value_status->setText("Moving Trajectory");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_FLIP:
            ui->value_status->setText("Moving Flip");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_BACK:
            ui->value_status->setText("Moving Flip Back");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_FRONT:
            ui->value_status->setText("Moving Flip Front");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_LEFT:
            ui->value_status->setText("Moving Flip Left");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_FLIP_RIGHT:
            ui->value_status->setText("Moving Flip Right");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_THRUST:
            ui->value_status->setText("Moving Manual Thrust");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_POSITION:
            ui->value_status->setText("Moving Position");
            break;
        case droneMsgsROS::droneManagerStatus::MOVING_SPEED:
            ui->value_status->setText("Moving Speed");
            break;
        case droneMsgsROS::droneManagerStatus::EMERGENCY:
            ui->value_status->setText("Emergency");
            break;
        case droneMsgsROS::droneManagerStatus::HOVERING:
            ui->value_status->setText("Hovering");
            break;
        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            ui->value_status->setText("Hovering Visual Servoing");
            break;
        case droneMsgsROS::droneManagerStatus::LANDED:
            ui->value_status->setText("Landed");
            is_takenOff=false;
            break;
        case droneMsgsROS::droneManagerStatus::LANDING:
            ui->value_status->setText("Landing");
            break;
        case droneMsgsROS::droneManagerStatus::UNKNOWN:
            ui->value_status->setText("Unknow");
            break;
        case droneMsgsROS::droneManagerStatus::TAKINGOFF:
            ui->value_status->setText("Taking Off");
            is_takenOff=true;
            break;
        case droneMsgsROS::droneManagerStatus::SLEEPING:
            ui->value_status->setText("Sleeping");
            break;
        }

        if(connect->telemetry_receiver->battery_msgs.batteryPercent<=25.0&&connect->telemetry_receiver->battery_msgs.batteryPercent!=0)
        {
            QPalette* palette = new QPalette();
            palette->setColor(QPalette::WindowText,Qt::red);
            ui->value_battery->setPalette(*palette);
        }
        //Battery
        ui->value_battery->setText(QString::number(connect->telemetry_receiver->battery_msgs.batteryPercent) +  "%");

        //Connection
        if(connect->graph_receiver->is_wifi_connected)
            ui->value_wifi->setText("Connected");
        else
            ui->value_wifi->setText("Disconnected");


    }
}*/


/*void ControlPanel::updateAction(const QString action)
{
    ui->value_action->setText(action);
}*/


void ControlPanel::updateTask(const QString task)
{

    //Only show the last part of the task name. For example: Task full name: Searching/Stabilize only shows Stabilize.
    QString delimiter="/";
    int indice=task.lastIndexOf(delimiter);

    if(indice==-1)
    {
         ui->value_task->setText(task);
    }
    else
    {
        QString aux_task=task;
        aux_task.remove(0,indice+1);

        ui->value_task->setText(aux_task);
    }
}

void ControlPanel::updateMissionName(const QString mission_name)
{
    ui->value_mission->setText(mission_name);
    //QMessageBox::information(this,tr("Mission File Open"),"File opened successfully.");
}

void ControlPanel::displayMissionErrors(const std::vector<std::string> error_messages){
    ui->value_task->setText("-");

    QString message1="File has errors. Please fix these errors and try again.<br><br>";
    QString errors="";
    for (int j = 0; j < error_messages.size(); j++)
    {
      std::size_t pos = error_messages[j].find(".");
      errors=errors+QString::fromStdString(error_messages[j].substr(7,pos-6)+"<br>");
    }

    errors="<font color = red>"+errors+"</font>";

    QMessageBox msgBox;
    msgBox.setTextFormat(Qt::TextFormat::RichText);
    msgBox.setText(message1+errors);
    msgBox.exec();
}

void ControlPanel::displayMissionCompleted(const bool ack)
{
    QString completed="Mission completed successfully.";
    QString aborted="Mission aborted.";

    QMessageBox msgBox;
    msgBox.setTextFormat(Qt::TextFormat::RichText);

    if(ack)
      msgBox.setText(completed);
    else
      msgBox.setText(aborted);

    ui->abort_button->setVisible(false);
    ui->land_button_3->setVisible(false);
    ui->start_button->setVisible(true);
    ui->value_task->setText("-");

    if(ui->selection_mode->currentText()=="Guided by TML mission")
        connect->mission_planner_receiver->activateAutonomousMode();

    //msgBox.exec(); //Not shown because it's troublesome to close the popup every time.
}

void ControlPanel::setCurrentUAV()
{
    if(connect->rosnamespace.compare("/")!=0)
    {
        char output[10012];
        strncpy(output, connect->rosnamespace.c_str(), sizeof(output));
        output[sizeof(output) - 1] = 0;
        char* process_name = strtok(output, "/drone");
        int drone_id = atoi( process_name );
        ui->selection_vehicle->setText("drone"+QString::number(drone_id));
    }
}

void ControlPanel::onTakeOffButton()
{
    std::cout<<"Take Off pressed button"<<std::endl;
    if(active_self_localization_by_odometry)
    {
      ui->take_off_button_2->setVisible(false);
      ui->reset_button_2->setVisible(true);
      ui->land_button_2->setVisible(true);
      /*if (connect->connect_status)
      {
          connect->mission_planner_receiver->deactivateAutonomousMode();
          connect->usercommander->publish_takeoff();
      }*/
      connect->setMissionMode();
      droneMsgsROS::BehaviorSrv::Request msg;
      droneMsgsROS::BehaviorSrv::Response res;
      droneMsgsROS::BehaviorCommand behavior;
      behavior.name = "TAKE_OFF";
      msg.behavior = behavior;
      activate_behavior_srv.call(msg,res);
      if(!res.ack)
        std::cout << res.error_message << std::endl;
    }
    else 
    {
      QMessageBox::information(this,tr("Self localization not started"),"The SELF_LOCALIZE_BY_ODOMETRY behavior is not active. Please activate it by pressing L before taking off");           
    }
}

void ControlPanel::onLandButton()
{
    isExecuting = false;
    std::cout<<"Land pressed button"<<std::endl;
    ui->take_off_button_2->setVisible(true);
    ui->reset_button_2->setVisible(false);
    ui->land_button_2->setVisible(false);
    ui->start_python_button->setVisible(true);
    ui->abort_python_button->setVisible(false);
    ui->emergency_land_python_button->setVisible(false);
    ui->select_python_button->setVisible(true);
    /*if (connect->connect_status)
    {
        connect->mission_planner_receiver->deactivateAutonomousMode();
        connect->usercommander->publish_land();
    }*/

    connect->setShowMode();
    droneMsgsROS::BehaviorSrv::Request msg;
    droneMsgsROS::BehaviorSrv::Response res;
    droneMsgsROS::BehaviorCommand behavior;
    behavior.name = "LAND";
    msg.behavior = behavior;
    activate_behavior_srv.call(msg,res);
    if(!res.ack)
      std::cout << res.error_message << std::endl;
}

void ControlPanel::onHoverButton()
{
    std::cout<<"Hover pressed button"<<std::endl;
    if (connect->connect_status)
    {
        connect->mission_planner_receiver->deactivateAutonomousMode();
        connect->usercommander->publish_hover();
    }
}

/** UNUSED */
void ControlPanel::onEmergencyStopButton()
{
    isExecuting = false;
    std::cout<<"Emergency pressed button"<<std::endl;
    if (connect->connect_status)
    {
        connect->mission_planner_receiver->deactivateAutonomousMode();
        connect->usercommander->publish_emergency();
    }
}

/** UNUSED */
void ControlPanel::onLoopButton()
{
    std::cout<<"looping pressed button"<<std::endl;
    if (connect->connect_status)
    {
        connect->mission_planner_receiver->deactivateAutonomousMode();
        connect->usercommander->sendCommandForLooping();
        //ControlPanel::updateAction("FLIP");
    }
}

void ControlPanel::onResetCommandButton()
{
    /** NOTE:
    Shows strange behaviour when the drone has been ordered to rotate previously,
    and a stabilize command was not issued after the rotation.
   */
    isExecuting = false;
    std::cout<<"Reset pressed button"<<std::endl;
    if (connect->connect_status)
    {

        //connect->mission_planner_receiver->deactivateAutonomousMode();
        //connect->usercommander->publish_yaw_zero();connect->setShowMode();
      droneMsgsROS::BehaviorSrv::Request msg;
      droneMsgsROS::BehaviorSrv::Response res;
      droneMsgsROS::BehaviorCommand behavior;
      behavior.name = "ROTATE";
      behavior.arguments = "angle: 0";
      msg.behavior = behavior;
      activate_behavior_srv.call(msg,res);
      if(!res.ack)
        std::cout << res.error_message << std::endl;
    }
}

void ControlPanel::onStartMissionButton()
{
    //If a mission name was not received.
    isExecuting = true;
    if(ui->value_mission->text()=="-")
        QMessageBox::information(this,tr("Mission File Not Found"),"A mission specification file was not found.");
    else
    {
        if (connect->mission_planner_receiver->startMission())
        {
            ui->start_button->setVisible(false);
            ui->abort_button->setVisible(true);
            ui->land_button_3->setVisible(true);
            cout << "Mission started." << endl;
        }
        else
            QMessageBox::information(this,tr("Mission Not Started"),"ERROR. The mission could not be started.");
    }
    Q_EMIT startMission();
}


void ControlPanel::externalMissionStarted() {
    isExecuting = true;
    ui->selection_mode->setCurrentIndex(0);
    swapControlPanel("Guided by TML mission");
    if(ui->value_mission->text()=="-")
        QMessageBox::information(this,tr("Mission File Not Found"),"A mission specification file was not found.");
    else
    {
        ui->start_button->setVisible(false);
        ui->abort_button->setVisible(true);
        ui->land_button_3->setVisible(true);

        cout << "Mission started." << endl;
        Q_EMIT startMission();
    }
}

void ControlPanel::onAbortMissionButton()
{
        isExecuting = false;
        Q_EMIT operatorText("Abort mission");
        if (connect->mission_planner_receiver->abortMission())
        {
            ui->abort_button->setVisible(false);
            ui->land_button_3->setVisible(false);
            ui->start_button->setVisible(true);
            ui->value_task->setText("-");
            onHoverButton();

            cout << "Mission aborted." << endl;
        }
        else
            QMessageBox::information(this,tr("Mission Not Started"),"ERROR. The mission could not be aborted.");
}

void ControlPanel::onEmergencyLandButton()
{
    isExecuting = false;
    onAbortMissionButton();
    Q_EMIT operatorText("Land");
    onLandButton();

    cout << "Mission aborted. Land issued." << endl;

    connect->mission_planner_receiver->emitEmergencyLand();
}

void ControlPanel::sendCommand()
{
    droneMsgsROS::actionArguments arguments;

    QString command = ui->commands->currentText();

    if (command=="TAKE OFF")
    {
        Q_EMIT operatorText("Take off");
        connect->cooperative_receiver->sendCommand(command,arguments);
    }

    else if (command=="LAND")
    {
        Q_EMIT operatorText("Land");
        connect->cooperative_receiver->sendCommand(command,arguments);
    }

    else if (command=="WAIT")
    {
        arguments.argumentName=droneMsgsROS::actionArguments::DURATION;
        std::vector<double> argument (1,ui->time->value());
        arguments.value=argument;

        Q_EMIT operatorText("Wait for "+QString::number(ui->time->value())+" seconds.");
       connect-> cooperative_receiver->sendCommand(command,arguments);
    }

    else if (command=="GO HOME")
    {

        arguments.argumentName=droneMsgsROS::actionArguments::DESTINATION;
        int index= point_manager->searchPointName("HOME");
        if (index>=0)
        {
            std::vector<double> argument (3);
            argument[0]=point_list.at(index).x;
            argument[1]=point_list.at(index).y;
            argument[2]=point_list.at(index).z;
            arguments.value=argument;

            Q_EMIT operatorText("Go to point HOME");
            connect->cooperative_receiver->sendCommand(command,arguments);
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setTextFormat(Qt::TextFormat::RichText);
            msgBox.setText("Point HOME was not defined");
            msgBox.exec();
        }
    }

    else if (command=="SET HOME")
    {
        point home;
        home.name="HOME";
        home.x=connect->odometry_receiver->drone_pose_msgs.x;
        home.y=connect->odometry_receiver->drone_pose_msgs.y;
        home.z=connect->odometry_receiver->drone_pose_msgs.z;
        point_manager->setHome(home);
        Q_EMIT operatorText("Memorize point as HOME");
    }

    else if (command=="GO TO POINT")
    {
        arguments.argumentName=droneMsgsROS::actionArguments::DESTINATION;
        int index= ui->point_selection->currentIndex();
        if (index > -1)
        {
          std::vector<double> argument (3);
          argument[0]=point_list.at(index).x;
          argument[1]=point_list.at(index).y;
          argument[2]=point_list.at(index).z;
          arguments.value=argument; //Value of point selected

          Q_EMIT operatorText("Go to point "+ui->point_selection->currentText());
          connect->cooperative_receiver->sendCommand(command,arguments);
        }
    }
}

void ControlPanel::managePoints()
{
    //Opens a new window that allows the user to manage points for the GO TO POINT command.
    point_manager->show();
}

void ControlPanel::updatePoints(std::vector<struct point> points)
{
  point_list=points;
  ui->point_selection->clear();
  for(int i = 0; i<point_list.size();i++)
  {
    ui->point_selection->addItem(point_list.at(i).name);
  }
}


void ControlPanel::abortPythonMission() {
  isExecuting = false;
  connect->setShowMode();
  ui->start_python_button->setVisible(true);
  ui->abort_python_button->setVisible(false);
  ui->emergency_land_python_button->setVisible(false);
  ui->select_python_button->setVisible(true);
  std_srvs::Empty msg;
  ros::service::call(python_srv + "/stop",msg);
}

void ControlPanel::startPythonMission() {
  isExecuting = true;
  std_srvs::Empty msg;
  ros::service::call(python_srv + "/start",msg);
  ui->start_python_button->setVisible(false);
  ui->abort_python_button->setVisible(true);
  ui->emergency_land_python_button->setVisible(true);
  ui->select_python_button->setVisible(false);
  connect->setMissionMode();
}

void ControlPanel::selectPythonMission() {
  python_mission = QFileDialog::getOpenFileName(this,
        tr("Select python mission"), "",
        tr("Python file (*.py)")).toStdString();
  if (python_mission != "") {
    droneMsgsROS::openMissionFile msg;
    msg.request.mission_file_path = python_mission;
    ros::service::call(python_select_srv,msg);
  }
}

void ControlPanel::executeTreeMission() {
  isExecuting = true;
  ui->execute_tree_button->setVisible(false);
  ui->emergency_land_tree_button->setVisible(true);
  ui->abort_mission_tree_button->setVisible(true);
  Q_EMIT(executeTree());
}

void ControlPanel::cancelTreeMission() {
  isExecuting = false;
  ui->emergency_land_tree_button->setVisible(false);
  ui->abort_mission_tree_button->setVisible(false);
  ui->execute_tree_button->setVisible(true);
  Q_EMIT(cancelTree());
}

void ControlPanel::newBehaviorCallback(const droneMsgsROS::ListOfBehaviors &msg)
{
  if (msg.behaviors.size() != 0)
  {
    for (int i = 0; i < msg.behaviors.size(); i++)
    {
      if (msg.behaviors[i] == "TAKE_OFF")
      {
        this->current_time->setHMS(00,00,00);
        QString text = this->current_time->toString();
        ui->value_fligth_time->setText(text);
        is_takenOff = true;
      }
      else if (msg.behaviors[i] == "LAND")
      {
        is_takenOff = false;
      }
      else if (msg.behaviors[i] == "SELF_LOCALIZE_BY_ODOMETRY")
      {
        active_self_localization_by_odometry = true;
      }
    }
    
    if(connect->telemetry_receiver->battery_msgs.batteryPercent<=25.0&&connect->telemetry_receiver->battery_msgs.batteryPercent!=0)
    {
        QPalette* palette = new QPalette();
        palette->setColor(QPalette::WindowText,Qt::red);
        ui->value_battery->setPalette(*palette);
    }
    //Battery
    ui->value_battery->setText(QString::number(connect->telemetry_receiver->battery_msgs.batteryPercent) +  "%");

    //Connection
    if(connect->graph_receiver->is_wifi_connected)
        ui->value_wifi->setText("Connected");
    else
        ui->value_wifi->setText("Disconnected");
  }
}
