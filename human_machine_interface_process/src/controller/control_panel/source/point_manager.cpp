/*
  PointManager
  @author  Germán Quintero
  @date    03-2015
  @version 1.0
*/
#include "../include/point_manager.h"

PointManager::PointManager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PointManager)
{
  ui->setupUi(this);
  this->setWindowTitle("Point Manager");

  //Force the first column (Label) to be longer than the rest in the layout.
  ui->point_list->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);

  //Can't delete points from an empty list.
  ui->delete_point_button->setEnabled(false);

  //An empty pattern or a pattern of white space. This is to prevent empty point names.
  m_whiteSpace.setPattern("\\s*");

  setSignalHandlers();
}

PointManager::~PointManager()
{
  delete ui;
}

void PointManager::setSignalHandlers()
{
  QObject::connect(ui->add_point_button,SIGNAL(clicked()),this,SLOT(addPoint()));
  QObject::connect(ui->delete_point_button,SIGNAL(clicked()),this,SLOT(deletePoint()));
  QObject::connect(ui->point_list,SIGNAL(itemSelectionChanged()),this,SLOT(deleteStatus()));
}

bool PointManager::addPoint()
{
  point new_point;
  new_point.name=ui->point_name->text();
  new_point.x=ui->coordinate_x->value();
  new_point.y=ui->coordinate_y->value();
  new_point.z=ui->coordinate_z->value();

  //If point name is empty or full of white space.
  if (m_whiteSpace.exactMatch(new_point.name))
  {
      QMessageBox msgBox;
      msgBox.setTextFormat(Qt::TextFormat::RichText);
      msgBox.setText("Point name must not be empty.");
      msgBox.exec();
      return false;
  }

  //If point name is not in the list (new name)
  else if (searchPointName(new_point.name)<0)
  {
    point_list.push_back(new_point);

    int new_row=ui->point_list->rowCount();
    QTableWidgetItem* new_point_name=new QTableWidgetItem(new_point.name);
    QTableWidgetItem* new_point_x=new QTableWidgetItem(QString::number(new_point.x));
    QTableWidgetItem* new_point_y=new QTableWidgetItem(QString::number(new_point.y));
    QTableWidgetItem* new_point_z=new QTableWidgetItem(QString::number(new_point.z));
    new_point_x->setTextAlignment(Qt::AlignCenter);
    new_point_y->setTextAlignment(Qt::AlignCenter);
    new_point_z->setTextAlignment(Qt::AlignCenter);
    ui->point_list->insertRow(new_row);
    ui->point_list->setItem(new_row,0,new_point_name);
    ui->point_list->setItem(new_row,1,new_point_x);
    ui->point_list->setItem(new_row,2,new_point_y);
    ui->point_list->setItem(new_row,3,new_point_z);

    Q_EMIT updatePoints(point_list);
    return true;
  }

  //If point name is already in list
  else
  {
    QMessageBox msgBox;
    msgBox.setTextFormat(Qt::TextFormat::RichText);
    msgBox.setText("Point "+new_point.name+" already in list.");
    msgBox.exec();
    return false;
  }
}

void PointManager::setHome(const point home)
{
    int index=searchPointName("HOME");
    //If HOME has not been set
    if(index<0)
    {
        point_list.push_back(home);

        int new_row=ui->point_list->rowCount();
        QTableWidgetItem* home_name=new QTableWidgetItem(home.name);
        QTableWidgetItem* home_x=new QTableWidgetItem(QString::number(home.x));
        QTableWidgetItem* home_y=new QTableWidgetItem(QString::number(home.y));
        QTableWidgetItem* home_z=new QTableWidgetItem(QString::number(home.z));
        home_x->setTextAlignment(Qt::AlignCenter);
        home_y->setTextAlignment(Qt::AlignCenter);
        home_z->setTextAlignment(Qt::AlignCenter);
        ui->point_list->insertRow(new_row);
        ui->point_list->setItem(new_row,0,home_name);
        ui->point_list->setItem(new_row,1,home_x);
        ui->point_list->setItem(new_row,2,home_y);
        ui->point_list->setItem(new_row,3,home_z);

        Q_EMIT updatePoints(point_list);
    }

    //If HOME has been set
    else
    {
       //Reset HOME point in list

       //In list structure
       point_list.at(index).x=home.x;
       point_list.at(index).y=home.y;
       point_list.at(index).z=home.z;

       //In table
       QTableWidgetItem* home_x=new QTableWidgetItem(QString::number(home.x));
       QTableWidgetItem* home_y=new QTableWidgetItem(QString::number(home.y));
       QTableWidgetItem* home_z=new QTableWidgetItem(QString::number(home.z));
       home_x->setTextAlignment(Qt::AlignCenter);
       home_y->setTextAlignment(Qt::AlignCenter);
       home_z->setTextAlignment(Qt::AlignCenter);
       ui->point_list->setItem(index,1,home_x);
       ui->point_list->setItem(index,2,home_y);
       ui->point_list->setItem(index,3,home_z);

       Q_EMIT updatePoints(point_list);
    }
}

bool PointManager::deletePoint()
{
  int item_selected=ui->point_list->currentIndex().row();
  point_list.erase(point_list.begin()+item_selected);

  ui->point_list->removeRow(item_selected);

  Q_EMIT updatePoints(point_list);

}

int PointManager::searchPointName(QString point_name)
{
  int index=-1;
  bool found = false;
  for(int i=0; i<point_list.size() && !found ;i++)
  {
    if (QString::compare(point_list.at(i).name,point_name,Qt::CaseSensitive)==0)
    {
      index=i;
      found=true;
    }
  }
  return index;
}

void PointManager::deleteStatus()
{
  //Point must be selected to be deleted.
  if (ui->point_list->currentRow()<0)
    ui->delete_point_button->setEnabled(false);
  else
    ui->delete_point_button->setEnabled(true);
}
