/*
  ControlPanelView
  @author  Germán Quintero
  @date    03-2015
  @version 1.0
*/
#include "../include/control_panel_view.h"

ControlPanelView::ControlPanelView(QWidget *parent, Connection* connection) :
    QWidget(parent),
    ui(new Ui::ControlPanelView)
{
    ui->setupUi(this);
    //Add the control panel widget.
    controlPanel = new ControlPanel(this,connection);
    ui->grid_controlpanel->addWidget(controlPanel);
}

ControlPanelView::~ControlPanelView()
{
    delete ui;
    delete controlPanel;
}

ControlPanel* ControlPanelView::getControlPanel()
{
    return controlPanel;
}
