/*!*****************************************************************************
 *  \file      new_tab_window.h
 *  \brief     Definition of all the classes used in the file
 *             new_tab_window.cpp .
 * \details    Represents the windows that are generated when a tab
 *             from the tab manager is requested to pop out.
 *
 *  \author    German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/


#ifndef NEW_TAB_WINDOW_H
#define NEW_TAB_WINDOW_H

#include <QWidget>
#include <QCloseEvent>
#include <QGridLayout>
#include "ui_new_tab_window.h"

//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
  class NewTabWindow;
}

class NewTabWindow : public QWidget
{
  Q_OBJECT

public:
  //Constructor & Destructor
  explicit NewTabWindow(QWidget *parent = 0, QString window_title = 0,QWidget* widget = 0, int index = 0);
  ~NewTabWindow();

public:
  /*!************************************************************************
       *  \brief  This method returns the widget shown on the window.
       *  \return The widget shown on the window.
       *************************************************************************/
  QWidget* getWidget();
  int getIndex();
  void setIndex(int index);

Q_SIGNALS:
  /*!************************************************************************
       *  \brief  This signal is emitted when the window is closed.
       *  \param The window that was closed.
       *************************************************************************/
  void tabWindowClosed(NewTabWindow*);

  /*!************************************************************************
      *  \brief  This signal is emitted when the window is disabled.
      *  \param The window that was disabled.
  *************************************************************************/
  void tabWindowDisabled(NewTabWindow*);

protected:
  /*!************************************************************************
       *  \brief  This method processes the event where the window is closed.
       *  \param event The event detected when the window is closed.
       *************************************************************************/
  void closeEvent(QCloseEvent *event) override;

private:
  //Layout
  Ui::NewTabWindow *ui;

  //The widget on the window
  QWidget* widget;
  int index;

public Q_SLOTS:
  /*!************************************************************************
      *  \brief  This slot is executed to disable the window.
  *************************************************************************/
  void disable();
};

#endif // NEW_TAB_WINDOW_H
