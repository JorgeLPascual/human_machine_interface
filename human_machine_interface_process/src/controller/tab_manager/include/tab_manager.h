/*!*****************************************************************************
 *  \file      tab_manager.h
 *  \brief     Definition of all the classes used in the file
 *             tab_manager.cpp .
 *  \details   The widget that contains all the tabs and their respective widgets.
 *             In charge of setting the tab buttons for pop-up.
 *
 *  \author    German Quintero
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************/

#ifndef TAB_MANAGER_H
#define TAB_MANAGER_H

#include <QTabWidget>
#include <QSignalMapper>
#include <QPushButton>
#include <QLayout>
#include "new_tab_window.h"
#include "../../connection/include/connection.h"
#include "tab_manager.h"

#include "ui_tab_manager.h"
#include <QString>

//For using Qt-generated layout files from .ui files (from Qt Designer).
namespace Ui {
  class TabManager;
}

class TabManager : public QTabWidget
{
  Q_OBJECT

public:
  //Constructor & Destructor
  explicit TabManager(QWidget *parent = 0);
  ~TabManager();

public Q_SLOTS:
  /*!************************************************************************
    *  \brief  Shows the tab specified by its index in a new window.
    *  \param tab_index The index of the tab in the TabManager widget.
    *  \details Resets pop-up button connections and removes the tab from the manager.
    *  \see setPopUpButtons(), unsetPopUpButtons(), new_tab_window.cpp
    *************************************************************************/
  void popUpTab(int tab_index);

    /*!************************************************************************
    *  \brief  Shows the tab specified by its widget in a new window.
    *  \param widget The widget of the tab in the TabManager widget.
    *  \details Resets pop-up button connections and removes the tab from the manager.
    *  \see setPopUpButtons(), unsetPopUpButtons(), new_tab_window.cpp
    *************************************************************************/
  void popUpTabByWidget(QWidget * widget);

  /*!************************************************************************
    *  \brief Recovers the tab that was shown in a new window.
    *  \param window The pop-up window in which the tab was shown.
    *  \details Resets pop-up button connections and adds the tab to the manager. Also deletes window.
    *  \see setPopUpButtons(), unsetPopUpButtons(), new_tab_window.cpp
    *************************************************************************/
  void disableTab(NewTabWindow* window);
  /*!************************************************************************
    *  \brief Recovers and disable the tab that was shown in a new window.
    *  \param window The pop-up window in which the tab was shown.
    *  \details Resets pop-up button connections and adds the tab to the manager. Also deletes window and disable it.
    *************************************************************************/
  void recoverTab(NewTabWindow* window);

  /*!************************************************************************
    *  \brief Adds a new tab.
    *  \param widget The widget you want to insert as a tab.
    *  \param title The title of the tab.
    *  \details Adds a new tab to the QTabWidget with its respective display button.
    *************************************************************************/
  void customAddTab(QWidget * widget, std::string title);

  /*!************************************************************************
  *  \brief Removes a tab.
  *  \param index The index of the tab you want to remove.
  *  \details Removes the tab and removes the button associated to it.
  *************************************************************************/
  void customRemoveTab(int index);

  /*!************************************************************************
    *  \brief Inserts a new tab.
    *  \param index The index where you want to insert the tab..
    *  \param widget The widget you want to insert as a tab.
    *  \param title The title of the tab.
    *  \details Adds a new tab to the QTabWidget with its respective display button.
    *************************************************************************/
  void customInsertTab(int index, QWidget * widget, std::string title);

  /*!************************************************************************
    *  \brief Update all the tabs that should be showed
    *  \details After removing all the tabs, it shows all the tabs correctly
    *************************************************************************/
  void updateTabs();
  /*!************************************************************************
    *  \brief Removes the tabs and its buttons
    *  \details Deletes the buttons correctly and after that removes the tab
  *************************************************************************/
  void removeTabs();

  /*!************************************************************************
    *  \brief Removes the tab with the widget and its buttons
    *  \details Deletes the buttons correctly and after that removes the tab. Also it saves the index
  *************************************************************************/
  void maximizeWidget(QWidget * widget);

  /*!************************************************************************
    *  \brief Recover the tab and its buttons
    *  \details Recover the tab in the position of the widget maximized 
  *************************************************************************/
  void minimizeWidget(QWidget * widget);

private:

  struct Tab {
    QWidget * widget;
    std::string title;
    int index;
  };
  std::vector<Tab> tabs;
  //Layout
  Ui::TabManager *ui;
  Connection * connection;
  /*PerformanceMonitorViewer *process_view;
  ParameterTemporalSeries *param_plot;
  ControlPanel *controlpanel;
  SkillViewer *skillviewer;
  EnvironmentPerception *drone_position;
  OperatorDialogue *operator_dialogue;
  */
  //Mapper that maps the clicked() signal of the tab buttons to clicked(tab_index).
  QSignalMapper *signalMapper;

  //List of tab_buttons
  std::vector<QPushButton*> tab_buttons;
  std::vector<NewTabWindow*> deployed_tabs;

  int index_maximized;
  std::string title_maximized;

  std::vector<QWidget*> to_deploy_after_minimize;

Q_SIGNALS:
  void maximizeTab(QWidget*);
  void disableAll();

};

#endif // TAB_MANAGER_H
