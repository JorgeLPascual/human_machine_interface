#include "../include/tab_manager.h"



TabManager::TabManager(QWidget *parent) :
    QTabWidget(parent),
    ui(new Ui::TabManager)
{
    ui->setupUi(this);

    //Mapper to map signals
    signalMapper = new QSignalMapper(this);
    QObject::connect(signalMapper,SIGNAL(mapped(const int)),this,SLOT(popUpTab(int)));
}

TabManager::~TabManager()
{
    delete ui;
    removeTabs();
    tab_buttons.clear();
}


void TabManager::recoverTab(NewTabWindow *window)
{   
    QWidget* tab_recovered = window->getWidget();
    this->customInsertTab(window->getIndex(),tab_recovered,window->windowTitle().toUtf8().constData());
    delete window;
}

void TabManager::disableTab(NewTabWindow *window)
{   
    QWidget* tab_recovered = window->getWidget();
    this->customInsertTab(window->getIndex(),tab_recovered,window->windowTitle().toUtf8().constData());
    to_deploy_after_minimize.push_back(window->getWidget());
    updateTabs();
    delete window;
}

void TabManager::popUpTab(int tab_index)
{
    //Tab to be shown in pop-up window.
    QWidget* rippedOffTab = tabs[tab_index].widget;
    QString window_title = QString::fromStdString(tabs[tab_index].title);
    int index = tabs[tab_index].index;
    this->customRemoveTab(tab_index);
    //Pop-up window
    NewTabWindow* newWindow = new NewTabWindow(0,window_title,rippedOffTab,index);
    newWindow->show();
    //Recover the tab when the pop-up window is closed.
    QObject::connect(newWindow,SIGNAL(tabWindowClosed(NewTabWindow*)),this,SLOT(recoverTab(NewTabWindow*)));
    QObject::connect(this,SIGNAL(disableAll()),newWindow,SLOT(disable()));
    QObject::connect(newWindow,SIGNAL(tabWindowDisabled(NewTabWindow*)),this,SLOT(disableTab(NewTabWindow*)));
}

void TabManager::popUpTabByWidget(QWidget * widget)
{
    //Tab to be shown in pop-up window.
    int tab_index = 0;
    for (int i = 0; i < tabs.size(); i++) {
        if (widget == tabs[i].widget) {
            tab_index = i;
            break;
        }
    }
    QWidget* rippedOffTab = tabs[tab_index].widget;
    QString window_title = QString::fromStdString(tabs[tab_index].title);
    int index = tabs[tab_index].index;
    this->customRemoveTab(tab_index);
    //Pop-up window
    NewTabWindow* newWindow = new NewTabWindow(0,window_title,rippedOffTab,index);
    newWindow->show();
    //Recover the tab when the pop-up window is closed.
    QObject::connect(newWindow,SIGNAL(tabWindowClosed(NewTabWindow*)),this,SLOT(recoverTab(NewTabWindow*)));
    QObject::connect(this,SIGNAL(disableAll()),newWindow,SLOT(disable()));
    QObject::connect(newWindow,SIGNAL(tabWindowDisabled(NewTabWindow*)),this,SLOT(disableTab(NewTabWindow*)));
}


void TabManager::customAddTab(QWidget * widget, std::string title) {
    Tab tab;
    tab.widget = widget;
    tab.title = title;
    tab.index = tabBar()->count();
    tabs.push_back(tab);
    updateTabs();
}

void TabManager::customRemoveTab(int index) {
    std::vector<Tab>::iterator it;
    it = tabs.begin() + index;
    tabs.erase(it);
    updateTabs();
}

void TabManager::customInsertTab(int index, QWidget * widget, std::string title) {
    std::vector<Tab>::iterator it;
    Tab tab;
    tab.widget = widget;
    tab.title = title;
    tab.index = index;
    int real_pos = 0;
    for (int i = tabs.size()-1; i >= 0 && real_pos == 0; i--) {
        if (tabs[i].index < index) {
            real_pos = i+1;
        }
    }
    it = tabs.begin() + real_pos;
    tabs.insert(it,tab);
    updateTabs();
}

void TabManager::updateTabs() {
    removeTabs();
    for (int i = 0; i < tabs.size(); i++) {
        this->addTab(tabs[i].widget,QString::fromStdString(tabs[i].title));
        QPushButton* button = new QPushButton();
        tabBar()->setTabButton(i, QTabBar::RightSide,((QWidget*)(button)));
        QObject::connect(button,SIGNAL(clicked()),signalMapper,SLOT(map()));
        signalMapper->setMapping(button,i);
        button->setFixedWidth(20);
        button->setIcon(this->style()->standardIcon(QStyle::SP_ToolBarVerticalExtensionButton));
        tab_buttons.push_back(button);
    }
    if (tabBar()->count() == 0) {
        std::cout << tabBar()->count() << " Hiding" << std::endl;
        this->hide();
    }
    else {
        this->show();
    }
}

void TabManager::removeTabs() {
    int total = tabBar()->count();
    for (int i = 0; i < total; i++) {
        QPushButton* button = tab_buttons[0];
        QObject::disconnect(button,SIGNAL(clicked()),signalMapper,SLOT(map()));
        signalMapper->removeMappings(button);
        tabBar()->setTabButton(0, QTabBar::RightSide,0);
        std::vector<QPushButton*>::iterator it;
        it = tab_buttons.begin();
        delete tab_buttons[0];
        tab_buttons.erase(it);
        removeTab(0);
    }
    tab_buttons.clear();
}

void TabManager::maximizeWidget(QWidget * widget) {
    bool found = false;
    Q_EMIT(disableAll());
    int total = tabBar()->count();
    for (int i = 0; i < total && !found; i++) {
        if (widget == tabs[i].widget) {
            found = true;
            index_maximized = tabs[i].index;
            title_maximized = tabs[i].title;
            customRemoveTab(index_maximized);
            Q_EMIT(maximizeTab(widget));
        }
    }
}

void TabManager::minimizeWidget(QWidget * widget) {
    customInsertTab(index_maximized,widget,title_maximized);
    for (int i = 0; i < to_deploy_after_minimize.size(); i++) {
        popUpTabByWidget(to_deploy_after_minimize[i]);
    }
    to_deploy_after_minimize.clear();
}