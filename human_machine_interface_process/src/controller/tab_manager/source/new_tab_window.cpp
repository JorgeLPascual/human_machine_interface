#include "../include/new_tab_window.h"


NewTabWindow::NewTabWindow(QWidget *parent, QString window_title, QWidget *widget, int index) :
    QWidget(parent),
    ui(new Ui::NewTabWindow)
{
    this->widget=widget;
    ui->setupUi(this);
    this->resize(800,600);
    this->setWindowTitle(window_title);
    ui->grid_layout->addWidget(widget);
    widget->show();
    this->index = index;
}

NewTabWindow::~NewTabWindow()
{
    delete ui;
}


void NewTabWindow::closeEvent(QCloseEvent *event)
{
    Q_EMIT tabWindowClosed(this);
    QWidget::closeEvent(event);
}

QWidget* NewTabWindow::getWidget()
{
    return widget;
}

int NewTabWindow::getIndex() {
    return index;
}

void NewTabWindow::setIndex(int index) {
    this->index = index;
}

void NewTabWindow::disable() {
    Q_EMIT tabWindowDisabled(this);
}