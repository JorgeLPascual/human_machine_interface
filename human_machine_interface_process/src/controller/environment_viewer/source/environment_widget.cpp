/*
  EnvironmentWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/environment_widget.h"


EnvironmentWidget::EnvironmentWidget(QWidget *parent, Connection* connection, OdometryStateReceiver * odometry_receiver,
        ObstaclesReceiver * obstacles_receiver, MissionStateReceiver * mission_receiver,
        TrajectoryAbsRefCommandReceiver * command_receiver, SocietyPoseReceiver * society_receiver) :
    QWidget(parent),
    ui(new Ui::EnvironmentWidget)
{
    std::locale::global( std::locale::classic() );
    ui->setupUi(this);
    n.param<std::string>("my_stack_workspace", my_stack_workspace,
     "~/workspace/ros/aerostack_catkin_ws");
    layout = ui->gridLayout;
    tool_widget = new ToolWidget(this);
    layout->addWidget(tool_widget,3,0,Qt::AlignLeft);
    object_controller = new ObjectController(this);
    object_controller->setToolWidget(tool_widget);
    object_label = new QLabel("Map");
    layout->addWidget(object_label,0,0);
    tools_label = new QLabel("Tools");
    layout->addWidget(tools_label,2,0);
    layout->addWidget(object_controller,1,0,1,7);


    QObject::connect(object_controller, SIGNAL(createDroneMenu(ObjectController::Drone,int)),
        this, SLOT(createDroneMenu(ObjectController::Drone, int)));
    QObject::connect(object_controller, SIGNAL(createWallMenu(ObjectController::Wall,int)),
        this, SLOT(createWallMenu(ObjectController::Wall, int)));
    QObject::connect(object_controller, SIGNAL(createPoleMenu(ObjectController::Pole,int)),
        this, SLOT(createPoleMenu(ObjectController::Pole, int)));
    QObject::connect(object_controller, SIGNAL(createLandmarkMenu(ObjectController::Landmark,int)),
        this, SLOT(createLandmarkMenu(ObjectController::Landmark, int)));
    QObject::connect(object_controller, SIGNAL(unselect()), this, SLOT(deleteMenu()));
    config_file_manager = new ConfigFileManager();
    received_data_processor = new ReceivedDataProcessor(object_controller, odometry_receiver,obstacles_receiver,mission_receiver,
        command_receiver, society_receiver);


    edit_button = new QPushButton("Edit Map");
    new_map_button = new QPushButton("New");
    load_map_button = new QPushButton("Import");
    save_map_button = new QPushButton("Export");
    //import_map_button = new QPushButton("Open");
    //export_map_button = new QPushButton("Save");
    cancel_map_button = new QPushButton("Cancel");
    accept_map_button = new QPushButton("Accept");
    clear_button = new QPushButton("Clear");

    layout->addWidget(edit_button,3,6,1,1);
    layout->addWidget(clear_button,3,6,1,1);

    layout_buttons = new QGridLayout(0);
    layout_buttons->addWidget(new_map_button,0,0);
    layout_buttons->addWidget(load_map_button,0,1);
    layout_buttons->addWidget(save_map_button,0,2);
    //layout_buttons->addWidget(import_map_button,0,3);
    //layout_buttons->addWidget(export_map_button,0,4);
    layout_buttons->addWidget(cancel_map_button,0,3);
    layout_buttons->addWidget(accept_map_button,0,4);

    layout->addLayout(layout_buttons,3,4,1,3);

    //Temporal solution to avoid errors while changing the widget
    QObject::connect(new_map_button, SIGNAL(clicked()), this, SLOT(createNewMapMenu()));
    //QObject::connect(export_map_button, SIGNAL(clicked()), this, SLOT(exportMap()));
    QObject::connect(accept_map_button, SIGNAL(clicked()), this, SLOT(exportMap()));
    QObject::connect(save_map_button, SIGNAL(clicked()), this, SLOT(saveMap()));
    QObject::connect(load_map_button, SIGNAL(clicked()), this, SLOT(loadMap()));
    //QObject::connect(import_map_button, SIGNAL(clicked()), this, SLOT(importMap()));
    QObject::connect(edit_button, SIGNAL(clicked()), this, SLOT(editMap()));
    QObject::connect(cancel_map_button, SIGNAL(clicked()), this, SLOT(cancelMap()));
    QObject::connect(clear_button, SIGNAL(clicked()), this, SLOT(clearTrajectories()));
    setShowMode();

    connect(mission_receiver, SIGNAL(commonMissionStarted()), this, SLOT(setMissionMode()));
    connect(mission_receiver, SIGNAL(commonMissionCompleted()), this, SLOT(setShowMode()));
    connect(mission_receiver, SIGNAL(commonMissionCompleted()), this, SLOT(setShowMode()));
    importMap();
}

EnvironmentWidget::~EnvironmentWidget()
{
    delete ui;
    delete object_controller;
    delete layout;
    delete config_file_manager;
    delete tool_widget;
}

void EnvironmentWidget::createNewMapMenu() {
    new_map_menu = new NewMapMenu(0,object_controller);
    new_map_menu->setAttribute(Qt::WA_DeleteOnClose, true);
    new_map_menu->show();
}

void EnvironmentWidget::createDroneMenu(ObjectController::Drone drone, int i) {
    if (!null) widget_config->close();
    DroneWidget * drone_widget = new DroneWidget(this,drone,i);
    drone_widget->setAttribute(Qt::WA_DeleteOnClose, true);

    layout->addWidget(object_controller,1,0,1,5);
    layout->addWidget(drone_widget,1,5,1,2);


    widget_config = drone_widget;
    QObject::connect(drone_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
    QObject::connect(drone_widget, SIGNAL(droneChanged(ObjectController::Drone, int)), object_controller,
        SLOT(changeDrone(ObjectController::Drone, int)));
    QObject::connect(drone_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
    null = false;
}

void EnvironmentWidget::createWallMenu(ObjectController::Wall w, int i) {
    if (!null) widget_config->close();
    WallWidget * wall_widget = new WallWidget(this,w,i);
    wall_widget->setAttribute(Qt::WA_DeleteOnClose, true);

    layout->addWidget(object_controller,1,0,1,5);
    layout->addWidget(wall_widget,1,5,1,2);

    widget_config = wall_widget;
    QObject::connect(wall_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
    QObject::connect(wall_widget, SIGNAL(wallChanged(ObjectController::Wall, int)), object_controller,
        SLOT(changeWall(ObjectController::Wall, int)));
    QObject::connect(wall_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
    null = false;
}

void EnvironmentWidget::createPoleMenu(ObjectController::Pole p, int i) {
    if (!null) widget_config->close();
    PoleWidget * pole_widget = new PoleWidget(this,p,i);
    pole_widget->setAttribute(Qt::WA_DeleteOnClose, true);

    layout->addWidget(object_controller,1,0,1,5);
    layout->addWidget(pole_widget,1,5,1,2);

    widget_config = pole_widget;
    QObject::connect(pole_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
    QObject::connect(pole_widget, SIGNAL(poleChanged(ObjectController::Pole, int)), object_controller,
        SLOT(changePole(ObjectController::Pole, int)));
    QObject::connect(pole_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
    null = false;
}

void EnvironmentWidget::createLandmarkMenu(ObjectController::Landmark land, int i) {
    if (!null) widget_config->close();
    LandmarkWidget * landmark_widget = new LandmarkWidget(this,land,i);
    landmark_widget->setAttribute(Qt::WA_DeleteOnClose, true);

    layout->addWidget(object_controller,1,0,1,5);
    layout->addWidget(landmark_widget,1,5,1,2);

    widget_config = landmark_widget;
    QObject::connect(landmark_widget, SIGNAL(closed()), this, SLOT(deleteMenu()));
    QObject::connect(landmark_widget, SIGNAL(landmarkChanged(ObjectController::Landmark, int)), object_controller,
        SLOT(changeLandmark(ObjectController::Landmark, int)));
    QObject::connect(landmark_widget, SIGNAL(closed()), object_controller, SLOT(unselectItem()));
    null = false;
}

void EnvironmentWidget::deleteMenu() {
    if (!null) widget_config->close();
    null = true;
    layout->addWidget(object_controller,1,0,1,7);
}

void EnvironmentWidget::saveMap() {
    QString filename = QFileDialog::getSaveFileName(this,tr("Save File"),my_stack_workspace.c_str(),"XML File (*.xml)");
    std::string filestd = filename.toStdString();
    if (filestd.find(".xml") == std::string::npos)
        filestd = filestd + ".xml";
    ObjectController::Objects objects = object_controller->getObjects();
    config_file_manager->saveObjects(filestd,objects);
}

void EnvironmentWidget::loadMap() {
    QString filename = QFileDialog::getOpenFileName(this,tr("Open File"),my_stack_workspace.c_str(),"XML File (*.xml)");
    if (filename != "") {
        ObjectController::Objects objects;
        objects = config_file_manager->loadObjects(filename.toStdString());
        object_controller->setObjects(objects);
    }
}

void EnvironmentWidget::exportMap() {
    bool res = config_file_manager->exportObjects(object_controller);
    if (res) {
        QMessageBox::about(this, tr("Restart Aerostack"),
        tr("<p>Changes will not be avaliable until the restart of Aerostack. You must restart the program.</p>"));
        object_controller->loadStatus();
        setShowMode();
        received_data_processor->setShowMode();
        deleteMenu();
        Q_EMIT(showing(this));
    }
}

void EnvironmentWidget::importMap() {
    ObjectController::Objects objects;
    objects = config_file_manager->importObjects();
    object_controller->setObjects(objects);
}

void EnvironmentWidget::setShowMode() {
    object_label -> setText("Map");
    tool_widget->hide();
    tools_label->hide();
    new_map_button->hide();
    save_map_button->hide();
    load_map_button->hide();
    //import_map_button->hide();
    //export_map_button->hide();
    cancel_map_button->hide();
    accept_map_button->hide();
    edit_button->show();
    clear_button->hide();
    tool_widget->unsetTool();
    object_controller->setMissionMode(false);
    received_data_processor->setShowMode();
    setFocusPolicy(Qt::NoFocus);
    object_controller->setFocusPolicy(Qt::NoFocus);
}

void EnvironmentWidget::setEditMode() {
    object_label -> setText("Map (Edition mode)");
    tool_widget->show();
    tools_label->show();
    new_map_button->show();
    save_map_button->show();
    load_map_button->show();
    //import_map_button->show();
    //export_map_button->show();
    cancel_map_button->show();
    accept_map_button->show();
    edit_button->hide();
    clear_button->hide();
    object_controller->setMissionMode(false);
}

void EnvironmentWidget::editMap() {
    object_controller->saveStatus();
    importMap();
    setEditMode();
    Q_EMIT(editing(this));
    setFocusPolicy(Qt::NoFocus);
    object_controller->setFocusPolicy(Qt::StrongFocus);
    object_controller->setFocus();
}

void EnvironmentWidget::cancelMap() {
    object_controller->loadStatus();
    setShowMode();
    deleteMenu();
    Q_EMIT(showing(this));
}

void EnvironmentWidget::setMissionMode() {
    clear_button->show();
    object_controller->setMissionMode(true);
    received_data_processor->setMissionMode();
    setFocusPolicy(Qt::NoFocus);
}

void EnvironmentWidget::clearTrajectories() {
    object_controller->clearTrajectories();
}

void EnvironmentWidget::editButton(bool b) {
    if(!b) {
        edit_button->hide();
    }
    else {
        edit_button->show();
    }
}
