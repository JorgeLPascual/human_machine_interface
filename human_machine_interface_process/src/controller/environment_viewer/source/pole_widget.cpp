/*
  PoleWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/pole_widget.h"

PoleWidget::PoleWidget(QWidget * parent, ObjectController::Pole pole, int i) : QWidget(parent), 
	ui(new Ui::PoleWidget) {

	ui->setupUi(this);
	this->pole = pole;
	this->internal_id = i;
	spin_id = ui->spin_id;
	spin_x_size = ui->spin_x_size;
	spin_y_size = ui->spin_y_size;
	spin_x_coor = ui->spin_x_coor;
	spin_y_coor = ui->spin_y_coor;
	spin_aruco0_id = ui->spin_aruco0_id;
	spin_aruco1_id = ui->spin_aruco1_id;
	spin_aruco2_id = ui->spin_aruco2_id;
	spin_aruco3_id = ui->spin_aruco3_id;
	accept = ui->accept;

	spin_id->setValue(pole.id);
	spin_x_size->setValue(pole.x_size);
	spin_y_size->setValue(pole.y_size);
	spin_x_coor->setValue(pole.x_coor);
	spin_y_coor->setValue(pole.y_coor);

	spin_aruco0_id->setValue(pole.aruco[0].id);
	spin_aruco1_id->setValue(pole.aruco[1].id);
	spin_aruco2_id->setValue(pole.aruco[2].id);
	spin_aruco3_id->setValue(pole.aruco[3].id);

	QObject::connect(accept, SIGNAL(clicked()),this,SLOT(acceptButton()));
	QObject::connect(spin_id, SIGNAL(valueChanged(int)),this,SLOT(valueChanged()));
	QObject::connect(spin_x_size, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_y_size, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_x_coor, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_y_coor, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_aruco0_id, SIGNAL(valueChanged(int)),this,SLOT(valueChanged()));
	QObject::connect(spin_aruco1_id, SIGNAL(valueChanged(int)),this,SLOT(valueChanged()));
	QObject::connect(spin_aruco2_id, SIGNAL(valueChanged(int)),this,SLOT(valueChanged()));
	QObject::connect(spin_aruco3_id, SIGNAL(valueChanged(int)),this,SLOT(valueChanged()));

} 

PoleWidget::~PoleWidget() {
	delete ui;
}

void PoleWidget::acceptButton() {
	Q_EMIT(closed());
	close();
}

void PoleWidget::valueChanged() {
	ObjectController::Pole pole = this->pole;
	pole.id = spin_id->value();
	pole.x_size = spin_x_size->value();
	pole.y_size = spin_y_size->value();
	pole.x_coor = spin_x_coor->value();
	pole.y_coor = spin_y_coor->value();
	pole.aruco[0].id = spin_aruco0_id->value();
	pole.aruco[1].id = spin_aruco1_id->value();
	pole.aruco[2].id = spin_aruco2_id->value();
	pole.aruco[3].id = spin_aruco3_id->value();
	Q_EMIT(poleChanged(pole,this->internal_id));
}