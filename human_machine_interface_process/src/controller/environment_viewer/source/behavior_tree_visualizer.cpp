
/*
  BehaviorTreeVisualizer
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/behavior_tree_visualizer.h"

BehaviorTreeVisualizer::BehaviorTreeVisualizer(QWidget *parent) : QWidget(parent), ui(new Ui::BehaviorTreeVisualizer)
{
  editing = false;
  ui->setupUi(this);
  my_layout = ui->gridLayout;
  variables_widget = new QWidget(this);
  variables_layout = new QGridLayout(variables_widget);
  expand_text_button = new QCheckBox("Collapse Text",this);
  edit_button = new QPushButton("Edit Mission");
  save_button = new QPushButton("Export");
  load_button = new QPushButton("Import");
  new_button = new QPushButton("New");
  cancel_button = new QPushButton("Cancel");
  accept_button = new QPushButton("Accept");

  /*edit_button->resize(0,0);
  save_button->resize(0,0);
  accept_button->resize(0,0);
  new_button->resize(0,0);
  load_button->resize(0,0);
  cancel_button->resize(0,0);*/

  variables_widget->setLayout(variables_layout);
  this->QWidget::setWindowTitle(QString::fromStdString("Behavior Tree"));
  tree = new BehaviorTree(this);

  visualized_tree = tree;
  tree_label = new QLabel("Behavior Tree");
  beliefs_label = new QLabel("Variables");
  beliefs_text = new QTextEdit(this);
  variables_layout->addWidget(expand_text_button,0,0,1,1,Qt::AlignLeft);
  variables_layout->addWidget(beliefs_label,1,0,1,1,Qt::AlignLeft);
  variables_layout->addWidget(beliefs_text,2,0,1,6);
  my_layout->addWidget(tree_label);
  //my_layout->addWidget(tree);
  splitter = new QSplitter(Qt::Vertical, this);
  my_layout->addWidget(splitter,1,0,1,6);
  splitter->insertWidget(0, tree);
  splitter->insertWidget(1, variables_widget);
  /*my_layout->addWidget(edit_button,3,0,1,5,Qt::AlignRight);
  my_layout->addWidget(cancel_button,3,0,1,2,Qt::AlignHCenter);
  my_layout->addWidget(new_button,3,2,1,2,Qt::AlignHCenter);
  my_layout->addWidget(load_button,3,4,1,2,Qt::AlignHCenter);
  my_layout->addWidget(save_button,3,6,1,2,Qt::AlignHCenter);
  my_layout->addWidget(accept_button,3,8,1,2,Qt::AlignHCenter);*/
  //my_layout->addWidget(beliefs_text);
  my_layout->addWidget(edit_button,3,5,1,1,Qt::AlignRight);
  variables_layout->addWidget(new_button);
  variables_layout->addWidget(load_button);
  variables_layout->addWidget(save_button);
  variables_layout->addWidget(cancel_button);
  variables_layout->addWidget(accept_button);

  edit_button->show();
  new_button->hide();
  accept_button->hide();
  save_button->hide();
  load_button->hide();
  cancel_button->hide();

  connect(expand_text_button, SIGNAL(stateChanged(int)), visualized_tree, SLOT(expandTreeText(int)));
  connect(this, SIGNAL(executeTreeSignal()), visualized_tree, SLOT(executeTree()));
  connect(this, SIGNAL(cancelTreeSignal()), visualized_tree, SLOT(cancelTree()));

  connect(edit_button, SIGNAL(clicked()), this, SLOT(sendEditModeChange()));
  connect(accept_button, SIGNAL(clicked()), this, SLOT(sendEditModeChange()));
  connect(new_button, SIGNAL(clicked()), visualized_tree, SLOT(createMissionDialog()));
  connect(load_button, SIGNAL(clicked()), this, SLOT(loadTreeFile()));
  connect(save_button, SIGNAL(clicked()), this, SLOT(saveTreeFile()));
  connect(cancel_button, SIGNAL(clicked()), this, SLOT(cancelTreeEdition()));

}

BehaviorTreeVisualizer::~BehaviorTreeVisualizer()
{
  delete tree_label;
  delete beliefs_label;
  delete beliefs_text;
  delete tree;
  delete variables_widget;
  delete splitter;
  delete ui;
}

void BehaviorTreeVisualizer::setText(std::string texto)
{
  text = QString(texto.c_str());
}

std::string BehaviorTreeVisualizer::getText()
{
  return beliefs_text->toPlainText().toUtf8().constData();
}

void BehaviorTreeVisualizer::startBlockingTextInput()
{
  Q_EMIT(missionStarted());
  beliefs_text->setReadOnly(true);
  edit_button->hide();
}

void BehaviorTreeVisualizer::stopBlockingTextInput()
{
  beliefs_text->setReadOnly(false);
  edit_button->show();
  Q_EMIT(missionFinished());
}

void BehaviorTreeVisualizer::update() {
  beliefs_text->setText(text);
}

void BehaviorTreeVisualizer::executeTree() {
  Q_EMIT(executeTreeSignal());
}

void BehaviorTreeVisualizer::cancelTree() {
  Q_EMIT(cancelTreeSignal());
}

void BehaviorTreeVisualizer::cancelTreeEdition() {
  visualized_tree->createMissionByTreeItem(visualized_tree->copyBehaviorTree(original_tree_copy,true,0));
  if (original_tree_copy != 0) {
    delete original_tree_copy;
  }
  sendEditModeChange();
}

void BehaviorTreeVisualizer::toggleEditMode() {
  editing = !editing;
  visualized_tree->setEditMode(editing);
  if (editing) {
    edit_button->hide();
    new_button->show();
    accept_button->show();
    save_button->show();
    load_button->show();
    cancel_button->show();
  }
  else {
    edit_button->show();
    new_button->hide();
    accept_button->hide();
    save_button->hide();
    load_button->hide();
    cancel_button->hide();
  }
}

void BehaviorTreeVisualizer::sendEditModeChange() {
  if (!editing) {
    Q_EMIT(setEditMode(true));
    tree_label -> setText("Behavior Tree (Edition Mode)");
    beliefs_label -> setText("Variables (Edition Mode)");
    original_tree_copy = visualized_tree->copyBehaviorTree(visualized_tree->getRoot(), true, 0);
  }
  else {
    Q_EMIT(setEditMode(false));
    tree_label -> setText("Behavior Tree");
    beliefs_label -> setText("Variables");
  }
}


void BehaviorTreeVisualizer::loadTreeFile() {
  QString filename = QFileDialog::getOpenFileName(this,tr("Open File"),"/home","YAML File (*.yaml)");
  if (filename != "") {
    TreeFileManager tfm;
    TreeItem * root_node = tfm.loadTree(filename.toStdString(),beliefs_text);
    if (root_node != nullptr) {
      visualized_tree->createMissionByTreeItem(root_node);
    }
  }
}


void BehaviorTreeVisualizer::saveTreeFile() {
  QString filename = QFileDialog::getSaveFileName(this,tr("Save File"),"/home","YAML File (*.yaml)");
  std::string filestd = filename.toStdString();
  if (filestd != "") {
    if (filestd.find(".yaml") == std::string::npos) {
      filestd = filestd + ".yaml";
    }
    TreeFileManager tfm;
    tfm.saveTree(visualized_tree->getRoot(),getText(),filestd);
  }
}
