/*
  ReceivedDataProcessor
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/received_data_processor.h"

ReceivedDataProcessor::ReceivedDataProcessor(ObjectController * object_controller, OdometryStateReceiver * odometry_receiver,
        ObstaclesReceiver * obstacles_receiver, MissionStateReceiver * mission_receiver,
        TrajectoryAbsRefCommandReceiver * command_receiver, SocietyPoseReceiver * society_receiver) : QObject() {

    this->odometry_receiver = odometry_receiver;
    this->obstacles_receiver = obstacles_receiver;
    this->mission_receiver = mission_receiver;
    this->command_receiver = command_receiver;
    this->object_controller = object_controller;
    this->society_receiver = society_receiver;
    mission_state = false;

    n.param<std::string>("drone_id_namespace", drone_id_namespace,
     "drone1");

    connect(this->odometry_receiver, SIGNAL(updateDronePosition()), this, SLOT(updateDrone()));
    connect(this->obstacles_receiver, SIGNAL(updateMapInfo()), this, SLOT(updateObstacles()));
    connect(this->command_receiver, SIGNAL(sendTrajectory()), this, SLOT(updateTrajectory()));
    connect(this->society_receiver, SIGNAL(poseReceived()), this, SLOT(updateDronesTrajectories()));

    connect(mission_receiver, SIGNAL(commonMissionStarted()), this, SLOT(setMissionMode()));
    connect(mission_receiver, SIGNAL(commonMissionCompleted()), this, SLOT(setShowMode()));
    connect(mission_receiver, SIGNAL(commonMissionCompleted()), this, SLOT(setShowMode())); // ??
}

ReceivedDataProcessor::~ReceivedDataProcessor() {
}

void ReceivedDataProcessor::updateDrone() {
    if (mission_state) {
        droneMsgsROS::dronePose msg = odometry_receiver->drone_pose_msgs;
        ObjectController::Objects objects = object_controller->getObjects();

        ObjectController::Drone drone;
        drone = objects.drones[0];
        drone.x_coor = msg.x;
        drone.y_coor = msg.y;
        drone.degrees = angles::to_degrees(msg.yaw);

        ObjectController::Point point;
        point.x = msg.x;
        point.y = msg.y;
        if (drone.points.size() > 0) {
            ObjectController::Point last_point = drone.points.at(drone.points.size()-1);
            if (sqrt(pow((last_point.x - point.x),2)+pow((last_point.y - point.y),2)) > precision) {
                drone.points.push_back(point);
            }
        } 
        else {
          if(point.x != 0 && point.y != 0)
            drone.points.push_back(point);
        }
        object_controller->updateDrone(drone,0);
    }
}


void ReceivedDataProcessor::updateObstacles() {
    if (mission_state) {
        ObjectController::Objects objects = object_controller->getObjects();
        std::vector<ObjectController::Pole> poles = objects.poles;
        droneMsgsROS::obstaclesTwoDim::_poles_type poles_vector = obstacles_receiver->getPolesVector();
        for (int i = 0; i < poles_vector.size(); i++) {
            bool isPole = false;
            bool isLandmark = false;
            for (int j = 0; j < objects.poles.size(); j++) {
                if (poles_vector[i].id == objects.poles[j].id) {
                    isPole = true;
                }
            }
            if (!isPole) {
                for (int k = 0; k < objects.landmarks.size(); k++) {
                    if (poles_vector[i].id == objects.landmarks[k].id) {
                        isLandmark = true;
                    }
                }
            }
            if (!isPole && !isLandmark) {
                ObjectController::Pole pole;
                pole.id = poles_vector[i].id;
                pole.x_radius = poles_vector[i].radiusX;
                pole.y_radius = poles_vector[i].radiusY;
                pole.x_coor = poles_vector[i].centerX;
                pole.y_coor = poles_vector[i].centerY;
                pole.x_size = poles_vector[i].radiusX*2;
                pole.y_size = poles_vector[i].radiusY*2;
                object_controller->updateObstacles(pole);
            }
        }
    }
}    

void ReceivedDataProcessor::updateTrajectory() {
    if (mission_state) {
        droneMsgsROS::dronePositionTrajectoryRefCommand::_droneTrajectory_type trajectory_vector = command_receiver->getTrajectoryVector();
        ObjectController::Objects objects = object_controller->getObjects();
        std::vector<std::vector<ObjectController::Point>> trajectories = objects.trajectories;
        std::vector<ObjectController::Point> points; 
        for (int i = 0; i < trajectory_vector.size(); i++) {
            ObjectController::Point p;
            p.x = trajectory_vector[i].x;
            p.y = trajectory_vector[i].y;
            points.push_back(p);
        }
        if (points.size() > 0) {
            object_controller->updateTrajectory(points);
        }
    }
}

void ReceivedDataProcessor::updateDronesTrajectories() {
    if (mission_state) {
        droneMsgsROS::societyPose::_societyDrone_type drone_info_vector = society_receiver->getDronesPosition();
        ObjectController::Objects objects = object_controller->getObjects();
        for (int i = 0; i < drone_info_vector.size(); i++) {
            bool found = false;
            ObjectController::Point point;
            point.x = drone_info_vector[i].pose.x;
            point.y = drone_info_vector[i].pose.y;
            for (int j = 0; j < objects.drones.size(); j++) {
                if (drone_info_vector[i].id == objects.drones[j].id) {
                    objects.drones[j].x_coor = drone_info_vector[i].pose.x;
                    objects.drones[j].y_coor = drone_info_vector[i].pose.y;
                    objects.drones[j].degrees = angles::to_degrees(drone_info_vector[i].pose.yaw);
                    if (objects.drones[j].points.size() > 0) {
                        ObjectController::Point last_point = objects.drones[j].points.at(objects.drones[j].points.size()-1);
                        if (sqrt(pow((last_point.x - point.x),2)+pow((last_point.y - point.y),2)) > precision) {
                            objects.drones[j].points.push_back(point);
                        }
                    } 
                    object_controller->updateDrone(objects.drones[j],j); 
                    found = true;
                }
            }
            if (!found) {
                if (drone_info_vector[i].pose.x != 0 && drone_info_vector[i].pose.y != -10) {
                    ObjectController::Drone drone;
                    drone.id = drone_info_vector[i].id;
                    drone.x_coor = drone_info_vector[i].pose.x;
                    drone.y_coor = drone_info_vector[i].pose.y;
                    drone.degrees = angles::to_degrees(drone_info_vector[i].pose.yaw);
                    drone.x_size = objects.drones[0].x_size;
                    drone.y_size = objects.drones[0].y_size;
                    drone.points.push_back(point);
                    object_controller->updateDrone(drone,-1);
                } 
            }
        }
    }
}

void ReceivedDataProcessor::setMissionMode() {
    mission_state = true;
}

void ReceivedDataProcessor::setShowMode() {
    mission_state = false;
}

