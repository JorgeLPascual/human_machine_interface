/*
  BehaviorTree
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/

#include "../include/behavior_tree.h"
#include "../include/tree_item.h" //TreeItem class is included here to prevent recursive includes with tree_item.

BehaviorTree::BehaviorTree(QWidget *parent) : QTreeWidget(parent)
{
	QWidget::setLocale(QLocale());

	this->headerItem()->setHidden(true);
	this->setContextMenuPolicy(Qt::CustomContextMenu);
	this->resize(600, 300);

	qRegisterMetaType<QVector<int>>("QVector<int>");

	visualizer_parent = (BehaviorTreeVisualizer*) parent;


	editing = false;
	has_root = false;
	running = false;
	isTextExpanded = true;

	connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(onCustomContextMenu(const QPoint &)));
	connect(visualizer_parent, SIGNAL(cancelTreeSignal()), this, SLOT(cancelTree()));


	/*
	Style sheet customization
	*/
	setIconSize(QSize(25, 25));
	this->setStyleSheet(" \
		QTreeView::branch { \
        	background: white; \
		} \
		QTreeView::branch:has-siblings:!adjoins-item { \
			border-image: url(:/images/images/vline.png) 0; \
		} \
		QTreeView::branch:has-siblings:adjoins-item { \
			border-image: url(:/images/images/branch-more.png) 0; \
		} \
		QTreeView::branch:!has-children:!has-siblings:adjoins-item { \
			border-image: url(:/images/images/branch-end.png) 0; \
		} \
		QTreeView::branch:has-children:!has-siblings:closed, \
		QTreeView::branch:closed:has-children:has-siblings { \
        	border-image: url(:/images/images/branch-more_proto_mas.png) 0; \
		} \
		QTreeView::branch:has-children:!has-siblings:closed, \
		QTreeView::branch:closed:has-children:!has-siblings { \
        	border-image: url(:/images/images/branch-end_proto_mas.png) 0; \
		} \
		QTreeView::branch:open:has-children:has-siblings  { \
        	border-image: url(:/images/images/branch-more_proto_menos.png) 0; \
		} \
		QTreeView::branch:open:has-children:!has-siblings  { \
        	border-image: url(:/images/images/branch-end_proto_menos.png) 0; \
		} \
		QTreeView::item:selected { \
      background-color:transparent; \
      color:black; \
    } \
		");
}

BehaviorTree::~BehaviorTree()
{

}

/*
This function always executes when the tree is first created.
*/
void BehaviorTree::createMission(const QString &texto)
{
	root_item = new TreeItem((TreeItem*) 0, NodeType::SEQUENCE);
	this->addTopLevelItem(root_item);
	std::string partial_mission_name = " [Execute all actions in sequence until one fails]";
	root_item->setPartialNodeName(partial_mission_name);
	std::string mission_name = texto.toUtf8().constData() + partial_mission_name;
	root_item->setNodeName(texto.toUtf8().constData());
	root_item->setText(0, QString::fromStdString(mission_name));
	QPixmap icono_pixmap = QPixmap(":/images/images/sequence.png");
	QIcon icono_sequence = QIcon(icono_pixmap);
	root_item->setIcon(0,icono_sequence);
	this->show();
}


void BehaviorTree::createMissionByTreeItem(TreeItem * root)
{
		root_item = root;
		this->addTopLevelItem(root_item);
		if (root != 0) {
			if (isTextExpanded) {
				expandTreeText(0);
			}
			else {
				expandTreeText(2);
			}
			this->expandAll();
		}
}

TreeItem * BehaviorTree::copyBehaviorTree(TreeItem * item, bool root, TreeItem * inherited_parent) {
	TreeItem * copy = 0;
	if (item != 0) {
		if (root) {
			copy = new TreeItem(0);
			copy->modifyNode(item->getNodeName(), item->getNodeType(), item->getBehaviorType(), item->isRecurrent(), item->isActivated(),item->getNodeAttributes());
		}
		else {
			copy = new TreeItem(inherited_parent);
			copy->modifyNode(item->getNodeName(), item->getNodeType(), item->getBehaviorType(), item->isRecurrent(), item->isActivated(),item->getNodeAttributes());
		}
		int children = item->childCount();
		for (int i = 0; i < children; i++) {
			copy->addChild(copyBehaviorTree(item->child(i),false,copy));
		}
	}
	return copy;
}

/*
This function is called whenever we do a right click
It creates the context menu and disables any functionality that cannot be executed. This depends on where we right clicked.
*/
void BehaviorTree::onCustomContextMenu(const QPoint &p)
{
	if(itemAt(p)!=0)
	{
		TreeItem *item_clicked;
		if(is_menu_created)
		{
			contextMenu->close();
			delete contextMenu;
			is_menu_created = false;
		}
		contextMenu = new QMenu("Menu", this);
		is_menu_created = true;
		item_clicked = (TreeItem*)itemAt(p);
		TreeItem *item_clicked_parent;

		bool isSucceeder=false;
		bool isInverter=false;
		bool isSelector=false;
		bool isBehavior = false;
		bool isQuery = false;
		bool isAddBelief = false;
		bool canHaveSiblings = false;
		int children;
		//Check if the clicked node can have children
		if(item_clicked->getNodeType()==NodeType::SUCCEEDER)
		{
			isSucceeder = true;
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType()==NodeType::INVERTER)
		{
			isInverter = true;
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType()==NodeType::SELECTOR)
		{
			isSelector = true;
			children = item_clicked->childCount();
		}
		if(item_clicked->getNodeType() == NodeType::BEHAVIOR)
		{
			isBehavior = true;
		}
		if(item_clicked->getNodeType() == NodeType::QUERY)
		{
			isQuery = true;
		}
		if(item_clicked->getNodeType() == NodeType::ADD_BELIEF)
		{
			isAddBelief = true;
		}

		//Check if the clicked node can have siblings
		item_clicked_parent = item_clicked->getParent();
		if(item_clicked_parent != 0)
		{
			if(item_clicked_parent->getNodeType() != NodeType::SUCCEEDER 
					&& item_clicked_parent->getNodeType() != NodeType::INVERTER 
					&& item_clicked_parent->getNodeType() != NodeType::SELECTOR)
			{
				canHaveSiblings=true;
			}
			else if((item_clicked_parent->getNodeType() == NodeType::SUCCEEDER || item_clicked_parent->getNodeType() == NodeType::INVERTER || item_clicked_parent->getNodeType() == NodeType::SELECTOR) 
				&& item_clicked_parent->childCount() < 1)
			{
				canHaveSiblings=true;
			}
		}
		point = p;
		QAction action1("Execute tree", this);
		QAction action2("Execute tree from this node", this);
		QAction action3("Add child", this);
		QAction action4("Add sibling", this);
		QAction action5("Delete this node", this);
		QAction action6("Modify this node", this);
		QAction action7("Cancel execution", this);

		if (editing) {
			if((isSucceeder || isInverter) && (children < 1))
			{
				contextMenu->addAction(&action3);
			}
			else if (!isSucceeder && !isInverter && !isBehavior && !isQuery && !isAddBelief)
			{
				contextMenu->addAction(&action3);
			}
			if(canHaveSiblings)
			{
				contextMenu->addAction(&action4);
			}
			if(!running)
			{
				contextMenu->addAction(&action5);
			}
			if(item_clicked != root_item)
			{
				contextMenu->addAction(&action6);
			}
			connect(&action3, SIGNAL(triggered()), this, SLOT(addChildAux()));
			connect(&action4, SIGNAL(triggered()), this, SLOT(addSiblingAux()));
			connect(&action5, SIGNAL(triggered()), this, SLOT(removeItemWidgetAtAux()));
			connect(&action6, SIGNAL(triggered()), this, SLOT(modifyItemWidgetAux()));

		}
		if(!running && !editing)
		{
			contextMenu->addAction(&action1);
			contextMenu->addAction(&action2);
			connect(&action1, SIGNAL(triggered()), this, SLOT(executeTree()));
			connect(&action2, SIGNAL(triggered()), this, SLOT(executeTreeFromItem()));
		}
		else if (!editing)
		{
			contextMenu->addAction(&action7);
			connect(&action7, SIGNAL(triggered()), this, SLOT(cancelTree()));
		}
		contextMenu->exec(mapToGlobal(p));
	}
	else if (itemAt(p)==0)
	{
		point = p;
		if(is_menu_created)
		{
			contextMenu->close();
			delete contextMenu;
			is_menu_created = false;
		}
		is_menu_created = true;
		contextMenu = new QMenu("Menu", this);
		QAction action_2("Cancel execution", this);
		QAction action_1("Execute tree", this);
		if(!running && has_root && !editing)
		{
			contextMenu->addAction(&action_1);
			connect(&action_1, SIGNAL(triggered()), this, SLOT(executeTree()));
			contextMenu->exec(mapToGlobal(p));
		}
		else if(running)
		{
			contextMenu->addAction(&action_2);
			connect(&action_2, SIGNAL(triggered()), et, SLOT(cancelExecution()));
			contextMenu->exec(mapToGlobal(p));
		}
	}
}

/*
This function adds a root to the tree if it doesn't have one already
*/
void BehaviorTree::addTopLevelItem(TreeItem *top_level_item)
{
	if (has_root) {
		if (root_before != 0) {
			delete root_before;
		}
	}
	if (top_level_item != 0) {
		root_before = top_level_item;
		has_root = true;
		top_level_item->setRoot(true);
	}
	else {
		has_root = false;
	}
	QTreeWidget::addTopLevelItem(top_level_item);
}

bool BehaviorTree::isRunning()
{
	return running;
}

BehaviorTreeVisualizer* BehaviorTree::getVisualizer()
{
	return visualizer_parent;
}

/*
This function creates an ExecutionTree object to execute the tree's nodes, starting at the root
*/
void BehaviorTree::executeTree()
{
	if(!has_root){
		QMessageBox::information(this,tr("Cannot execute tree"),"The behavior tree is not defined or defined incorrectly");
		this->getVisualizer()->stopBlockingTextInput();
	}
	else {

		if(checkTree(root_item))
		{
			if (running) {
				et->cancelExecution();
				joinExecutionTree();
			}
			else {
				et = new ExecutionTree(this);
				connect(this, SIGNAL(cancelExecutionSignal()), et, SLOT(cancelExecution()));
				running = true;
				connect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution()));
				connect(et,SIGNAL(finished()), this, SLOT(joinExecutionTree()));
				connect(et,SIGNAL(finished()), this->getVisualizer(), SLOT(stopBlockingTextInput()));
				connect(et, SIGNAL(updateText()), visualizer_parent, SLOT(update()));
				connect(this,SIGNAL(executionStarted()), this->getVisualizer(), SLOT(startBlockingTextInput()));
				executing_tree = new std::thread(std::ref(ExecutionTree::executeParallelTree),et,root_item);
				Q_EMIT(executionStarted());
			}
		} else {
			this->getVisualizer()->stopBlockingTextInput();
		}
	}
}

/*
This function creates an ExecutionTree object to execute the tree's nodes, starting at the clicked item
*/
void BehaviorTree::executeTreeFromItem()
{
	TreeItem * item_to_execute = (TreeItem*)itemAt(point);
	if(checkTree(item_to_execute))
	{
		if (running) {
			et->cancelExecution();
			joinExecutionTree();
		}
		et = new ExecutionTree(this);
		running = true;
		connect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution()));
		connect(et,SIGNAL(finished()), this, SLOT(joinExecutionTree()));
		connect(et,SIGNAL(finished()), this->getVisualizer(), SLOT(stopBlockingTextInput()));
		connect(et, SIGNAL(updateText()), visualizer_parent, SLOT(update()));
		connect(this,SIGNAL(executionStarted()), this->getVisualizer(), SLOT(startBlockingTextInput()));
		executing_tree = new std::thread(std::ref(ExecutionTree::executeParallelTree),et,item_to_execute);
		Q_EMIT(executionStarted());
	}
}

void BehaviorTree::joinExecutionTree() {
	executing_tree->join();
	ExecutionTree::mutex.lock();
	disconnect(this, SIGNAL(cancelExecutionSignal()), et, SLOT(cancelExecution()));
	disconnect(qApp,SIGNAL(lastWindowClosed()), et, SLOT(cancelExecution()));
	disconnect(et, SIGNAL(updateText()), visualizer_parent, SLOT(update()));
	disconnect(et,SIGNAL(finished()), this->getVisualizer(), SLOT(stopBlockingTextInput()));
	disconnect(et, SIGNAL(updateText()), visualizer_parent, SLOT(update()));
	disconnect(this,SIGNAL(executionStarted()), this->getVisualizer(), SLOT(startBlockingTextInput()));
	delete executing_tree;
	running = false;
	delete et;
	if (is_menu_created) {
		contextMenu->close();
		delete contextMenu;
		is_menu_created = false;
	}
	ExecutionTree::mutex.unlock();
}

void BehaviorTree::updateBackground()
{
	ExecutionTree::mutex.lock();
	repaint();
	ExecutionTree::mutex.unlock();
}

void BehaviorTree::cancelTree()
{
	if (running) {
		Q_EMIT(cancelExecutionSignal());
	}
}

void BehaviorTree::windowFinished(TreeItem *the_item)
{
	this->expandAll();
	if (isTextExpanded) {
		expandTreeText(0);
	}
	else {
		expandTreeText(2);
	}
	disconnect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

/*
Next two functions are called whenever we want to add a child
*/
void BehaviorTree::addChildAux()
{
	connect(this, SIGNAL(executeAddChildAction(const QPoint &)), this, SLOT(addChild(const QPoint &)));
	Q_EMIT(executeAddChildAction(point));
	disconnect(this, SIGNAL(executeAddChildAction(const QPoint &)), this, SLOT(addChild(const QPoint &)));
}

void BehaviorTree::addChild(const QPoint &p)
{
	parent_item_for_child = (TreeItem*)itemAt(p);
	window = new BehaviorDialog(this, parent_item_for_child);
	window->show();
	connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

/*
Next two functions are called whenever we want to add a sibling
*/
void BehaviorTree::addSiblingAux()
{
	connect(this, SIGNAL(executeAddSiblingAction(const QPoint &)), this, SLOT(addSibling(const QPoint &)));
	Q_EMIT(executeAddSiblingAction(point));
	disconnect(this, SIGNAL(executeAddSiblingAction(const QPoint &)), this, SLOT(addSibling(const QPoint &)));
}

void BehaviorTree::addSibling(const QPoint &p)
{
	parent_item_for_sibling = (TreeItem*)itemAt(p);
	parent_item_for_sibling = parent_item_for_sibling->TreeItem::getParent();
	window = new BehaviorDialog(this, parent_item_for_sibling);
	window->show();
	connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

/*
Next two functions are called whenever we want to modify any node
*/
void BehaviorTree::modifyItemWidgetAux()
{
	connect(this, SIGNAL(executeModifyItemWidgetAction(const QPoint &)), this, SLOT(modifyItemWidget(const QPoint &)));
	Q_EMIT(executeModifyItemWidgetAction(point));
	disconnect(this, SIGNAL(executeModifyItemWidgetAction(const QPoint &)), this, SLOT(modifyItemWidget(const QPoint &)));
}

void BehaviorTree::modifyItemWidget(const QPoint &p)
{
	parent_item_for_modify = (TreeItem*)itemAt(p);
	parent_item_for_modify = parent_item_for_modify->TreeItem::getParent();
	window = new BehaviorDialog(this, parent_item_for_modify, (TreeItem*)itemAt(p));
	window->show();
	connect(window, SIGNAL(windowAccepted(TreeItem *)), this, SLOT(windowFinished(TreeItem *)));
}

/*
Next two functions are called whenever we want to remove any node
*/
void BehaviorTree::removeItemWidgetAtAux()
{
	connect(this, SIGNAL(executeRemoveItemAction(const QPoint &)), this, SLOT(removeItemWidgetAt(const QPoint &)));
	Q_EMIT(executeRemoveItemAction(point));
}

void BehaviorTree::removeItemWidgetAt(const QPoint &p)
{
	disconnect(this, SIGNAL(executeRemoveItemAction(const QPoint &)), this, SLOT(removeItemWidgetAt(const QPoint &)));
	if ((BehaviorTree*)itemAt(p) != this){
		TreeItem *item_to_remove = (TreeItem*)itemAt(p);
		TreeItem *item_to_remove_back = item_to_remove;
		item_to_remove->removeItemWidget();
		if (item_to_remove_back->isRoot())
		{
			has_root = false;
			this->clear();
		}
	}
}

/*
The following two functions are called before executing the tree. They check if the tree is correctly structured.
If it is not correctly structured, it will not let the user execute the tree until he/she fixes the errors
*/
bool BehaviorTree::checkTree(TreeItem *item)
{
	if(has_root)
	{
		bool result = false;
		result = checkItem(item);
		return result;
	} else
	{
		return false;
	}
}

bool BehaviorTree::checkItem(TreeItem *item)
{
	connect(this,SIGNAL(update()), this, SLOT(updateBackground()));
	QMessageBox error_message;
	bool isCorrect = true;
	bool correctChildren = true;
	NodeType item_nodetype = item->getNodeType();
	switch(item_nodetype)
	{
		case NodeType::BEHAVIOR:
		case NodeType::QUERY:
		case NodeType::ADD_BELIEF:
		//When the node can't have children
		{
			if(item->childCount() != 0)
			{
				this->getVisualizer()->stopBlockingTextInput();
				isCorrect = false;
				error_message.setWindowTitle(QString::fromStdString("Tree structure error"));
				error_message.setText(QString::fromStdString("The item '" + item->getNodeName() + "' has an incorrect number of children."));
				item->setColor("#ff5757");
				Q_EMIT(update());
				error_message.exec();
				correctChildren = false;
			}
			break;
		}
		//When the node can have just one child
		case NodeType::SUCCEEDER:
		case NodeType::INVERTER:
		case NodeType::REPEATER:
		{
			if(item->childCount() != 1)
			{
				this->getVisualizer()->stopBlockingTextInput();
				isCorrect = false;
				error_message.setWindowTitle(QString::fromStdString("Tree structure error"));
				error_message.setText(QString::fromStdString("The item '" + item->getNodeName() + "' has an incorrect number of children."));
				item->setColor("#ff5757");
				Q_EMIT(update());
				error_message.exec();
				correctChildren = false;
			}
			break;
		}
		//When the item must have at least one child
		case NodeType::SEQUENCE:
		case NodeType::PARALLEL:
		case NodeType::REPEAT_UNTIL_FAIL:
		case NodeType::SELECTOR:
		{
			if(item->childCount() < 1)
			{
				this->getVisualizer()->stopBlockingTextInput();
				isCorrect = false;
				error_message.setWindowTitle(QString::fromStdString("Tree structure error"));
				error_message.setText(QString::fromStdString("The item '" + item->getNodeName() + "' has an incorrect number of children."));
				item->setColor("#ff5757");
				Q_EMIT(update());
				error_message.exec();
				correctChildren = false;
			}
			break;
		}
	}

	if(!correctChildren)
	{
		isCorrect = false;
		disconnect(this,SIGNAL(update()), this, SLOT(updateBackground()));
		return isCorrect;
	}

	//Checking each of it's children recursively
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			isCorrect = isCorrect && checkItem(item->child(i));
		}
	}
	item->setColor("#ffffff");
	Q_EMIT(update());
	disconnect(this,SIGNAL(update()), this, SLOT(updateBackground()));
	return isCorrect;
}

bool BehaviorTree::checkNodeArguments(TreeItem *item)
{
	if (item->getNodeType() == NodeType::ADD_BELIEF)
	{
		//
	} else if (item->getNodeType() == NodeType::BEHAVIOR)	{
		//
	}
}

void BehaviorTree::expandTreeText(int checkState)
{
	if (has_root) {
		if(checkState == 2)
		{
			minimizeText(root_item);
			isTextExpanded=false;
		}
		else if(checkState == 0)
		{
			expandText(root_item);
			isTextExpanded=true;
		}
	}
}

void BehaviorTree::expandText(TreeItem *item)
{
	std::string text = item->getNodeName();
	text = text +  item->getPartialNodeName();
	item->setText(0, QString::fromStdString(text));
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			expandText(item->child(i));
		}
	}
}

void BehaviorTree::minimizeText(TreeItem *item)
{
	item->setText(0, QString::fromStdString(item->getNodeName()));
	if(item->childCount()>0)
	{
		for(int i = 0; i < item->childCount(); i++)
		{
			minimizeText(item->child(i));
		}
	}
}

void BehaviorTree::setEditMode(bool b) {
	editing = b;
}

void BehaviorTree::createMissionDialog() {
	mission_dialog = new MissionSpecificationDialog();
	connect(mission_dialog, SIGNAL(missionNameSelected(const QString &)), this, SLOT(createMission(const QString &)));
	mission_dialog->show();
}

TreeItem * BehaviorTree::getRoot() {
	if(has_root)
		return root_item;
	else
		return nullptr;
}
