/*
  ToolWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/tool_widget.h"

ToolWidget::ToolWidget(QWidget * parent) : QWidget(parent),	ui(new Ui::ToolWidget) {
	ui->setupUi(this);
	button1 = ui->b1;
	button2 = ui->b2;
	button3 = ui->b3;
	button4 = ui->b4;
	button5 = ui->b5;
	button6 = ui->b6;
	button7 = ui->b7;
	mapper = new QSignalMapper(this);
	QObject::connect(button1, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping(button1,button1);
	QObject::connect(button2, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping(button2,button2);
	QObject::connect(button3, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping(button3,button3);
	QObject::connect(button4, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping(button4,button4);
	QObject::connect(button5, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping(button5,button5);
	QObject::connect(button6, SIGNAL(clicked()),mapper,SLOT(map()));
	mapper->setMapping(button6,button6);
	QObject::connect(button7,SIGNAL(clicked()),this,SLOT(onClickedStikyButton()));
	QObject::connect(mapper,SIGNAL(mapped(QWidget *)),this,SLOT(onClickedButton(QWidget *)));
	setFocusPolicy(Qt::NoFocus);
}

ToolWidget::~ToolWidget() {
	delete ui;
	delete mapper;
}


void ToolWidget::onClickedButton(QWidget * b) {
	if (last_pressed != NULL) {
		last_pressed->setChecked(false);
	} 
	QToolButton * button = (QToolButton *) b;
	button->setChecked(true);
	switch(atoi(b->objectName().toStdString().substr(1,1).c_str())) {
		case 1:{
			QPixmap p(":/images/images/cursor-drone.png");
			QCursor q(p.scaled(40,40),0,0);
			QApplication::setOverrideCursor(q);
			break;
		} 
		case 2: {
			QPixmap p(":/images/images/cursor-pole.png");
			QCursor q(p.scaled(40,40),0,0);
			QApplication::setOverrideCursor(q);
			break;
		}
		case 3:	{
			QPixmap p(":/images/images/cursor-landmark.png");
			QCursor q(p.scaled(40,40),0,0);
			QApplication::setOverrideCursor(q);
			break;
		}
		case 4:	{
			QPixmap p(":/images/images/cursor-cursor.png");
			QCursor q(p.scaled(20,20),0,0);
			QApplication::setOverrideCursor(q);
			break;
		}
		case 5:	{
			QPixmap p(":/images/images/cursor-cursor.png");
			QCursor q(p.scaled(20,20),0,0);
			QApplication::setOverrideCursor(q);
			break;
		}
		case 6:	{
			QApplication::setOverrideCursor(Qt::OpenHandCursor);
			break;
		}
		default:	{
			QPixmap p(":/images/images/cursor-cursor.png");
			QCursor q(p.scaled(20,20),0,0);
			QApplication::setOverrideCursor(q);
			break;
		}
	}
	last_pressed = button;
}

QToolButton * ToolWidget::getTool() {
	if (last_pressed != NULL) 
		return last_pressed;
	return NULL;
}

void ToolWidget::onClickedStikyButton() {
	sticky = !sticky;
}

bool ToolWidget::isSticky() {
	return sticky;
}
void ToolWidget::unsetTool() {
	last_pressed = NULL;
	sticky = false;
	button1->setChecked(false);
	button2->setChecked(false);
	button3->setChecked(false);
	button4->setChecked(false);
	button5->setChecked(false);
	button6->setChecked(false);
	button7->setChecked(false);
	QApplication::setOverrideCursor(Qt::ArrowCursor);
}