/*
  NewMapMenu
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/new_map_menu.h"

NewMapMenu::NewMapMenu(QWidget *parent, ObjectController * o) : QDialog(parent), ui(new Ui::NewMapMenu) {
	ui->setupUi(this);
	object_controller = o;
	QObject::connect(ui->acceptButton, SIGNAL(clicked()), this, SLOT(drawMap()));
	ui->spin1->setValue(5);
	ui->spin2->setValue(5);
    setModal(true);
}


NewMapMenu::~NewMapMenu() {
	delete ui;
}

void NewMapMenu::drawMap() {
	object_controller->setMap(ui->spin1->value(), ui->spin2->value(),ui->spin3->value(),ui->spin4->value());
	close();
}