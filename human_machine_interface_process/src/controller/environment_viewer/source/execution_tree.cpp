/*
  ExecutionTree
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/execution_tree.h"

std::mutex ExecutionTree::mutex;
std::condition_variable ExecutionTree::condition;

ExecutionTree::ExecutionTree(BehaviorTree * parent) : QObject() {
	n.param<std::string>("topic_behavior_completed", topic_behavior_completed,
     "behavior_event");
  n.param<std::string>("activate_behavior", activate_behavior,
     "activate_behavior");
  n.param<std::string>("initiate_behaviors", initiate_behaviors,
     "initiate_behaviors");
  n.param<std::string>("cancel_behavior", cancel_behavior,
     "inhibit_behavior");
  n.param<std::string>("execute_query", execute_query,
     "execute_query");
  n.param<std::string>("add_belief", add_belief,
     "add_belief");
  
  running = true;
  waiting = false;
  tree_returned_value = false;
  cancelled = false;
  this->parent = parent;

  behavior_sub=n.subscribe(topic_behavior_completed, 1000, &ExecutionTree::behaviorCompletedCallback,this);
  activate_behavior_srv=n.serviceClient<droneMsgsROS::BehaviorSrv>(activate_behavior);
  initiate_behavior_srv=n.serviceClient<droneMsgsROS::InitiateBehaviors>(initiate_behaviors);
  cancel_behavior_srv=n.serviceClient<droneMsgsROS::BehaviorSrv>(cancel_behavior);
  execute_query_srv=n.serviceClient<droneMsgsROS::ConsultBelief>(execute_query);
  add_belief_srv=n.serviceClient<droneMsgsROS::AddBelief>(add_belief);

  //Hasta que se cree la API
  droneMsgsROS::InitiateBehaviors msg;
  initiate_behavior_srv.call(msg);
  connect(this,SIGNAL(update()), parent, SLOT(updateBackground()));
}

ExecutionTree::~ExecutionTree() {
  disconnect(this,SIGNAL(update()), parent, SLOT(updateBackground()));
}


void ExecutionTree::executeTree(TreeItem * item) {
  bool ret_val = executeItem(item);
  mutex.lock();
  tree_returned_value = ret_val;
  running = false;
  condition.notify_all();
  Q_EMIT(finished());
  mutex.unlock();
}

bool ExecutionTree::executeItem(TreeItem * item) {
  YAML::Node node = YAML::Load(item->getNodeAttributes());
  switch(item->getNodeType()) {
    case NodeType::SEQUENCE: {
      item->childCount();
      for (int i = 0; i < item->childCount(); i++) {
        if (!executeItem(item->child(i))) {
          if (cancelled) {
            setColor(item,COLOR_WHITE);
            return false;
          }
          setColor(item,COLOR_RED);
          sleep(1);
          setColor(item,COLOR_WHITE);
          return false;
        }
      }
      setColor(item,COLOR_GREEN);
      sleep(1);
      setColor(item,COLOR_WHITE);
      return true;
    }
    case NodeType::INVERTER: {
      bool value = !executeItem(item->child(0));
      if (cancelled) {
        setColor(item,COLOR_WHITE);
        return false;
      }
      if (value) {
        setColor(item,COLOR_GREEN);
      }
      else {
        setColor(item,COLOR_RED);
      }
      sleep(1);
      setColor(item,COLOR_WHITE);
      return value;
    }
    case NodeType::REPEAT_UNTIL_FAIL: {
      int number = item->childCount();
      int i = 0;
      bool failure = false;
      while (true) {
        ros::Duration t(1/node["t"].as<double>());
        ros::Time startTime = ros::Time::now();
        for (int j = 0; j < item->childCount(); j++) {
          if (!executeItem(item->child(j)) || cancelled) {
            failure = true;
            break;
          }
        }
        i++;
        i %= number;
        ros::Duration secondsPassed = ros::Time::now() - startTime;
        ros::Duration timeLeft = t - secondsPassed;
        while (timeLeft > ros::Duration(0) && !failure && !cancelled) {
          //std::this_thread::sleep_for(std::chrono::milliseconds((int) (1000/t-secondsPassed)));
          timeLeft.sleep();
          secondsPassed = ros::Time::now() - startTime;
          timeLeft = t - secondsPassed;
        }
        if (failure || cancelled) {
          break;
        }
      }
      if (cancelled) {
        setColor(item,COLOR_WHITE);
        return false;
      }
      if (failure) {
        setColor(item,COLOR_RED);
      }
      else {
        setColor(item,COLOR_GREEN);
      }
      sleep(1);
      setColor(item,COLOR_WHITE);
      return true;
    }
    case NodeType::REPEATER: {
      int n = node["n"].as<int>();
      bool res;
      for (int i = 0; i < n; i++) {
        res = executeItem(item->child(0));
        if(!res){
          break;
        }
        if (cancelled) {
          setColor(item,COLOR_WHITE);
          return false;
        }
      }
      setColor(item,COLOR_GREEN);
      sleep(1);
      setColor(item,COLOR_WHITE);
      return true;
    }
    case NodeType::SELECTOR: {
      for (int i = 0; i < item->childCount(); i++) {
        bool value = executeItem(item->child(i));
        if (cancelled) {
          setColor(item,COLOR_WHITE);
          return false;
        }
        if (value) {
          setColor(item,COLOR_GREEN);
          sleep(1);
          setColor(item,COLOR_WHITE);
          return true;
        }
      }
      setColor(item,COLOR_RED);
      sleep(1);
      setColor(item,COLOR_WHITE);
      return false;
    }
    case NodeType::QUERY: {
      setColor(item,COLOR_BLUE);
      droneMsgsROS::ConsultBelief query;
      query.request.query = processData(node["query"].as<std::string>());
      execute_query_srv.call(query);
      if (cancelled) {
        setColor(item,COLOR_WHITE);
        return false;
      }
      if (!query.response.success) {
        setColor(item,COLOR_RED);
        sleep(1);
        setColor(item,COLOR_WHITE);
        return false;
      }

      std::string res = "";
      YAML::Node node = YAML::Load(parent->getVisualizer()->getText());
      YAML::Node node2 = YAML::Load(query.response.substitutions);
      for(YAML::const_iterator it=node2.begin();it!=node2.end();++it) {
        node[it->first.as<std::string>()] = processType(it);
      }
      res = processQueryData(node);


      setText(res);
      setColor(item,COLOR_GREEN);
      sleep(1);
      setColor(item,COLOR_WHITE);
      return true;
    }
    case NodeType::SUCCEEDER: {
      executeItem(item->child(0));
      setColor(item,COLOR_GREEN);
      sleep(1);
      setColor(item,COLOR_WHITE);
      return true;
    }
    case NodeType::BEHAVIOR: {
      setColor(item,COLOR_BLUE);
      std::unique_lock<std::mutex> lock(behavior_mutex);
      mutex.lock();
      actual_item = item;
      droneMsgsROS::BehaviorSrv::Request msg;
      droneMsgsROS::BehaviorSrv::Response res;
      droneMsgsROS::BehaviorCommand behavior;
      behavior.name = item->behaviorTypeToString(item->getBehaviorType());
      std::string args = processData(item->getNodeAttributes());
      behavior.arguments = args;
      msg.behavior = behavior;
      if (!item->isRecurrent()) {
        bool result = activate_behavior_srv.call(msg,res);
        behavior_value = result && res.ack;
      }
      else {
        if (item->isActivated()) {
          bool result = activate_behavior_srv.call(msg,res);
          behavior_value = result && res.ack;
        }
        else {
          bool result = cancel_behavior_srv.call(msg,res);
          behavior_value = result && res.ack;
        }
      }
      if (!res.ack) {
        mutex.unlock();
        setColor(item,COLOR_RED);
        sleep(1);
        setColor(item,COLOR_WHITE);
        return false;
      }
      if (!item->isRecurrent()) {
        waiting = true;
        mutex.unlock();
        behavior_condition_variable.wait(lock);
      }
      else {
        mutex.unlock();
      }
      if (cancelled) {
        setColor(item,COLOR_WHITE);
        return false;
      }
      else {
        if (behavior_value) {
          setColor(item,COLOR_GREEN);
        }
        else {
          setColor(item,COLOR_RED);
        }
        sleep(1);
        setColor(item,COLOR_WHITE);
        return behavior_value;
      }
    }
    case NodeType::PARALLEL: {
      std::unique_lock<std::mutex> lock(mutex);
      int number_of_successes = node["n"].as<int>();
      for (int i = 0; i < item->childCount(); i++) {
        ExecutionTree * et = new ExecutionTree(this->parent);
        trees_in_parallel.push_back(et);
        threads.push_back(new std::thread(std::ref(ExecutionTree::executeParallelTree),et,item->child(i)));
      }
      bool value = false;
      int failures = 0;
      int successes = 0;
      int number_of_failures = item->childCount()-number_of_successes + 1;
      while (successes < number_of_successes && failures < number_of_failures) {
        condition.wait(lock);
        successes = 0;
        failures = 0;
        for (int i = 0; i < trees_in_parallel.size(); i++) {
          if (!trees_in_parallel[i]->isRunning()) {
            if (trees_in_parallel[i]->getReturnedValue()) {
              successes +=1;
            }
            else {
              failures +=1;
            }
          }
        }
        if (successes >= number_of_successes || failures >= number_of_failures || cancelled) {
          for (int i = 0; i < trees_in_parallel.size(); i++) {
            if (trees_in_parallel[i]->isRunning()) {
              trees_in_parallel[i]->cancelExecution();
            }
          }
        }
      }
      lock.unlock();
      if (successes >= number_of_successes) {
        value = true;
      }
      for (int i = 0; i < trees_in_parallel.size(); i++) {
        threads[i]->join();
        delete threads[i];
        delete trees_in_parallel[i];
      }
      trees_in_parallel.clear();
      threads.clear();
      if (cancelled) {
        setColor(item,COLOR_WHITE);
        return false;
      }
      if (value) {
        setColor(item,COLOR_GREEN);
      }
      else {
        setColor(item,COLOR_RED);
      }
      sleep(1);
      setColor(item,COLOR_WHITE);
      return value;
    }
    case NodeType::ADD_BELIEF: {
      setColor(item,COLOR_BLUE);
      droneMsgsROS::AddBelief beliefs;
      beliefs.request.belief_expression = processData(node["belief_expression"].as<std::string>());
      bool aux = node["multivalued"].as<bool>();
      beliefs.request.multivalued = aux;
      add_belief_srv.call(beliefs);
      if (cancelled) {
        setColor(item,COLOR_WHITE);
        return false;
      }
      if (!beliefs.response.success) {
        setColor(item,COLOR_RED);
        sleep(1);
        setColor(item,COLOR_WHITE);
        return false;
      }
      setColor(item,COLOR_GREEN);
      sleep(1);
      setColor(item,COLOR_WHITE);
      return true;
    }
    default: {
    }
  }
}

void ExecutionTree::executeParallelTree(ExecutionTree * et, TreeItem * item)  {
  et->executeTree(item);
}

void ExecutionTree::cancelExecution() {
	mutex.try_lock();
  cancelled = true;
  if (waiting) {
    droneMsgsROS::BehaviorSrv msg;
    droneMsgsROS::BehaviorCommand behavior;
    behavior.name = actual_item->behaviorTypeToString(actual_item->getBehaviorType());
    behavior.arguments = actual_item->getNodeAttributes();
    msg.request.behavior = behavior;
    cancel_behavior_srv.call(msg);
    behavior_condition_variable.notify_all();
  }
  else {
    behavior_condition_variable.notify_all();
    condition.notify_all();
  }
  mutex.unlock();
}

bool ExecutionTree::getReturnedValue() {
  return tree_returned_value;
}

bool ExecutionTree::isRunning() {
  return running;
}

void ExecutionTree::behaviorCompletedCallback(const droneMsgsROS::BehaviorEvent &msg) {
  mutex.lock();
  if (waiting && msg.name == actual_item->behaviorTypeToString(actual_item->getBehaviorType())) {
    waiting = false;
    behavior_value = (msg.behavior_event_code == droneMsgsROS::BehaviorEvent::GOAL_ACHIEVED);
    behavior_condition_variable.notify_all();
  }
  mutex.unlock();
}

void ExecutionTree::setColor(TreeItem* item, std::string color) {
  item->setColor(color);
  Q_EMIT(update());
}

std::string ExecutionTree::processData(std::string raw_arguments) {
  std::string text = parent->getVisualizer()->getText();
  if (text != "") {
    YAML::Node node_beliefs = YAML::Load(text);
    for(YAML::const_iterator it=node_beliefs.begin();it!=node_beliefs.end();++it) {
      std::string name = SUBSTITUTION_S + it->first.as<std::string>();
      if (raw_arguments.find(name)) {
        std::string data = processType(it);
        boost::replace_all(raw_arguments, name, data);
      }
    }
  }
  return raw_arguments;
}

std::string ExecutionTree::processQueryData(YAML::Node query) {
  std::string res = "";
  for(YAML::const_iterator it=query.begin();it!=query.end();++it) {
    res = res + it->first.as<std::string>() + ": " + processType(it);
    res = res + "\n";
  }
  return res;
}

std::string ExecutionTree::processType(YAML::const_iterator it) {
  std::string res = "";
  switch (it->second.Type()) {
    case YAML::NodeType::Scalar: {
      res = it->second.as<std::string>();
      break;
    }
    case YAML::NodeType::Sequence: {
      std::vector<double> vec = it->second.as<std::vector<double>>();
      std::ostringstream oss;
      if (!vec.empty())
      {
        std::copy(vec.begin(), vec.end()-1,
        std::ostream_iterator<double>(oss, ","));
        oss << vec.back();
      }
      res = "[" + oss.str() + "]";
      break;
    }
  }
  return res;
}

void ExecutionTree::setText(std::string str) {
  parent->getVisualizer()->setText(str);
  Q_EMIT(updateText());
}