/*
  MissionVisualizer
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/mission_visualizer.h"

MissionVisualizer::MissionVisualizer(QWidget *parent, Connection * connection , OdometryStateReceiver * odometry_receiver ,
        ObstaclesReceiver * obstacles_receiver , MissionStateReceiver * mission_receiver ,
        TrajectoryAbsRefCommandReceiver * command_receiver , SocietyPoseReceiver * society_receiver ) 
{
    this->connection = connection;
    environment_widget = new EnvironmentWidget(0,connection,connection->odometry_receiver,connection->obstacle_receiver,
        connection->mission_planner_receiver,connection->trajectory_abs_ref_commander_receiver,connection->society_pose_receiver);
    using_bt = false;

    behavior_tree_visualizer = new BehaviorTreeVisualizer(this);
    behavior_tree_visualizer->hide();

    setOrientation(Qt::Horizontal);
    addWidget(environment_widget);


    connect(environment_widget,SIGNAL(editing(QWidget*)),this,SLOT(emitEdit()));
    connect(environment_widget,SIGNAL(showing(QWidget*)),this,SLOT(emitShow()));

    connect(connection,SIGNAL(visualizer(bool)),this,SLOT(useBTVisualizer(bool)));
    connect(behavior_tree_visualizer,SIGNAL(setEditMode(bool)),this,SLOT(setBTEditMode(bool)));

    connect(connection,SIGNAL(missionMode()),this,SLOT(setMissionMode()));
    connect(connection,SIGNAL(showMode()),this,SLOT(setShowMode()));
}

MissionVisualizer::~MissionVisualizer() 
{
  delete environment_widget;
  delete behavior_tree_visualizer;
}

EnvironmentWidget * MissionVisualizer::getEnvironmentWidget() {
  return environment_widget;
}

void MissionVisualizer::emitEdit() {
  if (using_bt) {
    behavior_tree_visualizer->hide();
  }
  refresh();
  Q_EMIT(editing(this));
}

void MissionVisualizer::emitShow() {
  if (using_bt) {
    behavior_tree_visualizer->show();
  }
  refresh();
  Q_EMIT(showing(this));
}

void MissionVisualizer::setMissionMode() {
  environment_widget->setMissionMode();
}

void MissionVisualizer::setShowMode() {
  environment_widget->setShowMode();
}

void MissionVisualizer::useBTVisualizer(bool b) {
  if (b && !using_bt) {
    behavior_tree_visualizer->show();
    using_bt = true;
    addWidget(behavior_tree_visualizer);
    connect(connection,SIGNAL(executeTreeSignal()),behavior_tree_visualizer,SLOT(executeTree()));
    connect(connection,SIGNAL(cancelTreeSignal()),behavior_tree_visualizer,SLOT(cancelTree()));
    //connect(behavior_tree_visualizer,SIGNAL(missionFinished()),connection,SLOT(finishedTree()));
    connect(behavior_tree_visualizer,SIGNAL(missionStarted()),environment_widget,SLOT(setMissionMode()));
    connect(behavior_tree_visualizer,SIGNAL(missionFinished()),environment_widget,SLOT(setShowMode()));
    connect(behavior_tree_visualizer,SIGNAL(missionFinished()),connection,SLOT(finishTree()));
  }
  else if (!b && using_bt) {
    behavior_tree_visualizer->hide();
    disconnect(connection,SIGNAL(executeTreeSignal()),behavior_tree_visualizer,SLOT(executeTree()));
    disconnect(connection,SIGNAL(cancelTreeSignal()),behavior_tree_visualizer,SLOT(cancelTree()));
    //disconnect(behavior_tree_visualizer,SIGNAL(missionFinished()),connection,SLOT(finishedTree()));
    disconnect(behavior_tree_visualizer,SIGNAL(missionStarted()),environment_widget,SLOT(setMissionMode()));
    disconnect(behavior_tree_visualizer,SIGNAL(missionFinished()),environment_widget,SLOT(setShowMode()));
    using_bt = false;
  }
}

void MissionVisualizer::setBTEditMode(bool b) {
  if (b && using_bt) {
    setTreeEditMode(true);
    Q_EMIT(editingTree(this));
  }
  else if (!b && using_bt) {
    setTreeEditMode(false);
    Q_EMIT(showingTree(this));
  }
}


void MissionVisualizer::setTreeEditMode(bool b) {
  if (b) {
    environment_widget->editButton(false);
  }
  else {
    environment_widget->editButton(true);
  }
  behavior_tree_visualizer->toggleEditMode();
}