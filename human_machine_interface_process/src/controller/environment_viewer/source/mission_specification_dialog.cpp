/*
  MissionSpecificationDialog
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/mission_specification_dialog.h"

MissionSpecificationDialog::MissionSpecificationDialog(QWidget *parent) : QDialog(parent), ui(new Ui::MissionSpecificationDialog)
{

  ui->setupUi(this);
  my_layout = ui->layout;
  padre = parent;
  this->setModal(true);

  connect(ui->accept_pushbutton, SIGNAL(clicked()), this, SLOT(actionAccept()));
}

MissionSpecificationDialog::~MissionSpecificationDialog()
{
  delete ui;
}

void MissionSpecificationDialog::actionAccept()
{
  if(!this->ui->line_edit_name->text().isEmpty())
  {
    QString mission_name; 
    mission_name = ui->line_edit_name->text();
    Q_EMIT(missionNameSelected(mission_name));
    Q_EMIT(close());
  } 
  else
  {
    QMessageBox error_message;
    error_message.setWindowTitle(QString::fromStdString("Mission parameters error"));
    error_message.setText(QString::fromStdString("The mission name cannot be empty. Please insert a name."));
    error_message.exec();
  }
}