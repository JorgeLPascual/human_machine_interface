
/*
  DroneWidget
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/#include "../include/drone_widget.h"

DroneWidget::DroneWidget(QWidget * parent, ObjectController::Drone d, int i) : QWidget(parent), 
	ui(new Ui::DroneWidget) {

	ui->setupUi(this);
	this->d = d;
	this->internal_id = i;
	spin_id = ui->spin_id;
	spin_x_size = ui->spin_x_size;
	spin_y_size = ui->spin_y_size;
	spin_x_coor = ui->spin_x_coor;
	spin_y_coor = ui->spin_y_coor;
	spin_degrees = ui->spin_degrees;
	spin_take_off = ui->spin_take_off;
	accept = ui->accept;
	spin_id->setValue(d.id);
	spin_x_size->setValue(d.x_size);
	spin_y_size->setValue(d.y_size);
	spin_x_coor->setValue(d.x_coor);
	spin_y_coor->setValue(d.y_coor);
	spin_degrees->setValue(d.degrees);
	spin_take_off->setValue(d.take_off);

	QObject::connect(accept, SIGNAL(clicked()),this,SLOT(acceptButton()));
	QObject::connect(spin_id, SIGNAL(valueChanged(int)),this,SLOT(valueChanged()));
	QObject::connect(spin_x_size, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_y_size, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_x_coor, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_y_coor, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_degrees, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));
	QObject::connect(spin_take_off, SIGNAL(valueChanged(double)),this,SLOT(valueChanged()));

} 

DroneWidget::~DroneWidget() {
	delete ui;
}

void DroneWidget::acceptButton() {
	Q_EMIT(closed());
	close();
}

void DroneWidget::valueChanged() {
	ObjectController::Drone d = this->d;
	d.id = spin_id->value();
	d.x_size = spin_x_size->value();
	d.y_size = spin_y_size->value();
	d.x_coor = spin_x_coor->value();
	d.y_coor = spin_y_coor->value();
	d.degrees = spin_degrees->value();
	d.take_off = spin_take_off->value();
	Q_EMIT(droneChanged(d,this->internal_id));
}