/*!*******************************************************************************************
 *  \file       mission_specification_dialog.h
 *  \brief      Mission_specification_dialog definition file.
 *  \details    File used for specificating mission name.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef MISSIONSPECIFICATIONDIALOG_H
#define MISSIONSPECIFICATIONDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QString>
#include <QSize>
#include <QPushButton>
#include <QMessageBox>
#include <iostream>
#include "ui_mission_specification_dialog.h"
#include "tree_item.h"

namespace Ui{
class MissionSpecificationDialog;
}

class MissionSpecificationDialog : public QDialog
{
  Q_OBJECT

public: 
  explicit MissionSpecificationDialog(QWidget *parent = 0);
  ~MissionSpecificationDialog();
  
private:
  Ui::MissionSpecificationDialog *ui;
  QGridLayout *my_layout;
  QPushButton *accept_button;
  QWidget *padre;


public Q_SLOTS:
  void actionAccept();

Q_SIGNALS:
  void missionNameSelected(const QString &);
};
#endif