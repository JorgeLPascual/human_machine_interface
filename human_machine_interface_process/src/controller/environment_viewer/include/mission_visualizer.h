/*!*******************************************************************************************
 *  \file       mission_visualizer.h
 *  \brief      Mission_visualizer definition file.
 *  \details    In charge of displaying mission details.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef MISSION_VISUALIZER_H
#define MISSION_VISUALIZER_H

#include <QWidget>
#include <QSplitter>
#include "behavior_tree_visualizer.h"
#include "environment_widget.h"

class MissionVisualizer : public QSplitter
{
    Q_OBJECT

public:
    explicit MissionVisualizer(QWidget *parent = 0, Connection * connection = 0, OdometryStateReceiver * odometry_receiver = 0,
        ObstaclesReceiver * obstacles_receiver = 0, MissionStateReceiver * mission_receiver = 0,
        TrajectoryAbsRefCommandReceiver * command_receiver = 0, SocietyPoseReceiver * society_receiver = 0);
    ~MissionVisualizer();

    EnvironmentWidget * getEnvironmentWidget();
    

private:
    EnvironmentWidget * environment_widget;
    BehaviorTreeVisualizer * behavior_tree_visualizer;
    Connection * connection;
    bool using_bt;

public Q_SLOTS:
    void emitEdit();
    void emitShow();
    void setMissionMode();
    void setShowMode();
    void useBTVisualizer(bool b);
    void setBTEditMode(bool b);
    void setTreeEditMode(bool b);
    
Q_SIGNALS:
    void editing(QWidget *);
    void showing(QWidget *);
    void editingTree(QWidget *);
    void showingTree(QWidget *);
};  

#endif // MISSION_VISUALIZER_H
