/*!*******************************************************************************************
 *  \file       landmark_widget.h
 *  \brief      Landmark_widget definition file.
 *  \details    LandmarkWidget is the widget used for the landmark representation.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef LANDMARK_WIDGET_H
#define LANDMARK_WIDGET_H

#include <QWidget>
#include <QObject>
#include <iostream>
#include <QSignalMapper>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include "ui_landmark_widget.h"
#include "object_controller.h"


namespace Ui {
class LandmarkWidget;
}

class LandmarkWidget : public QWidget 
{
	Q_OBJECT

public:

    explicit LandmarkWidget(QWidget * parent, ObjectController::Landmark landmark, int i);
	~LandmarkWidget();

private:
	Ui::LandmarkWidget *ui;
	int internal_id;
	QSpinBox * spin_id;
	ObjectController::Landmark landmark;
	QDoubleSpinBox * spin_x_size;
	QDoubleSpinBox * spin_y_size;
	QDoubleSpinBox * spin_x_coor;
	QDoubleSpinBox * spin_y_coor;
	QSpinBox * spin_aruco0_id;
	QSpinBox * spin_aruco1_id;
	QDoubleSpinBox * degrees;
	QPushButton * accept;

public Q_SLOTS:
	void acceptButton();
	void valueChanged();

Q_SIGNALS:
	void closed();
	void landmarkChanged(ObjectController::Landmark landmark, int i);
};

#endif
