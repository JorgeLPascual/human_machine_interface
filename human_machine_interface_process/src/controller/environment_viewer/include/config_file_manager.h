/*!*******************************************************************************************
 *  \file       config_file_manager.h
 *  \brief      Config_file definition file.
 *  \details    The ConfigFileManager translates configuration files and creates intermediate files in order to use them in the map editor. 
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef CONFIG_FILE_MANAGER_H
#define CONFIG_FILE_MANAGER_H

#include <ros/ros.h>
#include <iostream>
#include <QObject>
#include <pugixml.hpp>
#include "object_controller.h"
#include <QMessageBox>

class ConfigFileManager : public QObject
{

	Q_OBJECT

public:
	explicit ConfigFileManager();
	~ConfigFileManager();
	void saveObjects(std::string filename, ObjectController::Objects objects);
	ObjectController::Objects loadObjects(std::string filename);
	ObjectController::Objects importObjects();
	bool exportObjects(QWidget * object_controller);

private:
	ros::NodeHandle n;
	ObjectController::Objects objects;
	std::string my_stack_directory;
	std::string drone_id_namespace;
	std::string config_dir;
	std::string drone_dir;
	std::string filename;

	bool exportDrone(int i);
	void exportMapData();
	void exportDroneData(int i);
	bool exportObstaclesData();
	void exportArucosData();

	std::vector<double> returnValuesPtr(std::string value);
	ObjectController::Aruco findArucoById(std::vector<ObjectController::Aruco> arucos, int id);
	bool isPole(pugi::xml_node node);
	ObjectController::Pole returnCompletedPole(ObjectController::Pole pole);
	ObjectController::Landmark returnCompletedLandmark(ObjectController::Landmark land);
	double mod(double a, double b);
//public Q_SLOTS:
};

#endif