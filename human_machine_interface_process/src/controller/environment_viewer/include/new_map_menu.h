/*!*******************************************************************************************
 *  \file       new_map_menu.h
 *  \brief      New_map_menu definition file.
 *  \details    File used when creating a new map.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef NEW_MAP_MENU_H
#define NEW_MAP_MENU_H

#include <QGridLayout>
#include <QObject>
#include <QWidget>
#include <QDialog>
#include "ui_new_map_menu.h"
#include "object_controller.h"

namespace Ui {
class NewMapMenu;
}

class NewMapMenu : public QDialog
{
    Q_OBJECT

public:
    explicit NewMapMenu(QWidget *parent = 0, ObjectController * o = 0);
    ~NewMapMenu();

private:

    Ui::NewMapMenu *ui;
    ObjectController * object_controller;

public Q_SLOTS:
	void drawMap();
};

#endif
