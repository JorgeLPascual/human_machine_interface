/*!*******************************************************************************************
 *  \file       tree_file_manager.h
 *  \brief      Tree_file_manager definition file.
 *  \details    This file is in charge of the tree files loading and saving.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef TREE_FILE_MANAGER_H
#define TREE_FILE_MANAGER_H

#include <iostream>
#include <fstream>
//#include "ui_tree_file_manager.h"
#include "tree_item.h"
#include <QTextEdit>
#include "behavior_tree.h"
#include "behavior_dialog.h"
#include "yaml-cpp/yaml.h"

/*
namespace Ui {
class TreeFileManager;
}
*/

class TreeFileManager 
{
public:
  TreeFileManager();
  ~TreeFileManager();

public:
  void saveTree(TreeItem* root_item, std::string variables, std::string save_route);
  TreeItem* loadTree(std::string load_route, QTextEdit * vis);

private:
  void recursive_save(TreeItem* item, int id_count, int parent);

  int id_count;
  YAML::Emitter emitter;
};

#endif