/*!*******************************************************************************************
 *  \file       received_data_processor.h
 *  \brief      Received_data_processor defintion file.
 *  \details    This file process new parameters in order to modify the environment.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef RECEIVED_DATA_H
#define RECEIVED_DATA_H

#include <ros/ros.h>
#include <iostream>
#include <QObject>
#include "../../connection/include/odometry_state_receiver.h"
#include "../../connection/include/mission_state_receiver.h"
#include "../../connection/include/obstacles_receiver.h"
#include "../../connection/include/trajectory_abs_ref_command_receiver.h"
#include "../../connection/include/society_pose_receiver.h"
#include "object_controller.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include <angles/angles.h>


class ReceivedDataProcessor : public QObject {

    Q_OBJECT

public:

    explicit ReceivedDataProcessor(ObjectController * object_controller = 0, OdometryStateReceiver * odometry_receiver = 0,
        ObstaclesReceiver * obstacles_receiver = 0, MissionStateReceiver * mission_receiver = 0,
        TrajectoryAbsRefCommandReceiver * command_receiver = 0, SocietyPoseReceiver * society_receiver = 0);
    ~ReceivedDataProcessor();

private:
    
    OdometryStateReceiver * odometry_receiver;
    ObstaclesReceiver * obstacles_receiver;
    MissionStateReceiver * mission_receiver;
    TrajectoryAbsRefCommandReceiver * command_receiver;
    SocietyPoseReceiver * society_receiver;
    ObjectController * object_controller;

    std::string drone_id_namespace;

    ros::NodeHandle n;

    bool mission_state;

    double precision = 0.05;

public Q_SLOTS:

    void updateDrone();
    void updateObstacles();
    void updateTrajectory();
    void setShowMode();
    void setMissionMode();
    void updateDronesTrajectories();
};


#endif //RECEIVED_DATA_H
