/*!*******************************************************************************************
 *  \file       execution_tree.h
 *  \brief      Execution_tree definition file.
 *  \details    In charge of the trees execution. 
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef EXECUTION_TREE_H
#define EXECUTION_TREE_H

#include <iostream>
#include <stdio.h>
#include <thread>
#include <stdlib.h>
#include <mutex>
#include <condition_variable>
#include <ros/ros.h>
#include <droneMsgsROS/BehaviorSrv.h> //Activar y cancelar
#include <droneMsgsROS/BehaviorCommand.h> //Mensaje con el argumento y el nombre
#include <droneMsgsROS/BehaviorEvent.h> //Recibir terminacion
#include <droneMsgsROS/ConsultBelief.h> 
#include <droneMsgsROS/AddBelief.h> 
#include <droneMsgsROS/InitiateBehaviors.h> 
//#include <droneMsgsROS/RemoveBelief.h> 
#include <std_srvs/Empty.h>
#include <std_msgs/Bool.h>
#include "yaml-cpp/yaml.h"
#include <ctime>
#include <cstdlib>
#include <chrono>
#include <boost/algorithm/string/replace.hpp>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <iostream>
#include "behavior_tree.h"
#include "tree_item.h"


static const std::string COLOR_BLUE = "#aec5ff";
static const std::string COLOR_GREEN = "#57ff67";
static const std::string COLOR_RED = "#ff5757";
static const std::string COLOR_WHITE = "#ffffff";
static const std::string SUBSTITUTION_S = "+";

class ExecutionTree : public QObject
{

  Q_OBJECT

public:
    explicit ExecutionTree(BehaviorTree * tree = 0);
    ~ExecutionTree();

    void executeTree(TreeItem * item);
    bool executeItem(TreeItem * item);
    bool getReturnedValue();
    bool isRunning();
    static void executeParallelTree(ExecutionTree * et, TreeItem * item);

    static std::mutex mutex;

private:

  void behaviorCompletedCallback(const droneMsgsROS::BehaviorEvent &msg);
  std::string processData(std::string raw_arguments);
  std::string processQueryData(YAML::Node query);
  std::string processType(YAML::const_iterator it);
  void setText(std::string str);

	bool running;
	bool tree_returned_value;
	bool waiting;
	bool cancelled;
  bool behavior_value;
  TreeItem * actual_item;
  BehaviorTree * parent;

	std::vector<ExecutionTree*> trees_in_parallel;
	std::vector<std::thread*> threads;

	static std::condition_variable condition;
	std::mutex behavior_mutex;
	std::condition_variable behavior_condition_variable;

  ros::NodeHandle n;
  ros::Subscriber behavior_sub;
  ros::ServiceClient activate_behavior_srv;
  ros::ServiceClient initiate_behavior_srv;
  ros::ServiceClient cancel_behavior_srv;
  //ros::ServiceClient initiate_behaviors_srv;
  ros::ServiceClient execute_query_srv;
  ros::ServiceClient add_belief_srv;

  std::string topic_behavior_completed;
  std::string activate_behavior;
  std::string initiate_behaviors;
  std::string cancel_behavior;
  //std::string initiate_behaviors;
  std::string execute_query;
  std::string add_belief;

  //Eliminar
	std::string tis;

  void setColor(TreeItem* item, std::string color);

public Q_SLOTS:
  void cancelExecution();

Q_SIGNALS:
  void finished();
  void update();
  void updateText();
};

#endif // EXECUTION_TREE