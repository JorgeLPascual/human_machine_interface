/*!*******************************************************************************************
 *  \file       tree_item.h
 *  \brief      Tree_item definition file.
 *  \details    The BehaviorTree, along with behavior_dialog, behavior_tree_visualizer, behavior_tree and execution_tree allows a user to create
 *                          missions with a tree structure via the HMI. 
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef TREE_ITEM_H
#define TREE_ITEM_H

#include <QWidget>
#include <QTreeWidget>
#include <QPoint>
#include <iostream>
#include <QString>
#include <QStringList>
#include <unistd.h>
#include <QIcon>
#include <QBrush>
#include <QColor>
#include <QPixmap>
#include <QColor>
#include <QBrush>
#include <QPropertyAnimation>
#include <sstream>
#include <string>

class BehaviorTree;

enum NodeType {
	SEQUENCE,
	BEHAVIOR,
	SELECTOR,
	PARALLEL,
	SUCCEEDER,
	INVERTER,
	QUERY,
	REPEAT_UNTIL_FAIL,
    REPEATER,
    ADD_BELIEF
};

enum BehaviorType {
	TAKE_OFF,
	GO_TO_POINT,
	LAND,
    ROTATE,
    FOLLOW_IMAGE,
    PAY_ATTENTION_TO_VISUAL_MARKERS,
    KEEP_HOVERING,
    WAIT
};

class TreeItem : public QTreeWidgetItem
{

public:   
    explicit TreeItem(TreeItem *parent = 0, NodeType node_type = NodeType::SEQUENCE, BehaviorType = BehaviorType::LAND);
    ~TreeItem();

    void addChild(TreeItem * item);
    void removeChild(TreeItem * item);
    TreeItem * child(int index);
    int childCount();

    NodeType getNodeType();
    BehaviorType getBehaviorType();
    TreeItem * getParent();
    std::vector<TreeItem*> getChildren();
    std::string getNodeName();
    std::string getPartialNodeName();
    std::string getNodeAttributes();
    bool isRoot();
    bool isRecurrent();
    bool isActivated();

    void setRoot(bool isRoot);
    void setNodeName(std::string text);
    void setPartialNodeName(std::string text);
    void setNodeAttributes(std::string text);
    void setNodeType(NodeType nodetype);
    void setBehaviorType(BehaviorType behaviortype);
    void setColor(std::string color);
    void set_Color(QColor color);
    void setRecurrent(bool recurrent);
    void setActivate(bool activate);

    void removeItemWidget();
    std::string nodeTypeToString(NodeType node);
    std::string behaviorTypeToString(BehaviorType behavior);
    BehaviorType stringToBehaviorType(std::string str);
    NodeType stringToNodeType(std::string str);

    void animatedColor(std::string color);

    void modifyNode(std::string node_name, NodeType node_type, BehaviorType behavior_type, bool is_recurrent, bool activate, std::string arguments);

private:
	std::string attributes;
	std::string node_name;
    std::string partial_node_name;

	bool root;
    bool recurrent;
    bool activate;

	TreeItem *parent_node;

	NodeType node_type;

	BehaviorType behavior_type;

	std::vector<TreeItem*> children;

	TreeItem * parent;
};

#endif // TREE_ITEM