/*!*******************************************************************************************
 *  \file       drone_widget.h
 *  \brief      Drone_widget definition file.
 *  \details    DroneWidget is the widget used for the drone representation.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef DRONE_WIDGET_H
#define DRONE_WIDGET_H

#include <QWidget>
#include <QObject>
#include <iostream>
#include <QSignalMapper>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include "ui_drone_widget.h"
#include "object_controller.h"


namespace Ui {
class DroneWidget;
}

class DroneWidget : public QWidget 
{
	Q_OBJECT

public:

    explicit DroneWidget(QWidget * parent, ObjectController::Drone d, int i);
	~DroneWidget();

private:
	Ui::DroneWidget *ui;
	int internal_id;
	ObjectController::Drone d;
	QSpinBox * spin_id;
	QDoubleSpinBox * spin_x_size;
	QDoubleSpinBox * spin_y_size;
	QDoubleSpinBox * spin_x_coor;
	QDoubleSpinBox * spin_y_coor;
	QDoubleSpinBox * spin_degrees;
	QDoubleSpinBox * spin_take_off;
	QPushButton * accept;

public Q_SLOTS:
	void acceptButton();
	void valueChanged();

Q_SIGNALS:
	void closed();
	void droneChanged(ObjectController::Drone drone, int i);
};

#endif
