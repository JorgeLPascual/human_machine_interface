/*!*******************************************************************************************
 *  \file       environment_widget.h
 *  \brief      Environment_widget definition file.
 *  \details    EnvironmentWidget is the widget used for the map edition management.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef ENVIRONMENT_WIDGET_H
#define ENVIRONMENT_WIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QGraphicsView>
#include <QSignalMapper>
#include <ros/ros.h>
#include <QFileDialog>
#include "ui_environment_widget.h"
#include "tool_widget.h"
#include "new_map_menu.h"
#include "object_controller.h"
#include "drone_widget.h"
#include "wall_widget.h"
#include "pole_widget.h"
#include "landmark_widget.h"
#include "config_file_manager.h"
#include "../../connection/include/odometry_state_receiver.h"
#include "../../connection/include/mission_state_receiver.h"
#include "../../connection/include/obstacles_receiver.h"
#include "../../connection/include/society_pose_receiver.h"
#include "../../connection/include/trajectory_abs_ref_command_receiver.h"
#include "../../connection/include/connection.h"
#include "behavior_tree_visualizer.h"
#include "received_data_processor.h"
#include <QMessageBox>
#include <QSplitter>

namespace Ui {
class EnvironmentWidget;
}

class EnvironmentWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EnvironmentWidget(QWidget *parent = 0, Connection * connection = 0, OdometryStateReceiver * odometry_receiver = 0,
        ObstaclesReceiver * obstacles_receiver = 0, MissionStateReceiver * mission_receiver = 0,
        TrajectoryAbsRefCommandReceiver * command_receiver = 0, SocietyPoseReceiver * society_receiver = 0);
    ~EnvironmentWidget();
    

private:

    Ui::EnvironmentWidget *ui;
    ToolWidget * tool_widget;
	ObjectController * object_controller;
    QGridLayout * layout;
    QGridLayout * layout_buttons;
    NewMapMenu * new_map_menu;
    QWidget * widget_config = NULL;
    ConfigFileManager * config_file_manager;
    QSignalMapper * mapper;
    bool null = true;
    ros::NodeHandle n;
    std::string my_stack_workspace;
    QLabel * object_label;
    QLabel * tools_label;
    QPushButton * edit_button;
    QPushButton * new_map_button;
    QPushButton * save_map_button;
    QPushButton * load_map_button;
    QPushButton * import_map_button;
    QPushButton * export_map_button;
    QPushButton * cancel_map_button;
    QPushButton * accept_map_button;
    QPushButton * clear_button;

    ReceivedDataProcessor * received_data_processor;

public Q_SLOTS:
	void createNewMapMenu();
	void createDroneMenu(ObjectController::Drone drone, int i);
    void createWallMenu(ObjectController::Wall wall, int i);
    void createPoleMenu(ObjectController::Pole pole, int i);
    void createLandmarkMenu(ObjectController::Landmark land, int i);
	void deleteMenu();

    void editButton(bool b);

    void saveMap();
    void loadMap();
    void exportMap();
    void importMap();
    void setShowMode();
    void setEditMode();
    void editMap();
    void cancelMap();
    void setMissionMode();
    void clearTrajectories();

Q_SIGNALS:
    void editing(QWidget *);
    void showing(QWidget *);
};

#endif // ENVIRONMENT_WIDGET_H
