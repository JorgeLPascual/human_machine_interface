/*!*******************************************************************************************
 *  \file       behavior_tree_visualizer.h
 *  \brief      Behavior_tree_visualizer definition file.
 *  \details    The BehaviorTreeVisualizer, along with behavior_dialog, behavior_tree, tree_item and execution_tree allows a user to create
 *              missions with a tree structure via the HMI.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef BEHAVIORTREEVISUALIZER_H
#define BEHAVIORTREEVISUALIZER_H

#include <QWidget>
#include <QTreeWidget>
#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QLabel>
#include <QSplitter>
#include <QCheckBox>
#include <iostream>
#include "behavior_tree.h"
#include "tree_file_manager.h"
#include "ui_behavior_tree_visualizer.h"
#include <QFileDialog>

namespace Ui {
class BehaviorTreeVisualizer;
}

class BehaviorTree;

class BehaviorTreeVisualizer : public QWidget
{
  Q_OBJECT

public:
  explicit BehaviorTreeVisualizer(QWidget *parent = 0);
  ~BehaviorTreeVisualizer();

  void setText(std::string texto);
  std::string getText();

private:
  Ui::BehaviorTreeVisualizer *ui;
  QGridLayout *my_layout;
  BehaviorTree *visualized_tree;
  QLabel *tree_label;
  QLabel *beliefs_label;
  QTextEdit *beliefs_text;
  BehaviorTree *tree;
  QString text;
  QSplitter *splitter;
  QWidget *variables_widget;
  QGridLayout *variables_layout;
  QCheckBox *expand_text_button;
  QPushButton * edit_button;
  QPushButton * load_button;
  QPushButton * save_button;
  QPushButton * accept_button;
  QPushButton * new_button;
  QPushButton * cancel_button;
  TreeItem * original_tree_copy;
  bool editing;

public Q_SLOTS:
 void startBlockingTextInput();
 void stopBlockingTextInput();
 void update();
 void executeTree();
 void cancelTree();
 void toggleEditMode();
 void sendEditModeChange();
 void loadTreeFile();
 void saveTreeFile();
 void cancelTreeEdition();

Q_SIGNALS:
  void missionStarted();
  void missionFinished();
  void executeTreeSignal();
  void cancelTreeSignal();
  void setEditMode(bool);

};

#endif
