/*!*******************************************************************************************
 *  \file       object_controller.h
 *  \brief      Object_controller definition file.
 *  \details    In charge of setting, drawing and modifying map items. 
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef OBJECT_CONTROLLER_H
#define OBJECT_CONTROLLER_H

#include <iostream>
#include <QWidget>
#include <QGraphicsView>
#include <QObject>
#include <QGridLayout>
#include <QWheelEvent>
#include <QGraphicsTextItem>
#include <QString>
#include <boost/format.hpp>
#include <QGraphicsEllipseItem>
#include "tool_widget.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneDragDropEvent>
#include <cmath>
#include <QSizeF>
#include <QTextDocument>
#include <time.h>

#define PI 3.14159265

class ObjectController : public QGraphicsView
{
	Q_OBJECT

public:

	explicit ObjectController(QWidget *parent = 0);
	~ObjectController();
	void setMap(double x_meters, double y_meters, double x_init = 0, double y_init = 0);
	void setToolWidget(ToolWidget * tw);
	bool isMapSet();
	void setMissionMode(bool status);
	void clearTrajectories();
	void saveStatus();
	void loadStatus();

	struct Point {
		double x;
		double y;
	};

	struct Map {
		bool set = false;
		double x_meters;
		double y_meters;
		double x_init = 0;
		double y_init = 0;
	} map;

	struct Drone {
		int id;
		double x_size;
		double y_size;
		double x_coor;
		double y_coor;
		double degrees;
		double take_off;
		bool selected = false;
		QGraphicsPixmapItem * item;
		QGraphicsLineItem * arrow;
		std::vector<Point> points;
	};

	struct Wall {
		int id;
		double x_size;
		double y_size;
		double x_coor;
		double y_coor;
		double degrees;
		std::string description;
		std::string virtual_description;
		QGraphicsRectItem * item;
		bool selected = false;
	};

	struct Aruco {
		int id;
		double x_coor;
		double y_coor;
		double z_coor;
		double degrees;
	};

	struct Pole {
		int id;
		double x_radius;
		double y_radius;
		double x_size;
		double y_size;
		double x_coor;
		double y_coor;
		Aruco aruco[4];
		bool selected = false;
		QGraphicsPixmapItem * item;
	};

	struct Landmark {
		int id;
		double x_size;
		double y_size;
		double x_coor;
		double y_coor;
		double degrees;
		Aruco aruco[2];
		bool selected = false;
		QGraphicsPixmapItem * item;
	};


	struct Objects {
		std::vector<Drone> drones;
		std::vector<Wall> walls;
		std::vector<Pole> poles;
		std::vector<Landmark> landmarks;
		std::vector<std::vector<Point>> trajectories;
		Map map;
	};


private:

	int x_center;
	int y_center;
	bool drag = false;
	bool click = false;
	clock_t time;
	int x_drag;
	int y_drag;

	bool sticky = false;

	Objects objects;
	Objects previous_status;

	double pix_per_meter = 100;
	int pix_guide;

	//Drone default values
	double default_drone_x_size = 0.6;
	double default_drone_y_size = 0.6;
	double default_drone_degrees = 90; //+Y orientation
	double default_take_off_value = 0.7;
	double default_real_drone_percent = 0.8;

	int default_drone_id = 1;
	int default_wall_id = 10000;
	int default_pole_id = 40000;
	int default_landmark_id = 60000;
	int default_aruco_id = 10;

	double default_aruco_z = 1.3;

	QGraphicsScene * scene;
	QGraphicsTextItem * coordinates;
	QPen pencil;
	QBrush brush;
	QGraphicsEllipseItem * sticky_ellipse;
	ToolWidget * tool_widget;
	bool delete_on_unselect = false;

	bool mission_mode = false;


	double coor_x;
	double coor_y;

	void reDraw();
	void drawGuide();
	void drawMap();
	void drawCoordinates(int x, int y);
	void drawObjects();
	void drawDrone(int i);
	void drawWall(int i);
	void drawPole(int i);
	void drawLandmarks(int i);
	void drawPoints();
	void drawTrajectories();


	void setAutoPixPerMeter();
	void setAutoMapWalls();
	double approachNumber(double i);

	void toMapCoor(int x, int y, double * result);
	void toSceneCoor(double x, double y, int * result);

	void resizeEvent(QResizeEvent * event);
	void wheelEvent(QWheelEvent * event);
	void mouseMoveEvent(QMouseEvent * event);
	void mousePressEvent(QMouseEvent * event);
	void mouseReleaseEvent(QMouseEvent * event);
	void keyPressEvent(QKeyEvent * event);
	void keyReleaseEvent(QKeyEvent * event);
	void addNewDrone();
	void addNewWall(std::string description = "wall", double x_size = 0.5, double y_size = 0.5, double x_coor = 0, double y_coor = 0);
	void addNewPole(double x_coor = 0, double y_coor = 0, double x_radius = 0.2, double y_radius = 0.2);
	Pole autoSetPoleArucos(Pole pole, int aruco0, int aruco1, int aruco2, int aruco3);
	void addNewLandmark(double x_coor = 0, double y_coor = 0);
	Landmark autoSetLandmarkArucos(Landmark landmark, int aruco0, int aruco1);

	void selectItem();
	void modifySelectedObjectPosition();

public: 
	Objects getObjects();
	void setObjects(Objects objects);
	void updateDrone(Drone drone, int drone_pos);
	void updateTrajectory(std::vector<Point> trajectory);
	void updateObstacles(Pole pole);

public Q_SLOTS:
	void changeDrone(ObjectController::Drone d, int i);
	void changeWall(ObjectController::Wall w, int i);
	void changePole(ObjectController::Pole p, int i);
	void changeLandmark(ObjectController::Landmark l, int i);
	void unselectItem();


Q_SIGNALS:
	void createDroneMenu(ObjectController::Drone drone, int i);	
	void createWallMenu(ObjectController::Wall w, int i);	
	void createPoleMenu(ObjectController::Pole p, int i);	
	void createLandmarkMenu(ObjectController::Landmark l, int i);	
	void unselect();

};

#endif
