/*!*******************************************************************************************
 *  \file       tool_widget.h
 *  \brief      Tool_widget definition file.
 *  \details    In charge of setting the mouse item and function. 
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef TOOL_WIDGET_H
#define TOOL_WIDGET_H

#include <QToolButton>
#include <QWidget>
#include <QObject>
#include <iostream>
#include <QSignalMapper>
#include "ui_tool_widget.h"
#include <QBitmap>
#include <QKeyEvent>


namespace Ui {
class ToolWidget;
}

class ToolWidget : public QWidget 
{
	Q_OBJECT

public:

    explicit ToolWidget(QWidget * parent);
	~ToolWidget();

	QToolButton * getTool();
	bool isSticky();
	void unsetTool();

private:
	Ui::ToolWidget *ui;

	QToolButton * button1;
	QToolButton * button2;
	QToolButton * button3;
	QToolButton * button4;
	QToolButton * button5;
	QToolButton * button6;
	QToolButton * button7;

	QToolButton * last_pressed = NULL;

	QSignalMapper * mapper;

	bool sticky = false;

public Q_SLOTS:

	void onClickedButton(QWidget * b);
	void onClickedStikyButton();

};

#endif
