/*!*******************************************************************************************
 *  \file       behavior_tree.h
 *  \brief      Behavior_tree definition file.
 *  \details    The BehaviorTree, along with behavior_dialog, behavior_tree_visualizer, tree_item and execution_tree allows a user to create
 *							missions with a tree structure via the HMI.
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef BEHAVIOR_TREE_H
#define BEHAVIOR_TREE_H

#include <iostream>
#include <QTreeWidget>
#include <QWidget>
#include <QTreeWidgetItem>
#include <QMouseEvent>
#include <QString>
#include <QStringList>
#include <QPoint>
#include <QMenu>
#include <QIcon>
#include <QPixmap>
#include <QBrush>
#include <QColor>
#include <QTextEdit>
#include <QCloseEvent>
#include <QMessageBox>
#include <QErrorMessage>

#include "behavior_tree_visualizer.h"
#include "execution_tree.h"
#include "behavior_dialog.h"
#include "mission_specification_dialog.h"

class TreeItem;
class ExecutionTree;
class BehaviorTreeVisualizer;

class BehaviorTree : public QTreeWidget
{
	Q_OBJECT
public:
	explicit BehaviorTree(QWidget *parent = 0);
	~BehaviorTree();

	void addTopLevelItem(TreeItem *);
	void executeMission();
	BehaviorTreeVisualizer* getVisualizer();
	TreeItem * getRoot();
	TreeItem * copyBehaviorTree(TreeItem * item, bool root, TreeItem * inherited_parent);

protected:
	void expandText(TreeItem *item);
	void minimizeText(TreeItem *item);
	void removeItemWidget(TreeItem *);

private:
	bool has_root;

	TreeItem * root_before;
	QPoint point;

	TreeItem *root_item;
	TreeItem *parent_item_for_child;
	TreeItem *parent_item_for_sibling;
	TreeItem *parent_item_for_modify;

	BehaviorDialog *window;

	BehaviorTreeVisualizer * visualizer_parent;

	MissionSpecificationDialog *mission_dialog;

	ExecutionTree * et;
	std::thread * executing_tree;
	bool running = false;

	QMenu *contextMenu;
	bool is_menu_created = false;
  bool isTextExpanded;

  bool editing;

	//BehaviorDialog window();

public Q_SLOTS:
	void onCustomContextMenu(const QPoint &);
	void createMission(const QString &);
	void createMissionByTreeItem(TreeItem * root);
	bool isRunning();
	void windowFinished(TreeItem *);

	void removeItemWidgetAt(const QPoint &);
	void removeItemWidgetAtAux();

	void addChildAux();
	void addSiblingAux();
	void addChild(const QPoint &);
	void addSibling(const QPoint &);
	void modifyItemWidgetAux();
	void modifyItemWidget(const QPoint &);
	void executeTree();
	void executeTreeFromItem();
	void joinExecutionTree();
	void updateBackground();
	bool checkItem(TreeItem *item);
	bool checkTree(TreeItem *item);
	bool checkNodeArguments(TreeItem *item);
	void cancelTree();
  void expandTreeText(int);
  void setEditMode(bool b);
  void createMissionDialog();

Q_SIGNALS:
	void executionStarted();
	void executeRemoveItemAction(const QPoint &);
	void executeAddChildAction(const QPoint &);
	void executeAddSiblingAction(const QPoint &);
	void executeModifyItemWidgetAction(const QPoint &);
	void message_error(const QString &);
  void update();
  void cancelExecutionSignal();
  void changeName(bool);
};

#endif //BEHAVIOR_TREE
