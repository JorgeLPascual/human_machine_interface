/*
  ExecutionViewer
  @author  Jorge Luis Pascual, Carlos Valencia.
  @date    07-2017
  @version 2.0
*/
#include "../include/execution_viewer.h"

ExecutionViewer::ExecutionViewer(QWidget *parent): QWidget(parent), ui(new Ui::ExecutionViewer)
{
  ui->setupUi(this);
  my_layout = ui->gridLayout;

  belief_label = new QLabel("Belief Viewer", this);
  behavior_label = new QLabel("Behavior Viewer", this);
  belief_content = new QTextEdit(this);
  behavior_content = new QTextEdit(this);

  my_layout->addWidget(behavior_label, 0, 0);
  my_layout->addWidget(behavior_content, 1, 0);
  my_layout->addWidget(belief_label, 2, 0);
  my_layout->addWidget(belief_content, 3, 0);

  behavior_label->show();
  behavior_content->show();
  belief_label->show();
  belief_content->show();
  behavior_content->setReadOnly(true);
  belief_content->setReadOnly(true);
  connect(this, SIGNAL(setBehaviorText(const QString &)), this, SLOT(setBehaviorBoxText(const QString &)));
  connect(this, SIGNAL(setBeliefText(const QString &)), this, SLOT(setBeliefBoxText(const QString &)));

  n.param<std::string>("all_beliefs", all_beliefs, "all_beliefs");
  n.param<std::string>("list_of_active_behaviors", list_of_active_behaviors, "list_of_active_behaviors");
  n.param<std::string>("drone_id_namespace", drone_id_namespace, "drone1");
  list_of_behaviors_sub = n.subscribe("/" + drone_id_namespace +  "/" + list_of_active_behaviors, 1000, &ExecutionViewer::listOfBehaviorsCallback, this);
  list_of_beliefs_sub = n.subscribe("/" + drone_id_namespace + "/" + all_beliefs, 1000, &ExecutionViewer::listOfBeliefsCallback, this);
}

ExecutionViewer::~ExecutionViewer()
{
  delete ui;
}

void ExecutionViewer::listOfBehaviorsCallback(const droneMsgsROS::ListOfBehaviors & msg)
{
  std::string all_behaviors_active;
  for(int i = 0; i < msg.behaviors.size(); i++)
  {
    all_behaviors_active = all_behaviors_active + msg.behaviors[i] + '\n';
  }
  Q_EMIT(setBehaviorText(QString::fromStdString(all_behaviors_active)));
}

void ExecutionViewer::listOfBeliefsCallback(const droneMsgsROS::AllBeliefs & msg)
{
  std::string all_beliefs_content = msg.beliefs;
  Q_EMIT(setBeliefText(QString::fromStdString(all_beliefs_content)));
}

void ExecutionViewer::setBehaviorBoxText(const QString &texto)
{
  behavior_content->setText(texto);
}

void ExecutionViewer::setBeliefBoxText(const QString &texto)
{
  belief_content->setText(texto);
}
