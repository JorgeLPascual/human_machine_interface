/*!*******************************************************************************************
 *  \file       execution_viewer.h
 *  \brief      Execution_viewer definition file.
 *  \details    This file displays the behaviors and beliefs used while the tree is executing. 
 *  \author     Jorge Luis Pascual, Carlos Valencia.
 *  \copyright Copyright 2017 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ********************************************************************************************/
#ifndef EXECUTIONVIEWER_H
#define EXECUTIONVIEWER_H

#include <ros/ros.h>
#include <QWidget>
#include <QGridLayout>
#include <QTextEdit>
#include <QLabel>
#include <QString>
#include <droneMsgsROS/ListOfBehaviors.h>
#include <droneMsgsROS/AllBeliefs.h>
#include "ui_execution_viewer.h"

namespace Ui{
class ExecutionViewer;
}

class ExecutionViewer : public QWidget
{
    Q_OBJECT
public: 
  explicit ExecutionViewer(QWidget *parent = 0);
  ~ExecutionViewer();
  void listOfBehaviorsCallback(const droneMsgsROS::ListOfBehaviors & msg);
  void listOfBeliefsCallback(const droneMsgsROS::AllBeliefs & msg);
protected:

private:
  ros::NodeHandle n;


  Ui::ExecutionViewer *ui;
  QGridLayout *my_layout;
  QLabel *behavior_label;
  QLabel *belief_label;
  QTextEdit *behavior_content;
  QTextEdit *belief_content;



  std::string drone_id_namespace;
  std::string all_beliefs;
  std::string list_of_active_behaviors;
  ros::Subscriber list_of_behaviors_sub;
  ros::Subscriber list_of_beliefs_sub;


public Q_SLOTS:
  void setBehaviorBoxText(const QString &);
  void setBeliefBoxText(const QString &);

Q_SIGNALS:
  void setBehaviorText(const QString &);
  void setBeliefText(const QString &);
};

#endif
