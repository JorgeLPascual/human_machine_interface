#include "../include/operator_dialogue.h"

OperatorDialogue::OperatorDialogue(QWidget *parent, MissionStateReceiver *mission_receiver,CooperativeReceiver* coop_receiver, ControlPanel *controlpanel) :
    QWidget(parent),
    ui(new Ui::OperatorDialogue)
{
    ui->setupUi(this);

    ui->dialogue->setReadOnly(true);

    mission_name = "";
    mission_in_progress=false;
    current_action_arguments="";
    mission_planner_receiver = mission_receiver;
    control_panel=controlpanel;
    cooperative_receiver=coop_receiver;

    setSignalHandlers(); // triggers
}

OperatorDialogue::~OperatorDialogue()
{
    delete ui;
}

void OperatorDialogue::setSignalHandlers()
{
    //From mission
    connect(mission_planner_receiver,SIGNAL(missionLoaded(QString)),this,SLOT(displayMissionLoaded(const QString)));
    connect(mission_planner_receiver,SIGNAL(taskReceived(QString)),this,SLOT(displayCurrentTask(const QString)));
    connect(mission_planner_receiver,SIGNAL(missionCompleted(bool)),this,SLOT(displayMissionCompleted(const bool)));
    connect(mission_planner_receiver,SIGNAL(actionReceived(QString)),this,SLOT(displayCurrentAction(const QString)));
    connect(mission_planner_receiver,SIGNAL(missionStarted()),this,SLOT(displayMissionStart()));
    connect(mission_planner_receiver,SIGNAL(actionCompleted(int,int)),this,SLOT(displayActionCompleted(const int, const int)));
    connect(mission_planner_receiver,SIGNAL(actionArguments(QString)),this,SLOT(actionArgumentsReceived(const QString)));

    //From cooperative
    connect(control_panel,SIGNAL(operatorText(QString)),this,SLOT(operatorText(const QString)));
    connect(control_panel,SIGNAL(operationMode(int)),this,SLOT(displayOperationModeChange(const int)));
}

void OperatorDialogue::displayOperationModeChange(const int mode)
{
    if (mode==AUTONOMOUS)
    {
        QString text=setOperatorFont()+"Set operation mode to guided by mission planner";
        ui->dialogue->append(text);
    }
    else if (mode==MANUAL)
    {
        QString text=setOperatorFont()+"Set operation mode to keyboard teleoperation";
        ui->dialogue->append(text);
    }
    else if (mode == COOPERATIVE)
    {
        QString text=setOperatorFont()+"Set operation mode to cooperative dialogue";
        ui->dialogue->append(text);
    }

    ui->dialogue->append(setVehicleFont()+"Done");
}

QString OperatorDialogue::setOperatorFont()
{
    QString text;

    text="Operator:\t";

    return text;
}

QString OperatorDialogue::setVehicleFont()
{
    QString text;

    text="Vehicle:\t";

    return text;
}

void OperatorDialogue::displayMissionLoaded(const QString &mission)
{
    if (!mission_in_progress)
    {
        mission_name=mission;
        //QString text="The mission '"+mission_name+"' has been loaded.\n\n";
        //ui->dialogue->setText(text);
    }
}

void OperatorDialogue::displayMissionStart()
{
    //Reset action arguments. Needed because arguments are received in a different signal.
    current_action_arguments="";

    mission_in_progress=true;
    QString text=setOperatorFont()+"Start mission \""+mission_name+"\"";
    ui->dialogue->append(text);
}

void OperatorDialogue::displayCurrentTask(const QString &current_task)
{
    QString text=setVehicleFont()+"My next task is "+current_task;
    ui->dialogue->append(text);
}

void OperatorDialogue::displayCurrentAction(const QString &current_action)
{
    if (mission_planner_receiver->is_autonomous_mode_active || cooperative_receiver->is_cooperative_mode_active)
    {
        QString action;
        if (current_action=="TAKE OFF")
        {
            action="taking off";
        }
        else if (current_action=="LAND")
        {
            action="landing";
        }
        else if (current_action=="WAIT")
        {
            action="waiting for "+current_action_arguments+" seconds";
        }
        else if (current_action=="STABILIZE")
        {
            action="stabilizing";
        }
        else if (current_action=="GO TO POINT")
        {
            action="going to point "+current_action_arguments;
        }
        else if (current_action=="ROTATE YAW")
        {
            action="rotating to "+current_action_arguments+ "degrees";
        }

        QString text=setVehicleFont()+"I am "+action+" ... ";
        ui->dialogue->append(text);

        //Reset current action arguments string. Needed because arguments are received in a different signal.
        current_action_arguments="";
    }
}

void OperatorDialogue::displayMissionCompleted(const bool mission_completed)
{
    mission_in_progress=false;
    QString text;
    if (mission_completed)
    {
        text=setVehicleFont()+"Completed mission \""+mission_name+"\"\n";
    }
    else
    {
        text=setVehicleFont()+"Aborted mission: \""+mission_name+"\"\n";
    }
    ui->dialogue->append(text);
}

void OperatorDialogue::displayActionCompleted(const int action_completed_state,const int timeout)
{
    if (mission_planner_receiver->is_autonomous_mode_active || cooperative_receiver->is_cooperative_mode_active)
    {
        QString text;
        if (action_completed_state==droneMsgsROS::CompletedAction::SUCCESSFUL)
        {
            text="done";

            //To insert 'done' right after 'I am <action> ...' instead of in a new line.
            ui->dialogue->moveCursor(QTextCursor::End);
            ui->dialogue->insertPlainText(text);
        }
        else if (action_completed_state==droneMsgsROS::CompletedAction::TIMEOUT_ACTIVATED)
        {
            text=setVehicleFont()+"I am not able to complete this action in time ("+QString::number(timeout)+" seconds)";
            ui->dialogue->append(text);
        }
    }
}

void OperatorDialogue::operatorText(const QString text)
{
    ui->dialogue->append(setOperatorFont()+text);

    //Since 'memorizing point as HOME' is a special type of action, its completion must be shown here.
    if (text=="Memorize point as HOME")
    {
        ui->dialogue->append(setVehicleFont()+"Done");
    }
}


void OperatorDialogue::actionArgumentsReceived(const QString action_arguments)
{
    current_action_arguments=action_arguments;
}
