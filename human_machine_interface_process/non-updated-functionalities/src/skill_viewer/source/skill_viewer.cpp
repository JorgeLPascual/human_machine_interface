/*****************************************************************************
** Includes
*****************************************************************************/
#include "../include/skill_viewer.h"
/*****************************************************************************
** Implementation
*****************************************************************************/

SkillViewer::SkillViewer(QWidget *parent, RosGraphReceiver *collector, UserCommander *usercommander) :
    QWidget(parent),
    ui(new Ui::SkillViewer)
{
    ui->setupUi(this);
    ui->table_process_viewer->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    skill_receiver=collector;
    //connect(skill_receiver,SIGNAL(stateskillReceived(droneMsgsROS::skillsList* list_skill_state)),this,SLOT(onskillStateReceived(droneMsgsROS::skillsList *list_skill_state)));
    connect(ui->table_process_viewer,SIGNAL(customContextMenuRequested(const QPoint&)),this,SLOT(onCustomContextMenuRequested(const QPoint&)));
    connect(skill_receiver,SIGNAL(stateSkill()),this,SLOT(onSkillStateReceived()));

    user_command=usercommander;

    initializeSkillViewerTable();


}

void SkillViewer::clearFocus()
{
    //std::cout<<"Editing line finished" <<std::endl;
    //ui->line_edit->clearFocus();
}


void SkillViewer::initializeSkillViewerTable(){
    //Inicializar tabla de skills. Solicitar lista a skill manager? QUITAR
}


void SkillViewer::updateSkillState(const droneMsgsROS::SkillDescriptor *skill_container, int row_skill_viewer)
{
    //ui->table_process_viewer->setItem(row_skill_viewer,1,new QTableWidgetItem("Not Requested"));
    switch(skill_container->current_state.state)
    {
    case droneMsgsROS::SkillState::not_requested:
        ui->table_process_viewer->setItem(row_skill_viewer,1,new QTableWidgetItem("Not Requested"));
        break;

    case droneMsgsROS::SkillState::requested:
        ui->table_process_viewer->setItem(row_skill_viewer,1,new QTableWidgetItem("Requested"));
        break;

     }
}

void SkillViewer::onSkillStateReceived()
{
  int row_skill_viewer=0;
  //std::cout << "Loop the list to create the items in the table"   << std::endl;
  for(unsigned int i = 0; i < skill_receiver->skill_list.skill_list.size(); i++)
  {
      skill_containter= skill_receiver->skill_list.skill_list.at(i);

      if(!initialized_table){
          if (ui->table_process_viewer->rowCount() < row_skill_viewer)
              ui->table_process_viewer->setRowCount(row_skill_viewer);
          ui->table_process_viewer->insertRow(row_skill_viewer);
      }
      ui->table_process_viewer->setItem(row_skill_viewer,0,new QTableWidgetItem(QString(skill_containter.name.c_str())));

      updateSkillState(&skill_containter,row_skill_viewer);
      row_skill_viewer++;
  }
  initialized_table=true;
}

void SkillViewer::onTextFilterChange(const QString &arg1)
{

}

void SkillViewer::onCustomContextMenuRequested(const QPoint& pos) {
    if(initialized_table){
        QTableWidgetItem* item = ui->table_process_viewer->itemAt(pos);
        if(ui->table_process_viewer->column(item)==0){
            if (item) {
                // Map the point to global from the viewport to account for the header.
                showContextMenu(item,  ui->table_process_viewer->viewport()->mapToGlobal(pos));
            }
        }
    }

}

void SkillViewer::showContextMenu(QTableWidgetItem* item, const QPoint& globalPos){
    QMenu menu;
    //menu.addAction("    ");
    menu.addAction("Request");
    menu.addAction("Do not request");
    //menu.addAction("Record(Not implemented)");
    //connect(&menu, SIGNAL(triggered(QAction*)), this, SLOT(menuSelection(QAction*)));
    //menu.exec(globalPos);
}

void SkillViewer::menuSelection(QAction* action){

   std::cout<<"Action clicked:" + action->text().toStdString() <<std::endl;

    QModelIndexList selection = ui->table_process_viewer->selectionModel()->selectedRows();

    // Multiple rows can be selected
    for(int i=0; i< selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        QTableWidgetItem *item = ui->table_process_viewer->item( index.row(), 0 );
        std::cout<< item->text().toStdString() <<std::endl;
        if(action->text().toStdString().compare("Stop")==0)
         user_command->processMonitorCommander(item->text().toStdString(),processMonitorStates::Stop);
        if(action->text().toStdString().compare("Start")==0)
         user_command->processMonitorCommander(item->text().toStdString(),processMonitorStates::Start);
        if(action->text().toStdString().compare("Reset")==0)
         user_command->processMonitorCommander(item->text().toStdString(),processMonitorStates::Reset);
    }
}



SkillViewer::~SkillViewer()
{
    delete ui;
}

