# Human Machine Interface 


The Human Machine Interface allows operators or users of the system to bi-directionally communicate with each aerial robotic agent. Both communication directions are equally important because:


* The communication from the robotic agent to the operator is needed to monitor its state and the performance of the requested mission. This will allow the operator to actuate in case of a need to redefine or abort the mission. 


* The communication from the operator to the robotic agent is needed if a non-rigid behavior is needed to accomplish the goal, making it possible to redefine the mission and also to start, stop and abort the mission.



## Developerís Guide

### Obtaining source code and libraries
* Source code project:

        git clone https://JorgeLPascual@bitbucket.org/JorgeLPascual/human_machine_interface.git
 
### Updating source code and libraries from an existing repository

        git remote set-url origin https://bitbucket.org/JorgeLPascual/human_machine_interface/
        git checkout master
        git pull origin master

The Human Machine Interface depends on ROS and Qt.

* ROS framework: 
http://wiki.ros.org/kinetic/Installation


This software has been tested on: Ubuntu 16.04 and open source drivers.


## Project organization

The project organization use a model/view architecture that manages the relationship between data and the way it is presented to the user.

    CMakeList.txt   
    package.xml  
    README.md  
    non-updated-functionalities      
    launch      
        hmi.launch      
    src                   
        controller           
            camera_viewer 
            connection      
            control_panel      
            environment_viewer      
            main_window      
            parameters_temporal_series      
            tab_manager      
            user_commander      
            vehicle_dynamics      
    view       
        ui      
            camera_viewer      
            connection      
            control_panel      
            environment_viewer      
            main_window      
            parameters_temporal_series      
            tab_manager      
            user_commander     
            vehicle_dynamucs      
        resource      
            images.qrc      
            images    